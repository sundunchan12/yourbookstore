function nextPageClick() {
    var pageNumber = document.getElementById("pageCountView").textContent;
    pageNumber++;
    document.getElementById("PageNumber").value = pageNumber;
    document.getElementById("formFilter").submit();
};

function prevPageClick() {
    var pageNumber = document.getElementById("pageCountView").textContent;
    pageNumber--;
    document.getElementById("PageNumber").value = pageNumber;
    document.getElementById("formFilter").submit();
};

function searchClick() {
    document.getElementById("PageNumber").value = 1;
}
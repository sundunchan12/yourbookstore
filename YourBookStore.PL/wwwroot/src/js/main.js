


function show(anything) {
	document.querySelector('.dropdown-menu__textBox').value = anything;
}

let dropdown = document.querySelector('.dropdown-menu');
if (dropdown != null) {
	dropdown.onclick = function () {
		dropdown.classList.toggle('active');
	}
}


function inc(element) {
	let el = document.querySelector(`[name="${element}"]`);
	el.value = parseInt(el.value) + 1;
}

function dec(element) {
	let el = document.querySelector(`[name="${element}"]`);
	if (parseInt(el.value) > 0) {
		el.value = parseInt(el.value) - 1;
	}
}

var quantityInputs = document.querySelectorAll(".quantity-input");
if (quantityInputs != null) {
	for (i = 0; i < quantityInputs.length; i++) {

		quantityInputs[i].querySelector(".quantity-input__btn-dec").
			addEventListener('click', function (event) {
				let parent = event.currentTarget.parentElement;
				let input = parent.querySelector(".quantity-input__value");
				if (parseInt(input.value) > 1) {
					input.value = parseInt(input.value) - 1;
				}


			});
		quantityInputs[i].querySelector(".quantity-input__btn-inc").
			addEventListener('click', function (event) {
				let parent = event.currentTarget.parentElement;
				let input = parent.querySelector(".quantity-input__value");
				input.value = parseInt(input.value) + 1;

			});
	}

}


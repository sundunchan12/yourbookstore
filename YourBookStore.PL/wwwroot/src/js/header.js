$(function () {
	let header = $('header');
	let headerHeight = header.height();

	if (header.attr('class') == "header-home") {
		if ($(window).scrollTop() > 1) {
			header.addClass('header-home_fixed');
			$('.hero-screen').css({
				'paddingTop': headerHeight + 'px'
			});
		}
		$(window).scroll(function () {
			if ($(this).scrollTop() > 1) {
				header.addClass('header-home_fixed');
				$('.hero-screen').css({
					'paddingTop': headerHeight + 'px'
				});
			} else {
				header.removeClass('header-home_fixed');
				$('.hero-screen').css({
					'paddingTop': 80
				})
			}
		});
	} else {
		$('body').css({
			'paddingTop': headerHeight + 'px'
		});
		if ($(window).scrollTop() > 1) {
			header.addClass('header_fixed');
			$('body').css({
				'paddingTop': headerHeight + 'px'
			});
		}
		$(window).scroll(function () {
			if ($(this).scrollTop() > 1) {
				header.addClass('header_fixed');
				$('body').css({
					'paddingTop': headerHeight + 'px'
				});
			} else {
				header.removeClass('header_fixed');
				$('.body').css({
					'paddingTop': 80
				})
			}
		});
	}

});
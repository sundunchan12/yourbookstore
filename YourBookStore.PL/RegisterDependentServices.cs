﻿﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.EntityFrameworkCore;
using YourBookStore.BLL.Infrastructure.Helpers.Security;
using YourBookStore.BLL.Interfaces;
using YourBookStore.BLL.Services;
using YourBookStore.DAL;
using YourBookStore.DAL.EF;
 using YourBookStore.DAL.Infrastructure.Filters.BookFilters;
 using YourBookStore.DAL.Interfaces;
using YourBookStore.DAL.Interfaces.RepositoryInterfaces;
using YourBookStore.DAL.Repositories;
 using YourBookStore.Domain.DTOs;
 using YourBookStore.Domain.Entities;
 using YourBookStore.PL.Infrastucture.Extensions;
using YourBookStore.Utility.GlobalEnums;
using YourBookStore.Utility.Mapper;

namespace YourBookStore.PL;

public static class RegisterDependentServices
{
    public static WebApplicationBuilder RegisterServices(this WebApplicationBuilder builder)
    {
        
        
      // Set up policies 
        builder.Services.AddAuthorization(options =>
        {
            options.AddPolicy("Administrators", policy => policy.RequireClaim("Role", UserRoles.Administrator.ToString()));
           
            options.AddPolicy("AuthenticateUsers", policy => policy.RequireClaim("Role", 
                UserRoles.Administrator.ToString(), UserRoles.User.ToString()));
        });

        
        
        // Connecting the database
        var connection = builder.Configuration.GetConnectionString("DefaultConnection");
        builder.Services.AddDbContext<YourBookStoreDBContext>(x => x.UseSqlServer(connection));
        
    
        // Add mapper configuration
        var mapper = new MapperConfiguration(ctg =>
        {
            ctg.AddProfile(new BookMappingProfile().AddBookViewModelsMap().AddFiltersMap());
            ctg.AddProfile(new AuthorMappingProfile().AddAuthorViewModelsMap());
            ctg.AddProfile(new OrderMappingProfile().AddOrderViewModelsMap());
            ctg.AddProfile(new GenreMappingProfile().AddGenreViewModelsMap());
            ctg.AddProfile(new CategoryMappingProfile().AddCategoryViewModelsMap());
            ctg.AddProfile(new LanguageMappingProfile().AddBookLanguageViewModelsMap());
            ctg.AddProfile(new UserMappingProfile().AddUserViewModelsMap());
        }).CreateMapper();
        builder.Services.AddSingleton(mapper);
        
        // Add services to the container.
        builder.Services.AddDistributedMemoryCache();
        builder.Services.AddSession();
        builder.Services.AddControllersWithViews().AddRazorRuntimeCompilation();;
        
        builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
        builder.Services.AddScoped<IPasswordHelper, PasswordHelper>();
        builder.Services.AddScoped(typeof(IGenericEFRepository<>), typeof(GenericRepository<>));
        builder.Services.AddScoped<IUserService, UserService>();
        builder.Services.AddScoped<IOrderService, OrderService>();
        builder.Services.AddScoped<ICategoryService, CategoryService>();
        builder.Services.AddScoped<IGenreService, GenreService>();
        builder.Services.AddScoped<IBookService, BookService>();
        builder.Services.AddScoped<IPaginationService, PaginationService>();
        builder.Services.AddScoped<ILanguageService, LanguageService>();
        builder.Services.AddScoped<IAuthorService, AuthorService>();
        builder.Services.AddScoped<IBasketService, BasketService>();
        builder.Services.AddScoped<IFilter<IQueryable<Book>, BookFilterDTO>, BookPriceFilter>();
        builder.Services.AddScoped<IFilter<IQueryable<Book>, BookFilterDTO>, BookNameFilter>();
        builder.Services.AddScoped<IFilter<IQueryable<Book>, BookFilterDTO>, BookCategoryFilter>();
        builder.Services.AddScoped<IFilter<IQueryable<Book>, BookFilterDTO>, BookGenreFilter>();
        builder.Services.AddScoped<IFilter<IQueryable<Book>, BookFilterDTO>, BookAuthorFilter>();
        builder.Services.AddScoped<IFilter<IQueryable<Book>, BookFilterDTO>, BookLanguageFilter>();
        builder.Services.AddScoped<IFilter<IQueryable<Book>, BookFilterDTO>, BookSortFilter>();
        
        
        builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie();
        // Register controllers
        return builder;
    }
}
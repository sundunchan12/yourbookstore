﻿using AutoMapper;
using YourBookStore.BLL.Infrastructure.Extensions;
using YourBookStore.Domain.DTOs;
using YourBookStore.Domain.Entities;
using YourBookStore.Domain.ViewModels;
using YourBookStore.Utility.Mapper;

namespace YourBookStore.PL.Infrastucture.Extensions;
public static class MapperExtensions
    {
        public static Profile AddBookViewModelsMap(this BookMappingProfile profile)
        {
            profile.CreateMap<BookDTO, BookViewModel>().ReverseMap();
            return profile;
        }

        
        public static Profile AddAuthorViewModelsMap(this AuthorMappingProfile profile)
        {
            profile.CreateMap<AuthorDTO, AuthorViewModel>().ReverseMap();
            return profile;
        }

        public static Profile AddOrderViewModelsMap(this OrderMappingProfile profile)
        {
            profile.CreateMap<OrderDetailsDTO, OrderDetailsViewModel>().ReverseMap();
            profile.CreateMap<OrderDTO, OrderViewModel>().ReverseMap();
            return profile;
        }

        public static Profile AddUserViewModelsMap(this UserMappingProfile profile)
        {
            profile.CreateMap<UserDTO, UserViewModel>().ReverseMap();
            profile.CreateMap<UserDTO, LoginViewModel>().ReverseMap();
            profile.CreateMap<UserDTO, RegistrationViewModel>().ReverseMap();
            return profile;
        }

        public static Profile AddGenreViewModelsMap(this GenreMappingProfile profile)
        {
            profile.CreateMap<GenreViewModel, GenreDTO>().ReverseMap();
            return profile;
        }
        
        public static Profile AddCategoryViewModelsMap(this CategoryMappingProfile profile)
        {
            profile.CreateMap<CategoryViewModel, CategoryDTO>().ReverseMap();
            return profile;
        }

        public static Profile AddBookLanguageViewModelsMap(this LanguageMappingProfile profile)
        {
            profile.CreateMap<GenreViewModel, GenreDTO>().ReverseMap();
            return profile;
        }

        
        public static Profile AddFiltersMap(this Profile profile)
        {
            profile.CreateMap<FilterViewModel, BookFilterDTO>().ReverseMap();
            return profile;
        }
    }
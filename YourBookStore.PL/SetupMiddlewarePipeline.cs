﻿namespace YourBookStore.PL;

public static class SetupMiddlewarePipeline
{
    public static WebApplication SetupMiddleware(this WebApplication app)
      {
          // Configure the pipeline
          // Configure the HTTP request pipeline.
       
          app.UseSession();
          app.UseHttpsRedirection();
          app.UseDeveloperExceptionPage();
          app.UseStaticFiles();

          app.UseRouting();
          
          app.UseCookiePolicy();
          app.UseAuthentication();
          app.UseAuthorization();

          app.UseEndpoints(endpoints =>
          {
              endpoints.MapControllerRoute("admin", "{area:exists}/{controller=Home}/{action=Index}/{id?}");
              endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}");
          });
          
          
        return app;
    }
}
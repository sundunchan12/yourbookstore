﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using YourBookStore.BLL.Interfaces;
using YourBookStore.Domain.ViewModels;
using YourBookStore.Utility.GlobalConstants;

namespace YourBookStore.PL.Controllers;


public class GenresController : Controller
{
    private readonly IMapper _mapper;
    private readonly IGenreService _genreService;
    private readonly ILogger<GenresController> _logger;

    public GenresController(IGenreService genreService, IMapper mapper, ILogger<GenresController> logger)
    {
        _genreService = genreService;
        _mapper = mapper;
        _logger = logger;
    }

    [HttpGet("Genres")]
    public ActionResult GetAllGenres()
    {
        var genres = _genreService.GetAllGenreNames().ToList();
        ViewBag.Title = "Усі жанри";
        return View("Genres", genres);
    }
    
    [HttpGet("Genres/{categoryName}")]
    public ActionResult GetGenresByCategoryName(string categoryName)
    {
        if (string.IsNullOrEmpty(categoryName))
        {
            _logger.LogError("Name is null.", DateTime.UtcNow);
            return BadRequest(ExceptionMessages.GetShouldNotBeMessage("Name"));
        }
        var genres = _genreService.GetGenreNamesByCategoryName(categoryName);
        ViewBag.Title = "Жанри у категорії " + categoryName;
        return View("Genres", genres.ToList());
    }
}

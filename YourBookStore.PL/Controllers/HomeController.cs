﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using YourBookStore.PL.Models;

namespace YourBookStore.PL.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;

    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    public IActionResult Index()
    {
        return View("Index");
    }
   

}
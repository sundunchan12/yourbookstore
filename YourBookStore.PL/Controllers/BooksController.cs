﻿using AutoMapper;
using Castle.Core.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using YourBookStore.BLL.Interfaces;
using YourBookStore.Domain.DTOs;
using YourBookStore.Domain.Enums;
using YourBookStore.Domain.ViewModels;
using YourBookStore.Utility.GlobalConstants;

namespace YourBookStore.PL.Controllers;

public class BooksController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IMemoryCache _cache;
        private readonly IBookService _bookService;
        private readonly IGenreService _genreService;
        private readonly ICategoryService _categoryService;
        private readonly ILanguageService _languageService;
        private readonly IAuthorService _authorService;
        private readonly IBasketService _basketService;
        private readonly ILogger<BooksController> _logger;
        private readonly IPaginationService _paginationService;

        public BooksController(
            IBookService bookService,
            IPaginationService paginationService,
            IGenreService genreService,
            ICategoryService categoryService,
            ILanguageService languageService,
            IAuthorService authorService,
            IBasketService basketService,
            IMapper mapper,
            IMemoryCache cache,
            ILogger<BooksController> logger)
        {
            _paginationService = paginationService; 
            _basketService = basketService;
            _categoryService = categoryService;
            _languageService = languageService;
            _genreService = genreService;
            _authorService = authorService;
            _bookService = bookService;
            _logger = logger;
            _mapper = mapper;
            _cache = cache;
            
        }

        [HttpGet("Books/Buy/{bookCode}")]
        public async Task<ActionResult> AddBookToBasket(string bookCode)
        {
            if (string.IsNullOrEmpty(bookCode))
            {
               
                return BadRequest(ExceptionMessages.CodeNullMessage);
            }

            var book = await _bookService.GetBookByCodeAsync(bookCode);

            if (book is null)
            {
                _logger.LogError("Book is null", DateTime.UtcNow);
                return BadRequest(ExceptionMessages.BookNullMessage);
            }
            string? userEmail;
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                userEmail = User.Claims.FirstOrDefault(t => t.Type == "Name")?.Value;
            }
            else
            {
                userEmail = null;
            }
            var isSuccess = await _basketService.AddOrderToBasketAsync(new OrderDetailsDTO
            {
                BookId = book.Id,
                Quantity = 1,
                Price = book.Price*1,
                Code = book.Code,
                Book = book,
                
            }, HttpContext.Session, userEmail);
            return RedirectToAction("GetBasketDetails", "Basket");
        }

        
        [HttpGet("Books/Genre/{genre}")]
        public async Task<ActionResult> GetBooksByGenreName(string genre)
        {
            if (!genre.IsNullOrEmpty())
            {
                var genres = new List<string>();
                genres.Add(genre);
                var filter = new FilterViewModel
                {
                    PageSize = 100,
                    PageNumber = 1,
                    PriceFrom = 10,
                    PriceTo = await _bookService.GetMaxBookPriceAsync(),
                    SearchName = "",
                    FilterStatus = FilterStatus.None,
                    Genres = genres
                
                };
                return RedirectToAction("SearchAllBooks", "Books", filter);
            }

            return RedirectToAction("SearchAllBooks", "Books");
        }
        
        [HttpGet("Books/Category/{category}")]
        public async Task<ActionResult> GetBooksByCategoryName(string category)
        {
            if (!category.IsNullOrEmpty())
            {
                var categories = new List<string>();
                categories.Add(category);
                var filter = new FilterViewModel
                {
                    PageSize = 100,
                    PageNumber = 1,
                    PriceFrom = 10,
                    PriceTo = await _bookService.GetMaxBookPriceAsync(),
                    SearchName = "",
                    FilterStatus = FilterStatus.None,
                    Categories = categories
                
                };
                return RedirectToAction("SearchAllBooks", "Books", filter);
            }

            return RedirectToAction("SearchAllBooks", "Books");
        }
        
        [HttpGet("Books/New")]
        public async Task<ActionResult> GetNewBooks()
        {
            var filter = new FilterViewModel
            {
                PageSize = 100,
                PageNumber = 1,
                PriceFrom = 10,
                PriceTo = await _bookService.GetMaxBookPriceAsync(),
                SearchName = "",
                FilterStatus = FilterStatus.New,
            
            };
            return RedirectToAction("SearchAllBooks", "Books", filter);
        }
        
        [HttpGet("Books/Bestsellers")]
        public async Task<ActionResult> GetBestsellersBooks()
        {
            var filter = new FilterViewModel
            {
                PageSize = 100,
                PageNumber = 1,
                PriceFrom = 10,
                PriceTo = await _bookService.GetMaxBookPriceAsync(),
                SearchName = "",
                FilterStatus = FilterStatus.Bestsellers,
            
            };
            return RedirectToAction("SearchAllBooks", "Books", filter);
        }
        
        
        [HttpGet("Books/{code}")]
        public async Task<ActionResult<BookViewModel>> GetBookDetailsByCode(string code)
        {
            if (string.IsNullOrEmpty(code))
            {
                return RedirectToAction("GetAllBooks", "Books");
            }
            var book = _mapper.Map<BookViewModel>(await _bookService.GetBookByCodeAsync(code));

            //var book = await _bookService.GetBookByCodeAsync(code);
            
            if (book is null)
            {
                return RedirectToAction("GetAllBooks", "Books");
            }

            ViewBag.Category = _bookService.GetBookCategoryByCode(code);
            return View("BookDetails", book);
        }
        
        
        [HttpGet("Books/All")]
        public async Task<ActionResult<IEnumerable<BookViewModel>>> GetAllBooks()  
        {
            ViewBag.AllGenres = _genreService.GetAllGenreNames().ToList();
            ViewBag.AllCategories = _categoryService.GetAllCategoriesNames().ToList();
            
            ViewBag.AllLanguages = _languageService.GetAllLanguagesNames().ToList();
            ViewBag.AllAuthors = _authorService.GetAllAuthorsNames().ToList();
            var filter = await CreateBookFilter();
            var result = await _paginationService.GetPaginateItemsAsync(_mapper.Map<BookFilterDTO>(filter));
            var items = _mapper.Map<IEnumerable<BookViewModel>>(result.Item2);

            var bookViewModels = items as BookViewModel[] ?? items.ToArray();
            
            if (!bookViewModels.Any())
            {
                return NotFound();
            }
            
            ViewBag.Books = bookViewModels;
            return View("Books", filter);
        }
        
        [HttpGet("Books/Filter")]
        public async Task<ActionResult<IEnumerable<BookViewModel>>> SearchAllBooks(FilterViewModel filter)
        {
            ViewBag.AllGenres = _genreService.GetAllGenreNames().ToList();
            ViewBag.AllCategories = _categoryService.GetAllCategoriesNames().ToList();
            ViewBag.AllLanguages = _languageService.GetAllLanguagesNames().ToList();
            ViewBag.AllAuthors = _authorService.GetAllAuthorsNames().ToList();
            if (!ModelState.IsValid)
            {   IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return View("Books", filter);
            }

            var result = await _paginationService.GetPaginateItemsAsync(_mapper.Map<BookFilterDTO>(filter));
            var books = _mapper.Map<IEnumerable<BookViewModel>>(result.Item2);

            var viewModels = books as BookViewModel[] ?? books.ToArray();
            if (viewModels.IsNullOrEmpty())
            {
                return View("Books", filter);
            }

            var bookViewModels = books as BookViewModel[] ?? viewModels.ToArray();

            if (!bookViewModels.Any())
            {
                _logger.LogError("List of all books is empty", DateTime.UtcNow);
                return NotFound();
            }
            ViewBag.AllGenres = _genreService.GetAllGenreNames().ToList();
            ViewBag.AllCategories = _categoryService.GetAllCategoriesNames().ToList();
            ViewBag.AllLanguages = _languageService.GetAllLanguagesNames().ToList();

            
            ViewBag.Books = bookViewModels;
            
            return View("Books", filter);
        }
        
     
        private async Task<FilterViewModel> CreateBookFilter() => 
            new FilterViewModel
            {
                PageSize = 100,
                PageNumber = 1,
                PriceFrom = 10,
                PriceTo = await _bookService.GetMaxBookPriceAsync(),
                SearchName = "",
                FilterStatus = FilterStatus.None
                
            };
        
}
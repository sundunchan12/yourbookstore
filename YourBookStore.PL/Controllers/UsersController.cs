﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using YourBookStore.BLL.Interfaces;
using YourBookStore.BLL.Validations;
using YourBookStore.Domain.DTOs;
using YourBookStore.Domain.ViewModels;
using YourBookStore.Utility.GlobalConstants;

namespace YourBookStore.PL.Controllers;

public class UsersController : Controller
{
    private readonly IMapper _mapper;

    private readonly IUserService _userService;
    private readonly IOrderService _orderService;
    private readonly IBookService _bookService;

    public UsersController(IUserService userService, IBookService bookService, IMapper mapper, IOrderService orderService)
    {
        _userService = userService;
        _orderService = orderService;
        _bookService = bookService;
        _mapper = mapper;
    }
    
    [HttpGet("SignIn")]
    [AllowAnonymous]
    public ActionResult SignIn()
    {
        return View("SignIn", new LoginViewModel());
    }

    [HttpPost("SignIn")]
    [AllowAnonymous]
    public async Task<ActionResult> SignIn(LoginViewModel login)
    {
        if (!ModelState.IsValid)
        {
            return View("SignIn");
        };
        
        try
        {
            await _userService.SignInAsync(_mapper.Map<UserDTO>(login), HttpContext);
            return Redirect("/Home");
        }
        catch (YourBookStoreException e)
        {
            ModelState.AddModelError(nameof(LoginViewModel.Email), "Неправильний логін або пароль");
        
        }
        
        return View("SignIn", login);

    }
    
    [HttpGet("SignOut")]
    public async Task<ActionResult> SignOut()
    {
        await _userService.SignOutAsync(HttpContext);
        return Redirect("/Home");
    }
    
    [HttpGet("SignUp")]
    [AllowAnonymous]
    public ActionResult SignUp()
    {
        return View("SignUp");
    }

    [HttpPost("SignUp")]
    [AllowAnonymous]
    public async Task<ActionResult> SignUp(RegistrationViewModel model)
    {
        if (_userService.IsUserExists(model.Email))
        {
            ModelState.AddModelError("Email", "Користувач з цим Email вже існує");
        }

        if (!ModelState.IsValid)
        {
            return View("SignUp", model);
        }

        await _userService.CreateAsync(_mapper.Map<UserDTO>(model));
        await SignIn(new LoginViewModel
        {
            Email = model.Email,
            Password = model.Password
        });
        return Redirect("/Home");
    }
    
    [HttpGet("Profile")]
    [Authorize]
    public ActionResult Profile()
    {
        if (HttpContext.User.Identity.IsAuthenticated)
        {
            var email = User.Claims.FirstOrDefault(x=>x.Type == "Name")?.Value;
            var user = _userService.GetUserByEmail(email);
            if (user is null) Redirect("SignIn");
            var userMapped = _mapper.Map<UserViewModel>(user);
            var userOrders = _orderService.GetOrdersByUserName(userMapped.Email).ToList();
            List<BookDTO> orderedBooks = new List<BookDTO>();
            foreach (OrderDTO order in userOrders)
            {
                foreach(OrderDetailsDTO orderDetail in order.OrderDetails)
                {
                    var book = _bookService.GetBookByIdAsync(orderDetail.BookId).Result;
                    orderDetail.Book = book;
                }
            }

            ViewBag.Books = orderedBooks;

            if (userOrders != null || userOrders.ToList().Count != 0)
            {
                ViewBag.Orders = userOrders; 
            }
            else
            {
                ViewBag.Orders = new List<OrderDTO>();
            }

            return View("UserProfile", userMapped);
        }
        
        return Redirect("SignIn");;
    }

    [HttpGet("Profile/Edit")]
    [Authorize]
    public ActionResult EditProfile(string email)
    {
        var user = _userService.GetUserByEmail(email);
        var userMapped = _mapper.Map<UserViewModel>(user);
        return View("UserProfileEdit", userMapped);
    }

    [HttpPost("Profile/Edit")]
    [Authorize]
    public async Task<ActionResult> EditProfile(UserViewModel model)
    {
        if (!ModelState.IsValid)
        {
            return View("UserProfileEdit", model);
        }
        var userMapped = _mapper.Map<UserDTO>(model);
        await _userService.UpdateUserAsync(userMapped);
        return Redirect("/Profile");
    }


}
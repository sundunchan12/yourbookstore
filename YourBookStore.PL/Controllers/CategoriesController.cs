﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using YourBookStore.BLL.Interfaces;
using YourBookStore.Domain.Entities;
using YourBookStore.Domain.ViewModels;

namespace YourBookStore.PL.Controllers;
public class CategoriesController : Controller
{
    private readonly IMapper _mapper;
    private readonly ICategoryService _categoryService;
    private readonly ILogger<GenresController> _logger;

    public CategoriesController(ICategoryService categoryService, IMapper mapper, ILogger<GenresController> logger)
    {
        _categoryService = categoryService;
        _mapper = mapper;
        _logger = logger;
    }

    [HttpGet("Categories")]
    [AllowAnonymous]
    public IActionResult Categories()
    {
        var categories = _categoryService.GetAllCategoriesNames().ToList();
        return View("Categories", categories);
    }
   
   
}
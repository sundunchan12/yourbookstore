﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using YourBookStore.BLL.Infrastructure.Enums;
using YourBookStore.BLL.Interfaces;
using YourBookStore.Domain.DTOs;
using YourBookStore.Domain.ViewModels;

using YourBookStore.BLL.Infrastructure.Extensions;
using YourBookStore.Domain.Entities;

namespace YourBookStore.PL.Controllers
{
    public class OrderController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IOrderService _orderService;

        public OrderController(IMapper mapper, IOrderService service)
        {
            _mapper = mapper;
            _orderService = service;
        }
        
        [HttpGet("Order/Ordering")]
        public ActionResult<OrderViewModel> Ordering(OrderViewModel orderInfo)
        {
            return View("Ordering", orderInfo);
        }

        [HttpPost("Order/New")]
        public async Task<ActionResult> MakeOrder(OrderViewModel orderInfo)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Ordering", "Order", orderInfo);
            }
            var order = HttpContext.Session.Get<OrderDTO>("basket");
            order.Email = orderInfo.Email;
            order.Phone = orderInfo.Phone;
            order.CardNumber = orderInfo.CardNumber;
            order.CardHolderName = orderInfo.CardHolderName;
            order.CVV = orderInfo.CVV;
            order.ExpirationDate = orderInfo.ExpirationsDate;
            order.FirstName = orderInfo.FirstName;
            order.SecondName = orderInfo.SecondName;
            order.PostalCode = orderInfo.PostalCode;
            order.ShipAddress = orderInfo.ShipAddress;
            
            var status = await _orderService.CreateOrderAsync(_mapper.Map<OrderDTO>(order));

             if (status.Item2 == CreateOrderStatus.Success)
             {
                 HttpContext.Session.Clear();
                 if(HttpContext.User.Identity.IsAuthenticated) return RedirectToAction("Profile", "Users");
                 return RedirectToAction("Index", "Home");
             }

            return RedirectToAction("GetBasketDetails", "Basket");
        }
        
        [HttpGet("Success")]
        public ActionResult<OrderViewModel> OrderDetails(OrderViewModel order)
        {
            if (!ModelState.IsValid && order != null)
            {
                return RedirectToAction("GetBasketDetails", "Basket");
            }
        
            return View("OrderDetails", order);
        }
        
    }
}

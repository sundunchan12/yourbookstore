﻿using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using YourBookStore.BLL.Interfaces;
using YourBookStore.Domain.DTOs;
using YourBookStore.Domain.ViewModels;

using YourBookStore.BLL.Infrastructure.Extensions;

namespace YourBookStore.PL.Controllers;



//[Route("[controller]")]
public class BasketController : Controller
{
    private readonly IOrderService _orderService;
    private readonly IMapper _mapper;
    private readonly ILogger<BasketController> _logger;
    private static IFormatter _serializer = new BinaryFormatter();

    public BasketController(IOrderService orderService, IMapper mapper, ILogger<BasketController> logger)
    {
        _orderService = orderService;
        _mapper = mapper;
        _logger = logger;
    }
    
    
    [HttpGet("Basket")]
    public ActionResult<OrderViewModel> GetBasketDetails()
    {

        var order = HttpContext.Session.Get<OrderDTO>("basket");
        
        return View("Basket", _mapper.Map<OrderViewModel>(order));
    }

    [HttpPost("Basket/Update")]
    public ActionResult<OrderViewModel> UpdateBasketDetails(OrderViewModel model)
    {
        if (!ModelState.IsValid)
        {
            return View("Basket", model);
        }
        var order = HttpContext.Session.Get<OrderDTO>("basket");
        var orderDetails = order.OrderDetails.FirstOrDefault(t => t.Code == model.OrderDetails.First().Code);

        if (orderDetails != null)
        {
            orderDetails.Quantity = model.OrderDetails.First().Quantity;
        }
        
        HttpContext.Session.Set("basket", order);
        
        return View("Basket", _mapper.Map<OrderViewModel>(order));
    }
    
    [HttpGet("Basket/Remove/{code}")]
    public ActionResult<OrderViewModel> RemoveBookFromBasketByCode(string code)
    {
        
        var order = HttpContext.Session.Get<OrderDTO>("basket");
        
        var orderDetail = order.OrderDetails.FirstOrDefault(o => o.Code == code);
        if(orderDetail is null) return RedirectToAction("GetBasketDetails"); 
        order.OrderDetails.Remove(orderDetail);
        
        HttpContext.Session.Set("basket", order);
        
        return RedirectToAction("GetBasketDetails");
    }
    
    [HttpGet("Basket/UpdateQuantity/{code}/{quantity}")]
    public ActionResult<OrderViewModel> UpdateBookQuantityByCode(string code, short quantity)
    {
        var order = HttpContext.Session.Get<OrderDTO>("basket");
        
        var orderDetail = order.OrderDetails.FirstOrDefault(o => o.Code == code);
        if(orderDetail is null || quantity < 1) return RedirectToAction("GetBasketDetails");
        orderDetail.Quantity = quantity;
        
        HttpContext.Session.Set("basket", order);
        
        return RedirectToAction("GetBasketDetails");
    }
}

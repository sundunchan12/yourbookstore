﻿namespace YourBookStore.DAL.EF.DBInitializer.InitializerVars;

internal static class InitializerVariables
    {
        #region UsersIds
        internal static readonly Guid adminId = Guid.NewGuid();
        internal static readonly Guid defaultId = Guid.NewGuid();
        #endregion

        #region CategoriesIds
        internal static readonly Guid fictionCategoryId = Guid.NewGuid();
        internal static readonly Guid nonFictionCategoryId = Guid.NewGuid();
        
        #endregion
        
        #region GenresIds
        internal static readonly Guid adventureGenreId= Guid.NewGuid();
        internal static readonly Guid detectiveGenreId = Guid.NewGuid();
        internal static readonly Guid fantasyGenreId = Guid.NewGuid();
        internal static readonly Guid dramaGenreId = Guid.NewGuid();
        internal static readonly Guid novelGenreId = Guid.NewGuid();
        internal static readonly Guid cookingGenreId = Guid.NewGuid();
        internal static readonly Guid popularScienceGenreId = Guid.NewGuid();
        internal static readonly Guid selfDevelopmentGenreId = Guid.NewGuid();
        internal static readonly Guid documentaryGenreId = Guid.NewGuid();
        internal static readonly Guid politicalGenreId = Guid.NewGuid();
        internal static readonly Guid psychologyGenreId = Guid.NewGuid();
        internal static readonly Guid philosophyGenreId = Guid.NewGuid();
      
        
        #endregion

        #region BooksIds
        internal static readonly Guid halfKingBookId = Guid.NewGuid();
        internal static readonly Guid blackSwanBookId = Guid.NewGuid();
        internal static readonly Guid afterBookId = Guid.NewGuid();
        internal static readonly Guid oneNineEightFourBookId = Guid.NewGuid();
        internal static readonly Guid mysterySixTwoTwoBookId = Guid.NewGuid();
        internal static readonly Guid charlieAndTheChocolateBookId = Guid.NewGuid();
        internal static readonly Guid schindlersListBookId = Guid.NewGuid();
        internal static readonly Guid fiveStepsToLoveBookId = Guid.NewGuid();
        internal static readonly Guid theQueensWalkBookId = Guid.NewGuid();
        internal static readonly Guid homeOfTheStrangeChilderBookId = Guid.NewGuid();
        internal static readonly Guid cityOfEmptyBookId = Guid.NewGuid();
        internal static readonly Guid paperTownsBookId = Guid.NewGuid();
        internal static readonly Guid theCollectorBookId = Guid.NewGuid();
        internal static readonly Guid itsGoingToHurtBookId = Guid.NewGuid();
        internal static readonly Guid nonViolentCommBookId = Guid.NewGuid();
        internal static readonly Guid sapiensBookId = Guid.NewGuid();
        internal static readonly Guid yuvalNoahBookId = Guid.NewGuid();
        internal static readonly Guid theGamesPeoplePlayBookId = Guid.NewGuid();
        internal static readonly Guid theFiveLoveBookId = Guid.NewGuid();
        internal static readonly Guid menAreFromMarsBookId = Guid.NewGuid();
        internal static readonly Guid vegetarianCuisineBookId = Guid.NewGuid();
        internal static readonly Guid beerEvolutionOfTasteBookId = Guid.NewGuid();
        internal static readonly Guid wineSimpleBookId = Guid.NewGuid();
        internal static readonly Guid homoDeusBookId = Guid.NewGuid();
        internal static readonly Guid lifeAfterGoogleBookId = Guid.NewGuid();
        internal static readonly Guid angerManagementBookId = Guid.NewGuid();
        internal static readonly Guid theChurchillFactorBookId = Guid.NewGuid();
        internal static readonly Guid theCaseOfVasilStusBookId = Guid.NewGuid();
        internal static readonly Guid vanGoghBookId = Guid.NewGuid();
        internal static readonly Guid theoriesInBookId = Guid.NewGuid();
        internal static readonly Guid scienceForTheSoulBookId = Guid.NewGuid();
        internal static readonly Guid polishCuisineBookId = Guid.NewGuid();
        internal static readonly Guid dontBeStupidBookId = Guid.NewGuid();
        internal static readonly Guid mothOfWarBookId = Guid.NewGuid();
        internal static readonly Guid bookAboutDepressionBookId = Guid.NewGuid();
        internal static readonly Guid animorphiaBookId = Guid.NewGuid();
        internal static readonly Guid theAlchemistBookId = Guid.NewGuid();
        internal static readonly Guid elevenTwentyTwoSixThreeBookId = Guid.NewGuid();
        internal static readonly Guid clockWorkOrangeBookId = Guid.NewGuid();
        internal static readonly Guid briefHistoryOfTimeBookId = Guid.NewGuid();
        internal static readonly Guid theChildYouMustBookId = Guid.NewGuid();
        internal static readonly Guid theSubtleArtOf = Guid.NewGuid();
        internal static readonly Guid flowersForAlgernonBookId = Guid.NewGuid();
        internal static readonly Guid hpChamberOfSecretsBookId = Guid.NewGuid();
        internal static readonly Guid hpPrisonerOfAzkabanBookId = Guid.NewGuid();
        internal static readonly Guid hpGobletOFFireBookId = Guid.NewGuid();
        internal static readonly Guid hpOrderOFThePhoenixBookId = Guid.NewGuid();
        internal static readonly Guid hpHalfBloodPrinceBookId = Guid.NewGuid();
        internal static readonly Guid hpPhilStoneBookId = Guid.NewGuid();
        internal static readonly Guid hoDeadlyHeirloomsBookId = Guid.NewGuid();
        internal static readonly Guid prideAndPrejudiceBookId = Guid.NewGuid();
        internal static readonly Guid fiveQuartersOfAnOrangeBookId = Guid.NewGuid();
        internal static readonly Guid theDaVinciCodeBookId = Guid.NewGuid();
        
        
        
         

        #endregion
        
        #region AuthorsIds
        internal static readonly Guid JoeAbercrombieAuthorId = Guid.NewGuid();
        internal static readonly Guid NicholasTalebAuthorId = Guid.NewGuid();
        internal static readonly Guid AnneToddAuthorId = Guid.NewGuid();
        internal static readonly Guid GeorgeOrwellAuthorId = Guid.NewGuid();
        internal static readonly Guid JoelDickerAuthorId = Guid.NewGuid();
        internal static readonly Guid RoaldDahlAuthorId = Guid.NewGuid();
        internal static readonly Guid ThomasKenillyAuthorId = Guid.NewGuid();
        internal static readonly Guid RachelLippincottAuthorId = Guid.NewGuid();
        internal static readonly Guid WalterTevisAuthorId = Guid.NewGuid();
        internal static readonly Guid RensomRiggsAuthorId = Guid.NewGuid();
        internal static readonly Guid JohnGreenAuthorId = Guid.NewGuid();
        internal static readonly Guid JohnFowlesAuthorId = Guid.NewGuid();
        internal static readonly Guid AdamKayeAuthorId = Guid.NewGuid();
        internal static readonly Guid MarshallRosenbergAuthorId = Guid.NewGuid();
        internal static readonly Guid EricByrneAuthorId = Guid.NewGuid();
        internal static readonly Guid GaryChapmanAuthorId = Guid.NewGuid();
        internal static readonly Guid JohnGrayAuthorId = Guid.NewGuid();
        internal static readonly Guid IrynaVasylenkoAuthorId = Guid.NewGuid();
        internal static readonly Guid NataliaWatsonAuthorId = Guid.NewGuid();
        internal static readonly Guid ChristineMulckeAuthorId = Guid.NewGuid();
        internal static readonly Guid YuvalNoahHarariAuthorId = Guid.NewGuid();
        internal static readonly Guid GeorgeGliderAuthorId = Guid.NewGuid();
        internal static readonly Guid DanDubravinAuthorId = Guid.NewGuid();
        internal static readonly Guid BorisJohnsonAuthorId = Guid.NewGuid();
        internal static readonly Guid VakhtangKipianiAuthorId = Guid.NewGuid();
        internal static readonly Guid IrvingStoneAuthorId = Guid.NewGuid();
        internal static readonly Guid PaulParsonsAuthorId = Guid.NewGuid();
        internal static readonly Guid RichardDawkinsAuthorId = Guid.NewGuid();
        internal static readonly Guid AnnaMoysenkoAuthorId = Guid.NewGuid();
        internal static readonly Guid JenCinceroAuthorId = Guid.NewGuid();
        internal static readonly Guid AlexanderKrasovitskyAuthorId = Guid.NewGuid();
        internal static readonly Guid SashaSkolichenkoAuthorId = Guid.NewGuid();
        internal static readonly Guid KirbyRosanesAuthorId = Guid.NewGuid();
        internal static readonly Guid PauloCoelhoAuthorId = Guid.NewGuid();
        internal static readonly Guid StephenKingAuthorId = Guid.NewGuid();
        internal static readonly Guid BurgressAnthonyAuthorId = Guid.NewGuid();
        internal static readonly Guid StephenHawkingAuthorId = Guid.NewGuid();
        internal static readonly Guid StephanieStahlAuthorId = Guid.NewGuid();
        internal static readonly Guid MarkManonAuthorId = Guid.NewGuid();
        internal static readonly Guid DanielleKeyesAuthorId = Guid.NewGuid();
        internal static readonly Guid JaneAustenAuthorId = Guid.NewGuid();
        internal static readonly Guid JoanHarrisAuthorId = Guid.NewGuid();
        internal static readonly Guid DanBrownAuthorId = Guid.NewGuid();
        internal static readonly Guid JoanneRowling = Guid.NewGuid();
       
        #endregion

        #region bookLanguagesIds
        internal static readonly Guid englishBookLanguageId = Guid.NewGuid();
        internal static readonly Guid russianBookLanguageId = Guid.NewGuid();
        internal static readonly Guid ukrainianBookLanguageId = Guid.NewGuid();
        #endregion

        #region ordersIds

        internal static readonly Guid orderOne = Guid.Parse("438de794-1ede-42fb-8d50-5f0e98939767");
        internal static readonly Guid orderSecond = Guid.Parse("e1f06da2-5eda-4668-887c-6f87bc92653e");
        
        #endregion
        
        #region Users
        internal static readonly string moderatorPassword = "KI1tvZIeiXKBLWJAC7ec4NWjoSkcaTJTdrRDMiwANs8=";
        internal static readonly byte[] moderatorPasswordSalt = new byte[] 
            { 
                67,  92,  15,  46, 
                125, 178, 137, 130, 
                148, 214, 71,  138, 
                132, 73,  134, 237
            };
        internal static readonly string adminPassword = "jovnJbl7fEPIm5e1JHo0gCIJHfDiTWQFT5a/XEf7fIo=";
        internal static readonly byte[] adminPasswordSalt = new byte[]
        {
            179, 137, 193, 187, 
            182, 238, 115, 74,
            37,  215, 68,  238, 
            63,  47,  56,  183
        };
        #endregion
}
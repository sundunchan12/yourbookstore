﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using YourBookStore.DAL.EF.DBInitializer.InitializerVars;
using YourBookStore.Domain.Entities;
using YourBookStore.DAL.EF.DBInitializer.InitializerVars;

namespace YourBookStore.DAL.EF.DBInitializer;

public class AuthorsInitializer : IEntityTypeConfiguration<Author>
{
    public void Configure(EntityTypeBuilder<Author> builder)
    {
        builder.HasData(
            new Author { Id = InitializerVariables.NicholasTalebAuthorId, FullName = "Нассим Таллеб"},
            new Author { Id = InitializerVariables.JoeAbercrombieAuthorId, FullName = "Джо Аберкромби"},
            new Author { Id = InitializerVariables.AnneToddAuthorId, FullName = "Анна Тодд"},
            new Author { Id = InitializerVariables.GeorgeOrwellAuthorId, FullName = "Джордж Орвелл"},
            new Author { Id = InitializerVariables.JoelDickerAuthorId, FullName = "Жоель Діккер"},
            new Author { Id = InitializerVariables.RoaldDahlAuthorId, FullName = "Роальд Дал"},
            new Author { Id = InitializerVariables.ThomasKenillyAuthorId, FullName = "Томас Кеніллі"},
            new Author { Id = InitializerVariables.RachelLippincottAuthorId, FullName = "Рейчел Ліппінкот"},
            new Author { Id = InitializerVariables.WalterTevisAuthorId, FullName = "Вальтер Тевіс"},
            new Author { Id = InitializerVariables.RensomRiggsAuthorId, FullName = "Ренсом Ріггз"},
            new Author { Id = InitializerVariables.JohnGreenAuthorId, FullName = "Джон Грін"},
            new Author { Id = InitializerVariables.JohnFowlesAuthorId, FullName = "Джон Фаулз"},
            new Author { Id = InitializerVariables.AdamKayeAuthorId, FullName = "Адам Кей"},
            new Author { Id = InitializerVariables.MarshallRosenbergAuthorId, FullName = "Маршалл Розенберг"},
            new Author { Id = InitializerVariables.YuvalNoahHarariAuthorId, FullName = "Ювал Ной Харарі"},
            new Author { Id = InitializerVariables.EricByrneAuthorId, FullName = "Ерік Берн"},
            new Author { Id = InitializerVariables.GaryChapmanAuthorId, FullName = "Гєри Чепмен"},
            new Author { Id = InitializerVariables.JohnGrayAuthorId, FullName = "Джон Грей"},
            new Author { Id = InitializerVariables.IrynaVasylenkoAuthorId, FullName = "Ірина Василенко"},
            new Author { Id = InitializerVariables.NataliaWatsonAuthorId, FullName = "Наталя Вотсон"},
            new Author { Id = InitializerVariables.ChristineMulckeAuthorId, FullName = "Крістін Мюльке"},
            new Author { Id = InitializerVariables.GeorgeGliderAuthorId, FullName = "Джордж Гілдер"},
            new Author { Id = InitializerVariables.DanDubravinAuthorId, FullName = "Ден Дубравін"},
            new Author { Id = InitializerVariables.BorisJohnsonAuthorId, FullName = "Боріс Джонсон"},
            new Author { Id = InitializerVariables.VakhtangKipianiAuthorId, FullName = "Вахтанг Кіпіані"},
            new Author { Id = InitializerVariables.IrvingStoneAuthorId, FullName = "Ірвінг Стоун"},
            new Author { Id = InitializerVariables.PaulParsonsAuthorId, FullName = "Пол Парсонс"},
            new Author { Id = InitializerVariables.RichardDawkinsAuthorId, FullName = "Річард Докінз"},
            new Author { Id = InitializerVariables.AnnaMoysenkoAuthorId, FullName = "Анна Мойсеенко"},
            new Author { Id = InitializerVariables.JenCinceroAuthorId, FullName = "Джен Сінсеро"},
            new Author { Id = InitializerVariables.AlexanderKrasovitskyAuthorId, FullName = "Олександр Красовицький"},
            new Author { Id = InitializerVariables.SashaSkolichenkoAuthorId, FullName = "Саша Сколиченко"},
            new Author { Id = InitializerVariables.KirbyRosanesAuthorId, FullName = "Кербі Розанес"},
            new Author { Id = InitializerVariables.PauloCoelhoAuthorId, FullName = "Пауло Коєльо"},
            new Author { Id = InitializerVariables.StephenKingAuthorId, FullName = "Стівен Кінг"},
            new Author { Id = InitializerVariables.BurgressAnthonyAuthorId, FullName = "Берджесс Ентоні"},
            new Author { Id = InitializerVariables.StephenHawkingAuthorId, FullName = "Стівен Хокінг"},
            new Author { Id = InitializerVariables.StephanieStahlAuthorId, FullName = "Стефані Шталь"},
            new Author { Id = InitializerVariables.MarkManonAuthorId, FullName = "Марк Менсон"},
            new Author { Id = InitializerVariables.DanielleKeyesAuthorId, FullName = "Деніел Кіз"},
            new Author { Id = InitializerVariables.JoanneRowling, FullName = "Джоан Роулінг"},
            new Author { Id = InitializerVariables.JaneAustenAuthorId, FullName = "Джейн Остен"},
            new Author { Id = InitializerVariables.JoanHarrisAuthorId, FullName = "Джоан Гарріс"},
            new Author { Id = InitializerVariables.DanBrownAuthorId, FullName = "Ден Браун"}
        );
    }
}
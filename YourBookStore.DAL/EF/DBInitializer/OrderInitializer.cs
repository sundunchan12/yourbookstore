﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using YourBookStore.DAL.EF.DBInitializer.InitializerVars;
using YourBookStore.Domain.Entities;
using YourBookStore.Utility.GlobalEnums;

namespace YourBookStore.DAL.EF.DBInitializer;

public class OrdersInitializer : IEntityTypeConfiguration<Order>
{
    public void Configure(EntityTypeBuilder<Order> builder)
    {
        builder.HasData(
            new Order
            {
                Id = InitializerVariables.orderOne ,
                CustomerId = InitializerVariables.defaultId, 
                OrderDate = DateTime.Now, 
                Status = "Очікує підтверждення", 
                ShipAddress = "Харків, Майдан Незалежності 14",
                PostalCode = "61015",
                FirstName = "Іван",
                SecondName = "Сірко",
                Email = "user@gmail.com",
                Phone = "+38099445165"
            },
            new Order
            {
                Id = InitializerVariables.orderSecond,
                CustomerId = InitializerVariables.defaultId, 
                OrderDate = DateTime.Now, 
                Status = "Доставлено", 
                ShipAddress = "Київ, Майдан Незалежності 35",
                PostalCode = "61600",
                FirstName = "Admin",
                SecondName = "Also admin",
                Email = "admin@gmail.com",
                Phone = "+380671239253"
            }
        );
    }
}
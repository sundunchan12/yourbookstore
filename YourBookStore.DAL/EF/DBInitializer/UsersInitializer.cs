﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using YourBookStore.DAL.EF.DBInitializer.InitializerVars;
using YourBookStore.Domain.Entities;
using YourBookStore.Utility.GlobalEnums;

namespace YourBookStore.DAL.EF.DBInitializer;

public class UsersInitializer : IEntityTypeConfiguration<User>
{
    public void Configure(EntityTypeBuilder<User> builder)
    {
        builder.HasData(
            new User
            {
                Id = InitializerVariables.adminId, 
                Email = "admin@gmail.com", 
                FirstName = "Admin", 
                LastName = "Also admin", 
                Password = InitializerVariables.adminPassword, 
                StoredSalt = InitializerVariables.adminPasswordSalt,
                Phone = "+38099445165",
                City = "Харків",
                PostalCode = "61015",
                Role = UserRoles.Administrator.ToString()
            },
            new User
            {
                Id = InitializerVariables.defaultId, 
                Email = "user@gmail.com", 
                FirstName = "Іван", 
                LastName = "Сірко", 
                Password = InitializerVariables.moderatorPassword, 
                StoredSalt = InitializerVariables.moderatorPasswordSalt,
                Phone = "+380671239253",
                City = "Київ",
                PostalCode = "61600",
                Role = UserRoles.User.ToString()
            }
        );
    }
}
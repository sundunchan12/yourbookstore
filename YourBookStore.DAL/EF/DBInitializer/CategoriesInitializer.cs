﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using YourBookStore.DAL.EF.DBInitializer.InitializerVars;
using YourBookStore.Domain.Entities;

namespace YourBookStore.DAL.EF.DBInitializer;

public class CategoriesInitializer : IEntityTypeConfiguration<Category>
{
    public void Configure(EntityTypeBuilder<Category> builder)
    {
        builder.HasData(
            new Category { Id = InitializerVariables.fictionCategoryId, Name = "Фікшн література", },
            new Category { Id = InitializerVariables.nonFictionCategoryId, Name = "Нон-фікшн література" }
        );
    }
}
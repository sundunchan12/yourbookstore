﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using YourBookStore.DAL.EF.DBInitializer.InitializerVars;
using YourBookStore.Domain.Entities;

namespace YourBookStore.DAL.EF.DBInitializer;

public class OrderDetailsInitializer : IEntityTypeConfiguration<OrderDetails>
{
    public void Configure(EntityTypeBuilder<OrderDetails> builder)
    {
        builder.HasData(
            new OrderDetails { Price = 120, BookId = InitializerVariables.itsGoingToHurtBookId, Quantity = 1, OrderId = InitializerVariables.orderOne },
            new OrderDetails { Price = 240, BookId = InitializerVariables.theDaVinciCodeBookId, Quantity = 1, OrderId = InitializerVariables.orderOne },
            new OrderDetails { Price = 870, BookId = InitializerVariables.halfKingBookId, Quantity = 3, OrderId = InitializerVariables.orderSecond }
        );
    }
}
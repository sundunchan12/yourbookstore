﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using YourBookStore.DAL.EF.DBInitializer.InitializerVars;
using YourBookStore.Domain.Entities;

namespace YourBookStore.DAL.EF.DBInitializer;

public class LanguagesInitializer : IEntityTypeConfiguration<Language>
{
    public void Configure(EntityTypeBuilder<Language> builder)
    {
        builder.HasData(
            new Language { Id = InitializerVariables.ukrainianBookLanguageId, Name = "Українська", },
            new Language { Id = InitializerVariables.russianBookLanguageId, Name = "Російська" },
            new Language { Id = InitializerVariables.englishBookLanguageId, Name = "Англійська" }
        );
    }
}
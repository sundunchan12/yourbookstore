﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using YourBookStore.DAL.EF.DBInitializer.InitializerVars;
using YourBookStore.Domain.Entities;

namespace YourBookStore.DAL.EF.DBInitializer;

 public class GenresInitializer : IEntityTypeConfiguration<Genre>
    {
        public void Configure(EntityTypeBuilder<Genre> builder)
        {
            builder.HasData(
                new Genre { Id = InitializerVariables.adventureGenreId, Name = "Пригоди", CategoryId = InitializerVariables.fictionCategoryId},
                new Genre { Id = InitializerVariables.detectiveGenreId, Name = "Детектив", CategoryId = InitializerVariables.fictionCategoryId},
                new Genre { Id = InitializerVariables.fantasyGenreId, Name = "Фантастика", CategoryId = InitializerVariables.fictionCategoryId},
                new Genre { Id = InitializerVariables.dramaGenreId, Name = "Драма", CategoryId = InitializerVariables.fictionCategoryId},
                new Genre { Id = InitializerVariables.novelGenreId, Name = "Роман", CategoryId = InitializerVariables.fictionCategoryId},
                new Genre { Id = InitializerVariables.cookingGenreId, Name = "Кулінарія", CategoryId = InitializerVariables.nonFictionCategoryId},
                new Genre { Id = InitializerVariables.popularScienceGenreId, Name = "Науково-популярна література", CategoryId = InitializerVariables.nonFictionCategoryId},
                new Genre { Id = InitializerVariables.documentaryGenreId, Name = "Документальна література", CategoryId = InitializerVariables.nonFictionCategoryId},
                new Genre { Id = InitializerVariables.selfDevelopmentGenreId, Name = "Саморозвиток", CategoryId = InitializerVariables.nonFictionCategoryId},
                new Genre { Id = InitializerVariables.politicalGenreId, Name = "Політика", CategoryId = InitializerVariables.nonFictionCategoryId},
                new Genre { Id = InitializerVariables.psychologyGenreId, Name = "Псилохолія", CategoryId = InitializerVariables.nonFictionCategoryId},
                new Genre { Id = InitializerVariables.philosophyGenreId, Name = "Філософія", CategoryId = InitializerVariables.nonFictionCategoryId}
            );
        }
    }
﻿using Microsoft.EntityFrameworkCore;
using  Microsoft.EntityFrameworkCore.Proxies;
using YourBookStore.DAL.EF.DBInitializer;
using YourBookStore.Domain.Entities;

namespace YourBookStore.DAL.EF;

public class YourBookStoreDBContext : DbContext
{
    public DbSet<User> Users { get; set; }
    public DbSet<Book> Books { get; set; }
    public DbSet<Genre> Genres { get; set; }
    public DbSet<Category> Categories { get; set; }
    public DbSet<Author> Authors { get; set; }
    public DbSet<Order> Orders { get; set; }
    public DbSet<Language> Languages { get; set; }
    public DbSet<OrderDetails> OrderDetails { get; set; }
    public DbSet<BookGenres> BookGenres { get; set; }

    public YourBookStoreDBContext(DbContextOptions<YourBookStoreDBContext> options) : base(options) {}

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseLazyLoadingProxies();
        optionsBuilder.UseSqlServer("Server=localhost;Database=YourBookStore;Trusted_Connection=True", builder =>
        {
            builder.EnableRetryOnFailure(5, TimeSpan.FromSeconds(10), null);
        });
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        //data base data init
        //
         modelBuilder.ApplyConfiguration(new AuthorsInitializer());
         modelBuilder.ApplyConfiguration(new LanguagesInitializer());
         modelBuilder.ApplyConfiguration(new GenresInitializer());
         modelBuilder.ApplyConfiguration(new CategoriesInitializer());
         modelBuilder.ApplyConfiguration(new BookGenresInitializer());
         modelBuilder.ApplyConfiguration(new BooksInitializer());
         modelBuilder.ApplyConfiguration(new UsersInitializer());
         modelBuilder.ApplyConfiguration(new OrdersInitializer());
         modelBuilder.ApplyConfiguration(new OrderDetailsInitializer());
        
        modelBuilder.Entity<OrderDetails>()
            .HasKey(od => new { od.BookId, od.OrderId });

        

        modelBuilder.Entity<OrderDetails>()
            .HasOne(bc => bc.Order)
            .WithMany(o => o.OrderDetails)
            .HasForeignKey(bc => bc.OrderId)
            .OnDelete(DeleteBehavior.NoAction);
        
        modelBuilder.Entity<BookGenres>()
            .HasKey(od => new { od.BookId, od.GenreId });
        
        modelBuilder.Entity<BookGenres>()
            .HasOne(bg => bg.Book)
            .WithMany(b => b.BookGenres)
            .HasForeignKey(bg => bg.BookId)
            .OnDelete(DeleteBehavior.NoAction);
        
        modelBuilder.Entity<BookGenres>()
            .HasOne(bg => bg.Genre)
            .WithMany(g => g.BookGenres)
            .HasForeignKey(bg => bg.GenreId)
            .OnDelete(DeleteBehavior.NoAction);
    }
}
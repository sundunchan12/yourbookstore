﻿using YourBookStore.DAL.EF;
using YourBookStore.DAL.Interfaces;

namespace YourBookStore.DAL;

public class UnitOfWork : IUnitOfWork
{
    private readonly YourBookStoreDBContext _context;

    public UnitOfWork(YourBookStoreDBContext context) 
    {
        _context = context;
    }

    public void Dispose()
    {
        _context.Dispose();
    }

    public async Task SaveAsync()
    {
        await _context.SaveChangesAsync();
    }
}
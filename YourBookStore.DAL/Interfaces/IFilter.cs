﻿namespace YourBookStore.DAL.Interfaces;

public interface IFilter<T, F>
{
    public T DoFilter(T items, F filter);
}
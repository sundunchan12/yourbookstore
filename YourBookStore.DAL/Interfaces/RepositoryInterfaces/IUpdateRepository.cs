﻿namespace YourBookStore.DAL.Interfaces.RepositoryInterfaces;

public interface IUpdateRepository<TEntity>
{
    public Task AddAsync(TEntity entity);

    public Task<TEntity> AddAndReturnResultAsync(TEntity entity);

    public Task UpdateAsync(TEntity entity);

    public Task DeleteAsync(TEntity entity);

    public Task DeleteByIdAsync(Guid id);
}
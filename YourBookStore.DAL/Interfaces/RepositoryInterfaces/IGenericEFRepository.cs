﻿namespace YourBookStore.DAL.Interfaces.RepositoryInterfaces;

public interface IGenericEFRepository<TEntity> : IUpdateRepository<TEntity>, IReadOnlyRepository<TEntity>
{
    public IQueryable<TEntity> GetAllAsNoTracking();
}
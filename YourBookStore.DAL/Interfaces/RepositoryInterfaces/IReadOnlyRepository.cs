﻿namespace YourBookStore.DAL.Interfaces.RepositoryInterfaces;

public interface IReadOnlyRepository<TEntity>
{
    public IQueryable<TEntity> GetAll();

    public IEnumerable<TEntity> Get(Func<TEntity, bool> predicate);

    public Task<TEntity> GetByIdAsync(Guid id);
}
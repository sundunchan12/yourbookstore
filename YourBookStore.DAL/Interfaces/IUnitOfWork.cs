﻿namespace YourBookStore.DAL.Interfaces;

public interface IUnitOfWork : IDisposable
{

    public Task SaveAsync();
}
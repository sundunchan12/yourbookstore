﻿using Microsoft.EntityFrameworkCore;
using YourBookStore.DAL.EF;
using YourBookStore.DAL.Interfaces.RepositoryInterfaces;
using YourBookStore.Utility.GlobalConstants;


namespace YourBookStore.DAL.Repositories;

public class GenericRepository<TEntity> : IGenericEFRepository<TEntity> where TEntity : class
{
    private readonly YourBookStoreDBContext _context;
    private readonly DbSet<TEntity> _dbSet;

    public GenericRepository(YourBookStoreDBContext context)
    {
        _context = context;
        _dbSet = context.Set<TEntity>();
    }

    public IQueryable<TEntity> GetAllAsNoTracking()
    {
        return _dbSet;
    }

    public IEnumerable<TEntity> Get(Func<TEntity, bool> predicate)
    {
        if (predicate == null)
        {
            throw new ArgumentNullException(nameof(predicate), ExceptionMessages.GetShouldNotBeMessage("Predicate"));
        }

        return _dbSet.Where(predicate);
    }
    public async Task<TEntity> GetByIdAsync(Guid id)
        {
            if (id == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(id), ExceptionMessages.EntityIdNullMessage);
            }

            return await _dbSet.FindAsync(id);
        }

        public async Task UpdateAsync(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity), ExceptionMessages.EntityNullMessage);
            }

            await Task.FromResult(_context.Entry(entity).State = EntityState.Modified);
        }

        public void Add(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity), ExceptionMessages.EntityNullMessage);
            }

            _dbSet.Add(entity);
        }

        public async Task AddAsync(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity), ExceptionMessages.EntityNullMessage);
            }

            await _dbSet.AddAsync(entity);
        }

        public async Task<TEntity> AddAndReturnResultAsync(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity), ExceptionMessages.EntityNullMessage);
            }

            return await Task.FromResult(_dbSet.AddAsync(entity).Result.Entity);
        }

        public async Task DeleteAsync(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity), ExceptionMessages.EntityNullMessage);
            }

            await Task.FromResult(_dbSet.Remove(entity));
        }

        public async Task DeleteByIdAsync(Guid id)
        {
            TEntity entity = await _dbSet.FindAsync(id);

            if (entity == null)
            {
                throw new ArgumentNullException(nameof(id), ExceptionMessages.EntityIdNullMessage);
            }

            _dbSet.Remove(entity);
        }

        public IQueryable<TEntity> GetAll()
        {
            return _dbSet;
        }

}

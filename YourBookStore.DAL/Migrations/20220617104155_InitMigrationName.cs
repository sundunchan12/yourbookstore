﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace YourBookStore.DAL.Migrations
{
    public partial class InitMigrationName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("0b2cceb2-629d-46db-9578-b3d2b96e92cd"), new Guid("1802fdd2-c264-473b-afb6-5ece9753d527") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("0e9ce81e-0296-4c2b-9302-255a24ef999f"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("1365032e-6311-4f78-8118-bd441aaccee2"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("1365032e-6311-4f78-8118-bd441aaccee2"), new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("13e2ae3c-09b5-46c3-868d-a0cb8e30801e"), new Guid("632a9f47-5efb-45ac-b1f1-72e61eb4d40d") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("13e2ae3c-09b5-46c3-868d-a0cb8e30801e"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("17c9218e-04b7-47fc-969e-53872d8ac017"), new Guid("615f3a31-17d3-42d5-be5f-c466eae26a06") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("1c2c89ce-86c8-4751-8917-247052fab5d7"), new Guid("aba782f8-1257-4c0d-a2a1-a633397a7655") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("1fd7bd85-befa-4d7d-9ff1-671eee40ff13"), new Guid("1802fdd2-c264-473b-afb6-5ece9753d527") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("2341255d-748a-4e7f-a0de-886c9dadabdd"), new Guid("48ebbfc6-bf96-4f39-b1ed-6c1d1aaeb55c") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("2341255d-748a-4e7f-a0de-886c9dadabdd"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("2341255d-748a-4e7f-a0de-886c9dadabdd"), new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("23f52275-0d2d-4363-9e8b-ce64a7eb7a7f"), new Guid("632a9f47-5efb-45ac-b1f1-72e61eb4d40d") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("23f52275-0d2d-4363-9e8b-ce64a7eb7a7f"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("2eab26b3-7493-4192-879e-fe499b121646"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("2eab26b3-7493-4192-879e-fe499b121646"), new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("329ff545-1784-4dc0-b9ee-15bff62ae53d"), new Guid("aba782f8-1257-4c0d-a2a1-a633397a7655") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("33e093c6-0d8c-4917-a6ab-aa4e63745c91"), new Guid("632a9f47-5efb-45ac-b1f1-72e61eb4d40d") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("33e093c6-0d8c-4917-a6ab-aa4e63745c91"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("33e093c6-0d8c-4917-a6ab-aa4e63745c91"), new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("3850c786-2726-4166-90f8-f4ba9d05836e"), new Guid("70276efb-6e62-4303-bda8-23601d6c3824") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("3b263589-953d-430d-a22d-26d51fa77c05"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("3b263589-953d-430d-a22d-26d51fa77c05"), new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("3d918f1a-d700-4c81-a5fa-06b66e893658"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("4183bc98-b2ca-471e-9d31-246ebf86f28f"), new Guid("aba782f8-1257-4c0d-a2a1-a633397a7655") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("45ee2451-3709-4c53-9991-f01d240687eb"), new Guid("1802fdd2-c264-473b-afb6-5ece9753d527") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("4a635251-3a99-429d-99a0-5f0d0e7dea25"), new Guid("615f3a31-17d3-42d5-be5f-c466eae26a06") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("516ca249-5be1-4661-abdd-58aa7f1d22cc"), new Guid("1802fdd2-c264-473b-afb6-5ece9753d527") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("55b276db-ec5a-4832-8aa5-60b1c68eae0f"), new Guid("632a9f47-5efb-45ac-b1f1-72e61eb4d40d") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("55b276db-ec5a-4832-8aa5-60b1c68eae0f"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("55b276db-ec5a-4832-8aa5-60b1c68eae0f"), new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("59cb50ed-8d0c-4c70-86d6-8c3730c6c434"), new Guid("3e040948-5dbf-4c4e-b77e-b992223ce869") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("630bfd2f-0691-46cb-9287-499a37610447"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("630bfd2f-0691-46cb-9287-499a37610447"), new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("64d5b701-d3ad-457b-9f8f-eeb001a75906"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("64d5b701-d3ad-457b-9f8f-eeb001a75906"), new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("6563cdb6-1d94-48b5-b24f-2fb3f7459bfe"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("6b0880ba-7add-4ec0-a560-348ad48590dc"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("6b0880ba-7add-4ec0-a560-348ad48590dc"), new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("7018f385-d5d5-4586-b797-93a5586e56a9"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("7018f385-d5d5-4586-b797-93a5586e56a9"), new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("7335982a-0762-43e1-9411-1a75ff1c577a"), new Guid("fbeff58e-b5ea-40bc-b987-b64f65730135") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("7cedb874-c290-491e-9be7-c61eb8cb0dff"), new Guid("1802fdd2-c264-473b-afb6-5ece9753d527") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("81e399ec-ec52-4287-92b2-cfddef7c7832"), new Guid("70276efb-6e62-4303-bda8-23601d6c3824") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("8b04ff96-45b9-4a9e-bb2e-ca1eee1557e9"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("8b04ff96-45b9-4a9e-bb2e-ca1eee1557e9"), new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("8e52cab5-47ec-48e6-905f-91ee506ac22c"), new Guid("3e040948-5dbf-4c4e-b77e-b992223ce869") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("9fb43d5a-d5d7-4f82-8524-877e378709dc"), new Guid("615f3a31-17d3-42d5-be5f-c466eae26a06") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("9fb43d5a-d5d7-4f82-8524-877e378709dc"), new Guid("70276efb-6e62-4303-bda8-23601d6c3824") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("a01c6032-6c3d-44c9-9eab-766b49e13199"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("a01c6032-6c3d-44c9-9eab-766b49e13199"), new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("c35d3f62-16a4-48a7-af9f-3a337fe703fe"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("c35d3f62-16a4-48a7-af9f-3a337fe703fe"), new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("c51700ae-e4f6-4e6f-83f3-6d960560c12c"), new Guid("70276efb-6e62-4303-bda8-23601d6c3824") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("c6b657da-4561-4d04-a50a-5c578dc28d84"), new Guid("615f3a31-17d3-42d5-be5f-c466eae26a06") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("c7521805-6d35-4902-93b1-d9e843fe699e"), new Guid("aba782f8-1257-4c0d-a2a1-a633397a7655") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("c81da85c-3dcf-43f8-a378-973bce0b1341"), new Guid("48ebbfc6-bf96-4f39-b1ed-6c1d1aaeb55c") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("c81da85c-3dcf-43f8-a378-973bce0b1341"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("c9eb874e-cb85-49b5-96b1-19944d0b06a0"), new Guid("48ebbfc6-bf96-4f39-b1ed-6c1d1aaeb55c") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("c9eb874e-cb85-49b5-96b1-19944d0b06a0"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("cadce17d-1091-4da4-aaf6-328d3de418d2"), new Guid("48ebbfc6-bf96-4f39-b1ed-6c1d1aaeb55c") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("cadce17d-1091-4da4-aaf6-328d3de418d2"), new Guid("632a9f47-5efb-45ac-b1f1-72e61eb4d40d") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("cadce17d-1091-4da4-aaf6-328d3de418d2"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("cc7a2bb3-b2a1-4dc4-9710-f84e73e27dc0"), new Guid("3e040948-5dbf-4c4e-b77e-b992223ce869") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("d4151b28-0f7e-485f-b315-48d8133b7ba3"), new Guid("48ebbfc6-bf96-4f39-b1ed-6c1d1aaeb55c") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("d4151b28-0f7e-485f-b315-48d8133b7ba3"), new Guid("699e5d59-067e-4d2a-8f31-c40b80dd63c8") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("d4151b28-0f7e-485f-b315-48d8133b7ba3"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("d9d5ed67-6f0e-4937-a11f-5cb08da82e1a"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("d9d5ed67-6f0e-4937-a11f-5cb08da82e1a"), new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("dff9d08f-a5eb-4b48-8ad0-e5514ad6ce88"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("dff9d08f-a5eb-4b48-8ad0-e5514ad6ce88"), new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("e2c8715a-2f25-49ba-806d-efc9f28d4bd9"), new Guid("3e040948-5dbf-4c4e-b77e-b992223ce869") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("ef5d4786-384a-49d8-bf97-9bcabe7482a6"), new Guid("70276efb-6e62-4303-bda8-23601d6c3824") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("f6c5a42f-5c05-41b6-9c5a-f4f12ed60c6b"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("f8020fe9-51ab-43e9-8c5b-7f89926ba5cb"), new Guid("3e040948-5dbf-4c4e-b77e-b992223ce869") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("fa7b67d1-f00c-4d13-818d-23c1db93a2dc"), new Guid("615f3a31-17d3-42d5-be5f-c466eae26a06") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("fa7b67d1-f00c-4d13-818d-23c1db93a2dc"), new Guid("70276efb-6e62-4303-bda8-23601d6c3824") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("fec0e801-9a7a-41f8-baf5-67e1c4b540c5"), new Guid("1802fdd2-c264-473b-afb6-5ece9753d527") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("ff0293d0-2109-4014-836d-d15e9cd5bc3a"), new Guid("48ebbfc6-bf96-4f39-b1ed-6c1d1aaeb55c") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("ff0293d0-2109-4014-836d-d15e9cd5bc3a"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("ff6f1b2a-b970-42d9-9924-143cf21cc2dc"), new Guid("615f3a31-17d3-42d5-be5f-c466eae26a06") });

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: new Guid("c51ef25c-3e3b-4a1d-9ed0-e9fef963242e"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumns: new[] { "BookId", "OrderId" },
                keyValues: new object[] { new Guid("13e2ae3c-09b5-46c3-868d-a0cb8e30801e"), new Guid("438de794-1ede-42fb-8d50-5f0e98939767") });

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumns: new[] { "BookId", "OrderId" },
                keyValues: new object[] { new Guid("c35d3f62-16a4-48a7-af9f-3a337fe703fe"), new Guid("e1f06da2-5eda-4668-887c-6f87bc92653e") });

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumns: new[] { "BookId", "OrderId" },
                keyValues: new object[] { new Guid("e2c8715a-2f25-49ba-806d-efc9f28d4bd9"), new Guid("438de794-1ede-42fb-8d50-5f0e98939767") });

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("a1ad8f8d-33f0-4f91-90d5-102415ce770f"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("ab8c180b-8bec-43f1-ae3d-c0d7c2a07008"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("0b2cceb2-629d-46db-9578-b3d2b96e92cd"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("0e9ce81e-0296-4c2b-9302-255a24ef999f"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("1365032e-6311-4f78-8118-bd441aaccee2"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("13e2ae3c-09b5-46c3-868d-a0cb8e30801e"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("17c9218e-04b7-47fc-969e-53872d8ac017"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("1c2c89ce-86c8-4751-8917-247052fab5d7"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("1fd7bd85-befa-4d7d-9ff1-671eee40ff13"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("2341255d-748a-4e7f-a0de-886c9dadabdd"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("23f52275-0d2d-4363-9e8b-ce64a7eb7a7f"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("2eab26b3-7493-4192-879e-fe499b121646"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("329ff545-1784-4dc0-b9ee-15bff62ae53d"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("33e093c6-0d8c-4917-a6ab-aa4e63745c91"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("3850c786-2726-4166-90f8-f4ba9d05836e"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("3b263589-953d-430d-a22d-26d51fa77c05"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("3d918f1a-d700-4c81-a5fa-06b66e893658"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("4183bc98-b2ca-471e-9d31-246ebf86f28f"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("45ee2451-3709-4c53-9991-f01d240687eb"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("4a635251-3a99-429d-99a0-5f0d0e7dea25"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("516ca249-5be1-4661-abdd-58aa7f1d22cc"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("55b276db-ec5a-4832-8aa5-60b1c68eae0f"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("59cb50ed-8d0c-4c70-86d6-8c3730c6c434"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("630bfd2f-0691-46cb-9287-499a37610447"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("64d5b701-d3ad-457b-9f8f-eeb001a75906"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("6563cdb6-1d94-48b5-b24f-2fb3f7459bfe"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("6b0880ba-7add-4ec0-a560-348ad48590dc"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("7018f385-d5d5-4586-b797-93a5586e56a9"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("7335982a-0762-43e1-9411-1a75ff1c577a"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("7cedb874-c290-491e-9be7-c61eb8cb0dff"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("81e399ec-ec52-4287-92b2-cfddef7c7832"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("8b04ff96-45b9-4a9e-bb2e-ca1eee1557e9"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("8e52cab5-47ec-48e6-905f-91ee506ac22c"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("9fb43d5a-d5d7-4f82-8524-877e378709dc"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("a01c6032-6c3d-44c9-9eab-766b49e13199"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("c35d3f62-16a4-48a7-af9f-3a337fe703fe"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("c51700ae-e4f6-4e6f-83f3-6d960560c12c"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("c6b657da-4561-4d04-a50a-5c578dc28d84"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("c7521805-6d35-4902-93b1-d9e843fe699e"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("c81da85c-3dcf-43f8-a378-973bce0b1341"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("c9eb874e-cb85-49b5-96b1-19944d0b06a0"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("cadce17d-1091-4da4-aaf6-328d3de418d2"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("cc7a2bb3-b2a1-4dc4-9710-f84e73e27dc0"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("d4151b28-0f7e-485f-b315-48d8133b7ba3"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("d9d5ed67-6f0e-4937-a11f-5cb08da82e1a"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("dff9d08f-a5eb-4b48-8ad0-e5514ad6ce88"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("e2c8715a-2f25-49ba-806d-efc9f28d4bd9"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("ef5d4786-384a-49d8-bf97-9bcabe7482a6"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("f6c5a42f-5c05-41b6-9c5a-f4f12ed60c6b"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("f8020fe9-51ab-43e9-8c5b-7f89926ba5cb"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("fa7b67d1-f00c-4d13-818d-23c1db93a2dc"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("fec0e801-9a7a-41f8-baf5-67e1c4b540c5"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("ff0293d0-2109-4014-836d-d15e9cd5bc3a"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("ff6f1b2a-b970-42d9-9924-143cf21cc2dc"));

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: new Guid("1802fdd2-c264-473b-afb6-5ece9753d527"));

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: new Guid("3e040948-5dbf-4c4e-b77e-b992223ce869"));

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: new Guid("48ebbfc6-bf96-4f39-b1ed-6c1d1aaeb55c"));

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: new Guid("615f3a31-17d3-42d5-be5f-c466eae26a06"));

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: new Guid("632a9f47-5efb-45ac-b1f1-72e61eb4d40d"));

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: new Guid("699e5d59-067e-4d2a-8f31-c40b80dd63c8"));

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: new Guid("70276efb-6e62-4303-bda8-23601d6c3824"));

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1"));

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: new Guid("aba782f8-1257-4c0d-a2a1-a633397a7655"));

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5"));

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: new Guid("fbeff58e-b5ea-40bc-b987-b64f65730135"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("003966d2-f5fd-45a2-9973-8f53f355b374"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("04d6e5bf-ce01-4d24-a858-978abcd391a1"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("0a697f2a-9c3c-47b6-8fb3-4cfc7066fd83"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("0ee07cb8-2b4f-49cd-ac3f-1bdd6f5a5bbc"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("0f208bab-8a58-4d6f-85cd-476f25a6145e"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("15069387-a9c3-43a2-ad25-8f7493b37410"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("192a13dc-d53f-4b84-acad-951df2244351"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("1e747a08-731f-4723-80da-01a0dc3dff26"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("20ca17d1-2f41-4e58-b7b5-0b5a7c8724ce"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("254ac9fa-05b1-4ac5-9aec-9b36bba55167"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("29959a5f-3263-47c3-985a-42441771de74"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("3299e2da-00c6-4ac0-8d8e-06e9b6632203"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("38e393aa-2f14-41e4-9894-8d212f07400d"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("50170e48-862f-45f2-9fba-60bfd74c5ae8"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("5f8b0ecb-2247-4725-9b5e-63e4edb8b749"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("6345ac34-35f7-45e2-9dda-841894f9661f"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("64b80a39-72b3-49e5-8660-de4fb8e368e4"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("6ee6d936-9f76-4eb0-8fc0-dd51cbebbecc"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("7c440fd0-6b3d-4d30-86a9-d87b810a8e64"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("87254d48-87de-4719-b811-1ce2d8f8513e"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("8b0384f2-3950-4d3f-96ad-17a597154149"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("96f6510c-154b-4459-b9a1-194ea20526c7"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("97106cd6-8c27-4306-bf90-9d14bdd32f75"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("9ac72d18-80c2-44f9-9101-f50fb405f3ab"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("9d239583-1b8b-47e6-8102-556822f5eeab"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("9f39ee28-f276-4815-81bd-de8b4f96d0cd"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("a7f3f865-d45b-4234-8665-9e04e7f8660c"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("a85c843e-60bd-4bd8-a126-8b853cfcd502"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("ad23527e-00e4-4ab1-8eb2-f1d371009037"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("b46f0991-27e7-4826-99f7-a75941162c03"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("b5ab23dd-4139-4212-bd3e-d9c155924fc0"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("b8b16048-4f4a-43ee-843b-025a4de23f7f"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("b90a3d56-4ca7-41a1-86fb-0dc3f124460c"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("ba043179-7347-4cd0-95bf-57124a484409"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("bc9a9e5e-4c52-4cf9-9e61-0a892dae4b16"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("bd37bc3a-970e-4788-bcdf-957af81e3a46"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("bdbfdb10-0979-402b-a2ac-5f192c73ada2"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("bfa2d8d6-98cf-4154-b82b-4f6828f7a2c9"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("c15afa54-56d0-45cd-8115-32753e5f18e1"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("ca3d2c69-2c73-4cd7-a30a-d8ed2f31e566"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("cbdd691b-df84-4bb7-830f-e379440b8c36"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("e56005b5-51ee-4c55-bd6e-576f1db5f847"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("ed2414f2-f485-4b8c-bda9-3d52f1ad545b"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("f7ab551c-7b22-4ca5-a116-1110c22bcb4f"));

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("1aa747e3-b416-4c8c-a8db-c0f96ccbcd57"));

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("3c358b2b-0b33-4817-a26d-63339c0afce2"));

            migrationBuilder.DeleteData(
                table: "Languages",
                keyColumn: "Id",
                keyValue: new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"));

            migrationBuilder.DeleteData(
                table: "Languages",
                keyColumn: "Id",
                keyValue: new Guid("8a9f010f-457d-4c76-85f3-9ef59df2eb53"));

            migrationBuilder.DeleteData(
                table: "Languages",
                keyColumn: "Id",
                keyValue: new Guid("ae8984c3-d127-47f1-be98-3491486060ac"));

            migrationBuilder.InsertData(
                table: "Authors",
                columns: new[] { "Id", "FullName" },
                values: new object[,]
                {
                    { new Guid("02714691-8d89-4980-b2aa-1b141e80f56a"), "Джордж Гілдер" },
                    { new Guid("0ed3b9f8-bc8e-46f2-89a6-0220cc2894f3"), "Джо Аберкромби" },
                    { new Guid("12e9aa82-0433-401d-9023-a9ca493ea179"), "Ден Дубравін" },
                    { new Guid("1372c186-8bac-4d1d-9ce0-cec3eed85400"), "Пауло Коєльо" },
                    { new Guid("2fad2e23-b525-4332-a934-55dfed8000cd"), "Ден Браун" },
                    { new Guid("30e1db22-c8e8-47bf-8955-e3e00907a26b"), "Гєри Чепмен" },
                    { new Guid("3109af45-ba2b-47a2-9c50-8c007613605f"), "Джен Сінсеро" },
                    { new Guid("3440424a-aa80-44ff-be38-e43d5bc682ca"), "Марк Менсон" },
                    { new Guid("34528118-7296-4c00-ab29-e5d3527e8b77"), "Крістін Мюльке" },
                    { new Guid("35ef4bd1-1f6c-4ee0-b46c-8ff5888ac38f"), "Томас Кеніллі" },
                    { new Guid("379e25f5-1c74-4500-95d2-0470934b5eb2"), "Джордж Орвелл" },
                    { new Guid("41b0dc17-889a-4907-99fc-2f49bf6b8800"), "Джон Фаулз" },
                    { new Guid("50913db2-b104-4d70-a69f-b79d24420415"), "Вальтер Тевіс" },
                    { new Guid("5494f0f2-f436-4484-b0a1-891e157ff38d"), "Рейчел Ліппінкот" },
                    { new Guid("549d5647-68e4-4f89-9b3a-5caf2182b346"), "Анна Мойсеенко" },
                    { new Guid("562253df-de02-4336-8109-b10f01438742"), "Наталя Вотсон" },
                    { new Guid("5e210f19-8a1c-4948-975c-52a5928d5321"), "Стівен Хокінг" },
                    { new Guid("69996b5e-e2ec-422b-bccc-17f964daba5d"), "Кербі Розанес" },
                    { new Guid("69d83a5e-2d96-4f7e-a412-33b31a77f849"), "Ерік Берн" },
                    { new Guid("7404bbbc-bcab-47b9-8d4d-7a856c747d22"), "Адам Кей" },
                    { new Guid("7639ba87-5278-43a1-9b2b-ade053b8f466"), "Джоан Гарріс" },
                    { new Guid("7e9646ae-e171-4d73-8f4b-b75e287c056c"), "Ювал Ной Харарі" },
                    { new Guid("81db6a68-66ef-47b3-926a-07598804df9b"), "Ірвінг Стоун" },
                    { new Guid("8340d844-bedf-4089-81b3-1e8ef7ee2146"), "Джон Грін" },
                    { new Guid("8add3043-422f-4695-9680-8a06f26a19f3"), "Роальд Дал" },
                    { new Guid("942aebca-56b1-4e58-a89c-5724f4b5d94f"), "Джоан Роулінг" },
                    { new Guid("96b4741e-5304-4b68-8aaf-0095795bc0d5"), "Стівен Кінг" },
                    { new Guid("96f11f14-d899-43ec-bb90-43b871b75b68"), "Джон Грей" },
                    { new Guid("987f3d90-0c0b-485e-b175-e25c1e9f6eee"), "Маршалл Розенберг" },
                    { new Guid("aa938510-5d23-4c14-a644-a5e4f28340b5"), "Берджесс Ентоні" },
                    { new Guid("ababf6b8-374d-4dd2-bc1c-28ee5031e463"), "Нассим Таллеб" },
                    { new Guid("b0f57400-9491-4452-b1e2-7f2854eb9155"), "Пол Парсонс" },
                    { new Guid("b2b95678-0797-45ae-8471-dbc476ec45ef"), "Олександр Красовицький" },
                    { new Guid("b5b2fa2f-3022-4c15-88fd-759ac416f881"), "Боріс Джонсон" },
                    { new Guid("bb3b661c-7c4c-46ed-8a1d-7bcd1eeb9fd5"), "Деніел Кіз" },
                    { new Guid("bfbd4d2b-84f6-4ae6-bf10-7e3ccd67c80c"), "Саша Сколиченко" },
                    { new Guid("ca2629c3-4bf9-43f2-b699-0e16ae65d1a6"), "Джейн Остен" },
                    { new Guid("d5404759-d9f0-4dd0-abb4-b8dead014a3a"), "Вахтанг Кіпіані" },
                    { new Guid("d8206aaa-9b66-4141-a990-6719c25dd8e7"), "Річард Докінз" },
                    { new Guid("dd297ede-c0fa-476a-90a1-662e9aa27d7a"), "Ренсом Ріггз" },
                    { new Guid("e6bc8173-af91-4228-a49e-642c9172a5e0"), "Анна Тодд" },
                    { new Guid("ec7d0a17-8fc7-4f15-9410-0b9e187f789c"), "Стефані Шталь" }
                });

            migrationBuilder.InsertData(
                table: "Authors",
                columns: new[] { "Id", "FullName" },
                values: new object[,]
                {
                    { new Guid("f069e340-ea06-4af4-b257-53835f9250d3"), "Жоель Діккер" },
                    { new Guid("f3ec65d8-2f2e-44c2-82f1-292267763bb4"), "Ірина Василенко" }
                });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("86cdea4b-f3df-4b87-b59a-3818c3b8e71a"), "Нон-фікшн література" },
                    { new Guid("e999c968-8637-4152-92e9-60805fc23438"), "Фікшн література" }
                });

            migrationBuilder.InsertData(
                table: "Languages",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("49077420-6517-46c1-b29e-223c1736593c"), "Англійська" },
                    { new Guid("d617c829-4595-45ef-ad64-629a9720607e"), "Українська" },
                    { new Guid("e5515da2-5fdb-4d85-8827-4b23b2b16e3c"), "Російська" }
                });

            migrationBuilder.UpdateData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("438de794-1ede-42fb-8d50-5f0e98939767"),
                columns: new[] { "CustomerId", "OrderDate" },
                values: new object[] { new Guid("f79f3e1f-fca1-4eec-b1d8-c4adcda2a2c8"), new DateTime(2022, 6, 17, 13, 41, 53, 940, DateTimeKind.Local).AddTicks(8412) });

            migrationBuilder.UpdateData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("e1f06da2-5eda-4668-887c-6f87bc92653e"),
                columns: new[] { "CustomerId", "OrderDate" },
                values: new object[] { new Guid("f79f3e1f-fca1-4eec-b1d8-c4adcda2a2c8"), new DateTime(2022, 6, 17, 13, 41, 53, 940, DateTimeKind.Local).AddTicks(8467) });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "City", "Email", "FirstName", "LastName", "Password", "Phone", "PostalCode", "Role", "StoredSalt" },
                values: new object[,]
                {
                    { new Guid("036590f1-41f9-4518-9ce5-5d2c0b6de155"), "Харків", "admin@gmail.com", "Admin", "Also admin", "jovnJbl7fEPIm5e1JHo0gCIJHfDiTWQFT5a/XEf7fIo=", "+38099445165", "61015", "Administrator", new byte[] { 179, 137, 193, 187, 182, 238, 115, 74, 37, 215, 68, 238, 63, 47, 56, 183 } },
                    { new Guid("f79f3e1f-fca1-4eec-b1d8-c4adcda2a2c8"), "Київ", "user@gmail.com", "Іван", "Сірко", "KI1tvZIeiXKBLWJAC7ec4NWjoSkcaTJTdrRDMiwANs8=", "+380671239253", "61600", "User", new byte[] { 67, 92, 15, 46, 125, 178, 137, 130, 148, 214, 71, 138, 132, 73, 134, 237 } }
                });

            migrationBuilder.InsertData(
                table: "Books",
                columns: new[] { "Id", "AuthorId", "Code", "Description", "IsBestseller", "IsNew", "LanguageId", "NumberOfPages", "Price", "Title" },
                values: new object[,]
                {
                    { new Guid("0617034e-cdde-41f1-9c26-f6c1451db5c9"), new Guid("5494f0f2-f436-4484-b0a1-891e157ff38d"), "000006", "П’ять кроків і в жодному разі не ближче. Інакше це може вбити Стеллу. Чому? Рідкісна хвороба, яка з дитинства не давала жити нормальним життям. Не наближайся, не варто. Незабаром на неї чекає операція, і нарешті все буде добре. Та все одно тримай дистанцію. Коли тобі хочеться обійняти її чи торкнутися волосся - не наближайся до неї, Вілле. Ти саме той, кого вона мусить уникати попри все. навіть один твій подих може зруйнувати все її життя. Якщо ти хочеш, щоб вона жила, — тримайся на відстані. Але я знаю, ти не можеш. Чому саме ти закохався в неї? Чому вона покохала саме тебе? Разом - не можна. Окремо - неможливо…", false, true, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 240, 116m, "За п'ять кроків до кохання" },
                    { new Guid("0b0c81e3-b883-475f-99ee-e804694fc441"), new Guid("30e1db22-c8e8-47bf-8955-e3e00907a26b"), "000016", "Ніщо так не сприяє зростанню почуття власної гідності, як уміння кохати та бути коханим. Навіть якщо ви розлучені, або пережили смерть чоловіка, або взагалі ніколи не одружувалися, ваша потреба в коханні нікуди не зникає. З іншого боку, найбільший успіх у житті приходить до тих, хто дарує кохання іншим. Ця книга допоможе вам навчитися приймати та віддавати любов. У перших двох розділах йдеться про те, що ж являють собою безшлюбні люди і чому любов така важлива для побудови успішних взаємин. З 3-го по 7-му розділ ви ознайомитеся з п'ятьма мовами любові. У 8-му розділі ви навчитеся визначати свою основну мову любові та розпізнавати її в інших.", false, false, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 240, 150m, "Пять языков любви" },
                    { new Guid("16c2cf19-16e0-49e0-a954-97bdc3b1f865"), new Guid("8add3043-422f-4695-9680-8a06f26a19f3"), "000004", "Всім шанувальникам творчості Джоан Роулінг, без сумніву, буде цікаво познайомитися з цією книгою, автора якої, Роальда Дала, прийнято вважати «літературним батьком» знаменитої письменниці. Хоча, можливо, цей сюжет вам уже знайомий, адже «Чарлі та шоколадна фабрика» – найпопулярніша книга Дала. Бідолашному хлопчику, який на кожен день народження отримував у подарунок лише маленький шоколадний батончик, чекає дивовижна пригода, адже його доброта є прикладом для інших, а добро завжди отримує свою нагороду.", true, false, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 240, 170m, "Чарлі і шоколадна фабрика" },
                    { new Guid("17d798a7-6595-49a7-a6e1-c3ebde48a673"), new Guid("81db6a68-66ef-47b3-926a-07598804df9b"), "000027", "Жоден художник не був більш безжально керований своїм творчим поривом і не був більш ізольованим ним від звичайних джерел людського щастя, ніж Вінсент Ван Гог. Життя цього геніального постімпрессіоніста було безперервною боротьбою з бідністю, смутком, божевіллям і відчаєм. «Жага до життя» Ірвінга Стоуна вміло передає захоплюючу атмосферу Парижа постімпресіоністів і з глибоким розумінням реконструює розвиток творінь Ван Гога. Художник постає перед читачем не тільки як митець, а й як особистість, і ця розповідь про його насичене, яскраве і стражденне життя не залишить байдужим навіть тих, хто не є шанувальником творчості Ван Гога.", false, false, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 488, 250m, "Ван Гог. Жага до життя" },
                    { new Guid("18e0aa90-7a79-4b14-84f2-bf3fd409d3fe"), new Guid("ec7d0a17-8fc7-4f15-9410-0b9e187f789c"), "000039", "Дитячі травми і образи не проходять безслідно. Вони проявляються в дорослому житті, і часто - самим несподіваним чином: не складаються стосунки, рушиться кар'єра, виникають хвороби... Проблеми ставлять нас у безвихідь, і ми навіть не підозрюємо, що усі відповіді і рішення - в нас самих. Відомий німецький психотерапевт і автор бестселерів Стефани Шталь стверджує: якщо ви подружитеся зі своїм ltamp;quot;внутрішнім ребенкомltamp;quot; подаруєте йому турботу і підтримку, яка так бракувало в дитинстві, на місце болю і бід прийдуть спокій, гармонія і легкість. Ефективні вправи і рекомендації, викладені в книзі, допоможуть вашому ltamp;quot;внутрішньому ребенкуltamp;quot; (тобто і вам!) стати нарешті щасливим.", false, false, new Guid("e5515da2-5fdb-4d85-8827-4b23b2b16e3c"), 304, 320m, "Ребенок в тебе должен обрести дом" },
                    { new Guid("2188047e-7756-44f6-a678-7a7efa9d5e8b"), new Guid("d8206aaa-9b66-4141-a990-6719c25dd8e7"), "000029", "Уже кілька десятиліть Річард Докінз блискуче пояснює наукові явища та дива природи, відстоюючи раціональний підхід та борючись із забобонами та помилками. У цій серії із 42 есе Докінз віддає належне складності світу природи. Він заглиблюється у древні часи, показуючи, наприклад, як історія гігантських та морських черепах може краще пояснити еволюцію. Річард Докінз - еволюційний біолог, відомий популяризатор науки, а також критик креаціонізму та теорії розумного задуму. До 2008 року обіймав посаду професора Оксфордського університету. Член Лондонського королівського товариства, що займається розвитком знань про природу.", false, false, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 384, 210m, "Наука для душі. Нотатки раціоналіста" },
                    { new Guid("350beb5c-aef1-4f31-996d-b3d405b82ed6"), new Guid("41b0dc17-889a-4907-99fc-2f49bf6b8800"), "000011", "Фредерик Клеґґ — нічим не примітний банківський клерк. Єдина його пристрасть — метелики. Впіймати, посадити під скло, зберегти їхню красу, щоб милуватися в будь-який момент... Але якось він знаходить екземпляр, набагато цікавіший за метеликів...", false, true, new Guid("49077420-6517-46c1-b29e-223c1736593c"), 368, 200m, "The Collector" },
                    { new Guid("38f24dde-c993-4d88-9b53-cf598813f815"), new Guid("987f3d90-0c0b-485e-b175-e25c1e9f6eee"), "000013", "Метод ненасильницького спілкування реально покращує життя тисяч людей. Він застосовується і в подружній спальні, і в класній кімнаті, і за круглим столом, і на лінії фронту. Корпорації, організації та уряди, які приймають метод ННО, швидко досягають значного прогресу у вирішенні своїх внутрішніх та зовнішніх проблем. За всієї своєї революційності метод ННО дуже простий, логічний і раціональний. Він ніяк не пов'язаний з релігійно-духовними навчаннями та психологічними школами та доступний абсолютно будь-якій людині.", true, true, new Guid("e5515da2-5fdb-4d85-8827-4b23b2b16e3c"), 288, 168m, "Ненасильственное общение. Язык жизни" },
                    { new Guid("39408675-5c3b-4f94-929e-5f7aa4e92031"), new Guid("96b4741e-5304-4b68-8aaf-0095795bc0d5"), "000036", "22 листопада 1963 року — дата вбивства Джона Кеннеді. Пролунали три постріли — і світ змінився назавжди. Сьогодення. Дізнавшись, що в барі його приятеля розташований портал до 1958 року, звичайний шкільний учитель Джейк Еппінг не може опиратися спокусі почати нове життя у рок-н-рольні часи Елвіса Преслі. Хіба гостю з майбутнього важко познайомитися з відлюдником Лі Гарві Освальдом і завадити йому вбити президента? Та чи варто гратися з минулим? Яким буде світ, де Кеннеді виживе?", false, false, new Guid("e5515da2-5fdb-4d85-8827-4b23b2b16e3c"), 976, 280m, "11/22/63" },
                    { new Guid("4493bc96-6700-40da-b5f0-8f624f385632"), new Guid("942aebca-56b1-4e58-a89c-5724f4b5d94f"), "000047", "Закінчилося дитинство Гаррі, і разом з ним пішла наївність, казковість та уявна безпека з навколишнього світу. Де гра в квідич, перші уроки, мудрі та добрі вчителі? Хто тепер взагалі мудрий, хто добрий, і чи друзі залишаться поруч? Казковий світ перетворився на смерть, що крадеться. Але це самотність, в яку занурюється Гаррі на своєму останньому шляху – це самота більше, ніж смерть. Виявилося, занадто у багатьох є справжнє обличчя, яке до пори невидиме. Яке темне минуле приховував великий маг сучасності Дамблдор? Хто залишиться живим в останньому полюванні на хоркруксів і Волан-де-Морта?", false, false, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 640, 240m, "Гаррі Поттер i Смертельні реліквії" },
                    { new Guid("4a3ab21f-c55f-48ec-85ec-95037732a52b"), new Guid("942aebca-56b1-4e58-a89c-5724f4b5d94f"), "000042", "Друга частина пригод Гаррі Поттера від Джоан Роулінг — книга «Гаррі Поттер і таємна кімната»! На вас чекають нові пригоди у світі чарівників, де старі секрети, літаючі автомобілі, грізна верба, дивний щоденник та небезпечні улюбленці Хегріда оживають на сторінках книги! Юному чарівникові не пощастило з канікулами на Привіт-драйв у сім'ї Дурслів. Він потайки мріє повернутися до чарівного світу, проте від його друзів усе літо не було звістки і здається, що цілий світ проти його повернення... Цілий світ чи маленький ельф на ім'я Доббі, який хоче захистити його від смертельної небезпеки. Та цього хлопця не так просто налякати, особливо коли доводиться обирати між навчанням у чарівній школі та життям з родичами.", true, false, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 352, 210m, "Гаррі Поттер і таємна кімната" },
                    { new Guid("54f8b531-7fb6-41d1-bf1f-f434c7faa905"), new Guid("e6bc8173-af91-4228-a49e-642c9172a5e0"), "000001", "Роман «Опісля» молодої американської письменниці Анни Тодд - це історія кохання Тесс і Гардіна, «хорошої» дівчинки і «поганого» хлопця, кохання пристрасного, відвертого і водночас надзвичайно ніжного й довірливого. Автор майстерно тримає читача у напрузі: чи будуть Тесс і Гардін після всіх перипетій разом, чи не зруйнують вони своє кохання вечірками, алкоголем, сексом та бійками? Чи збережуть тепло ­своїх стосунків? Молоді герої припали до смаку багатьом шанувальникам творчості Анни Тодд, яким подобається читати книжки про диво першої зустрічі, про іскру, що спалахує між чоловіком і жінкою, яка переростає у справжнє кохання.", false, false, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 624, 321m, "ОПісля" },
                    { new Guid("59c556ca-eb2d-4d94-a9e4-7b273fed96bc"), new Guid("02714691-8d89-4980-b2aa-1b141e80f56a"), "000023", "Дивовижна здатність Google «шукати та сортувати» приваблює весь світ своєю пошуковою системою і незліченними іншими бонусами — відео, картами, електронною поштою, календарями... І все, що вона пропонує, є безкоштовним, або принаймні так здається. Замість платити безпосередньо, користувачі долучаються до реклами. Попри те, що розвиток штучного інтелекту викликає марення всемогутності й трансцендентності, Кремнієва долина майже відмовилася від безпеки. Інтернет-брандмауери, які нібито захищають усі ці паролі й особисту інформацію, виявилися безнадійно проникними. У книзі «Життя після Google» Джордж Ґілдер — неперевершений візіонер технологій і культури — пояснює, чому Кремнієва долина страждає на нервовий зрив і на що очікувати, коли настає ера після Google.", false, false, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 320, 320m, "Життя після Google.Занепад великих даних і становлення блокчейн-економіки" },
                    { new Guid("5e720b18-2217-4fc3-9919-80dbebfd5b17"), new Guid("34528118-7296-4c00-ab29-e5d3527e8b77"), "000021", "Замість написати підручник, я зібрав під однією обкладинкою найпростішу та найцікавішу інформацію— і вам варто прочитати цю книжку від початку до кінця! Використайте її, щоб зрозуміти основи, а потім повертайтеся назад і перечитайте вже після того, як склали власну думку, щоб збагнути, які місця в ній можуть бути для вас корисними. Можливо, коли ви з’ясуєте, що любите легкі, ароматні білі вина, а не фруктові та повнотілі, як вважали раніше, вам вдасться відкрити на дегустаціях нові сорти. Пізніше використовуйте книжку як довідник, коли, скажімо, ви готові випити вина на день народження або коли потрапили в Шампань і хочете скуштувати щось, окрім традиційного стилю екстра-брют. Тож нехай ці рядки стануть вашим посібником, з яким ви будете купувати вина, дегустувати їх і впевнено навчатися. На цих сторінках вам трапиться багато технічних термінів, які виз найдете у глосарії.", false, false, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 272, 500m, "Wine Simple. Про вино від сомельє світового класу" },
                    { new Guid("5eee3176-7998-42e8-8459-dd85f9412170"), new Guid("ababf6b8-374d-4dd2-bc1c-28ee5031e463"), "000051", "Цілком очевидно, що життя — це сумарний ефект небагатьох важливих потрясінь. Згадайте своє життя. Перерахуйте важливі події й технологічні винаходи, що припали на ваш вік, і порівняйте з колись напрогнозованим. Скільки їх «приїхало за розкладом»? Подивіться на особисте життя, згадайте, як вибирали професію, зустріли свою половинку, емігрували, стик- нулися зі зрадою, несподівано розбагатіли або впали у злидні. Чи часто ці події відбувалися за планом? Малоймовірну подію, яка справляє колосальний ефект, колишній трейдер Насім Талеб влучно називав Чорним лебедем.", true, false, new Guid("e5515da2-5fdb-4d85-8827-4b23b2b16e3c"), 736, 500m, "Черный лебедь. Под знаком непредсказуемости" },
                    { new Guid("602dc562-2768-49f8-ab93-f17c6689cb0b"), new Guid("2fad2e23-b525-4332-a934-55dfed8000cd"), "000050", "Ця історія про неймовірне розслідування і приголомшливі відкриття Роберта Ленґдона та Софі Неве, без перебільшення, перевернула світ. Ден Браун створив легенду, в яку повірили всі, — попри відчайдушний опір Ватикану. Скандали навкруг роману, здається, не вгамуються ніколи, чому сприяє недавня його екранізація. А секрет цього величезного успіху полягає в тому, що Денові Брауну вдалося, як нікому до нього, наочно довести, що так звана історія та політика — це тільки ширма, за якою приховані величезні таємниці...", true, false, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 480, 240m, "Код Да Вінчі" },
                    { new Guid("61ea5103-1569-468b-92d7-bb934b47d1c3"), new Guid("69d83a5e-2d96-4f7e-a412-33b31a77f849"), "000015", "Доктор Ерік Берн розкриває таємні прийоми, які керують нашим життям. Усі ми, не усвідомлюючи цього, постійно граємо в ігри – подружні та сексуальні, серйозні ігри з керівниками та суперницькі ігри з друзями, в ігри, що визначають статус та сімейні сутички.  Ерік Берн детально описує понад 120 ігор, у які ми граємо самі та у які нас залучають навколишні. А також дає поради для тих, хто хоче навчитись протистояти прийомам ігор, у які вас намагаються втягнути. Він допомагає проаналізувати нюанси поведінки, позбавитись стереотипів у спілкуванні й дає розуміння сутності людських вчинків. Книжка, що вийшла майже 40 років тому, і досі є популярною та актуальною!", false, false, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 256, 220m, "Ігри, у які грають люди" },
                    { new Guid("684a01e0-d62d-4087-ad1b-acaa4626e9ca"), new Guid("7e9646ae-e171-4d73-8f4b-b75e287c056c"), "000022", "Протягом минулого століття людству вдалося зробити неможливе: приборкати голод, епідемії і війни. Здається, що в це важко повірити, проте, як пояснює Харарі в своєму фірмовому стилі - досконалому і захопливому - голод, епідемія і війна трансформувалися з незрозумілих і неконтрольованих сил природи у виклики, якими можна керувати. Вперше в історії більше людей помирає від переїдання, ніж від голодування; більше людей помирає від старості, ніж від інфекційних захворювань; і більше людей скоюють самогубство, ніж вбито солдатами, терористами і злочинцями взятими разом. У середнього американця в тисячу разів більше можливостей померти від харчування у МакДоналдсі, ніж бути вбитим Аль Каїдою.", false, false, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 512, 250m, "Homo Deus. Людина божественна. За лаштунками майбутнього" },
                    { new Guid("688c9373-179a-47ee-bb87-85fda22ae70c"), new Guid("942aebca-56b1-4e58-a89c-5724f4b5d94f"), "000043", "Написана Джоан Роулінг книга «Гаррі Поттер і в'язень Азкабану» - третя книга про пригоди Хлопчика-зі-шрамом, яка підкорила цілий світ! Гаррі Поттер - незвичайний хлопчик. Він дуже не любить літні канікули, адже цей час він проводить в будинку своїх дядька й тітки Дурслі. А вони терпіти не можуть чарівників. Одного разу, втікши від ненависних родичів, Гаррі підбирає автобус «Нічний Лицар», який везе його прямісінько до рятівного магічного світу. Але тепер він не такий безпечний як раніше. Через дванадцять довгих років на свободу вибрався найнебезпечніший злочинець в'язниці Азкабан і спадкоємець самого Волан-де-Морта - Сіріус Блек! Кажуть, ніби це він зрадив Джеймса та Лілі Поттер, а тепер шукає самого Гаррі щоб закінчити почате Темним Лордом. У 3 книзі Гаррі Поттеру і його друзям, Рону і Герміоні, доведеться знову зіткнутися зі злими чарами і розкрити старі секрети.", false, false, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 384, 210m, "Гаррі Поттер і в'язень Азкабану" },
                    { new Guid("7172bd43-970f-4f3e-af58-55eb3815e55f"), new Guid("942aebca-56b1-4e58-a89c-5724f4b5d94f"), "000044", "Четверта книга про пригоди юного чарівника Гаррі Поттера. Нові заклинання, нові зілля, нові вчителі, нові предмети - Гаррі з нетерпінням чекає на початок навчального року. Крім того, у школі магії та чарівництва Хогвартс проходитиме турнір чарівників. У ньому братимуть участь учні 3-х шкіл. Але для когось цей турнір закінчиться трагічно. Волан-де-Морт повернеться знову і цього разу вже в тілі.", true, false, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 670, 210m, "Гаррі Поттер і келих вогню" },
                    { new Guid("74272c7c-afc9-4d86-9ac2-b2af244966f0"), new Guid("549d5647-68e4-4f89-9b3a-5caf2182b346"), "000030", "Відкрийте світ традиційної польської кухні! Капустняк та білий борщ, ароматні супи, відварний яловичий язик та ніжний рулет із грудинки з пікантними травами, качка та курка по-польськи, знаменитий бігос, риба у різноманітних соусах, жарке зі свинини, солодкі пляцки та сирники, фруктові супи з вишні , пишні бабки та хрумке печиво – найкращі рецепти приголомшливих страв польської кухні, класичні частування та їх оригінальні сучасні інтерпретації. Відкрийте для себе неймовірну різноманітність смаку національних кухонь світу! Адже кожна з них – це унікальні та самобутні страви, яскраві та незвичайні гастрономічні відкриття! Сміливо додайте щось нове у свої кулінарні будні з рецептами найпопулярніших національних частування.", false, false, new Guid("e5515da2-5fdb-4d85-8827-4b23b2b16e3c"), 72, 140m, "Польская кухня" },
                    { new Guid("797f7ead-a732-4a67-88e9-c8d19a9b3344"), new Guid("0ed3b9f8-bc8e-46f2-89a6-0220cc2894f3"), "000052", "У світі, де сталь — відповідь на все, а люди моляться Матері Війні, важко знайти своє місце однорукому хлопчині. Ярві, молодший син короля Ґеттландії, мав стати служителем Батька Миру, але доля розпорядилася інакше. Батькова й братова смерть не лишають йому іншого вибору, як зійти на престол, якого він ніколи не прагнув. Однак втримати трон однією рукою нелегко. Ярві зазнáє зради, пройде крізь багато випробувань і знайде справжніх друзів, перш ніж стане тим, ким він є. Та чи не втратить він на своєму шляху до помсти чогось важливого?", true, false, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 296, 290m, "Пів Короля" },
                    { new Guid("86400a7e-ac30-4489-a5b1-ba14f21f13dc"), new Guid("aa938510-5d23-4c14-a644-a5e4f28340b5"), "000037", "Один із найпопулярніших і найулюбленіших серед критиків графічний роман усіх часів, відзначений нагородами шедевр Ніла Ґеймана «СЕНДМЕН» установив нові стандарти для дорослої та ліричної фантастики у галузі коміксів. Проілюстрована популярними художниками жанру, ця серія — насичений коктейль із сучасної та давньої міфології, у який майстерно вплетено сучасну літературу, історичну драму та легенди. Перша з дванадцяти книг, які складають повну серію про «СЕНДМЕНА», відкриває унікальну для графічної літератури сагу і знайомить читачів із чарівним і темним світом снів та жахіть — домом Морфея, Повелителя снів, і його роду, Безмежних.", true, false, new Guid("e5515da2-5fdb-4d85-8827-4b23b2b16e3c"), 256, 67m, "Заводной апельсин" },
                    { new Guid("89f5b15b-3e3e-46ec-b77f-5fbba3a62604"), new Guid("12e9aa82-0433-401d-9023-a9ca493ea179"), "000024", "Управління гнівом – книга, в якій зібрано колекцію технік та підходів, які підвищать контроль над цією вибухонебезпечною емоцією. Вона буде цікава людям, які часто відчувають гнів та роздратування. Ви можете використовувати прочитане в будь-якій сфері, починаючи від спілкування з дітьми, закінчуючи роботою в організаціях.", false, false, new Guid("e5515da2-5fdb-4d85-8827-4b23b2b16e3c"), 208, 295m, "Управление гневом" },
                    { new Guid("92c6f78e-375f-44c3-901f-0cc32fc2e966"), new Guid("f069e340-ea06-4af4-b257-53835f9250d3"), "000003", "Одного грудневого ранку на підлозі в номері 622 розкішного готелю в швейцарських Альпах знаходять труп. Поліції так і не вдається розкрити справу, а з плином часу чимало людей забуло про те, що сталося. Через багато років письменник Жоель Діккер приїжджає в той самий готель, щоб оговтатися від розриву стосунків. Він навіть не уявляє, що закінчить розслідувати давній злочин, та ще й не сам: Скарлетт, прекрасна гостя й письменниця-початківка, що зупинилась у сусідньому номері, буде супроводжувати його в пошуку, а також намагатиметься збагнути, як написати хорошу книжку. Що сталося тієї ночі? Відповідь на це питання знайдемо в цьому диявольському детективі, побудованому з точністю швейцарського годинника.", true, false, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 712, 280m, "Загадка 622" },
                    { new Guid("93ee36c7-ef13-4377-b206-c3458ef318e7"), new Guid("942aebca-56b1-4e58-a89c-5724f4b5d94f"), "000046", "Пропонуємо вашій увазі шосту книгу про Гаррі Поттера - «Гаррі Поттер і Принц-напівкровка»! Після подій у Відділі таємниць у Міністерства магії не залишилося сумнівів - Темний Лорд повернувся. Тепер він діє відкрито, викрадаючи чарівників та вчиняючи терор у всій Британії. У книзі «Гаррі Поттер і напівкровний Принц» Джоан Роулінг занурює читачів у похмуру атмосферу очікування небезпеки. Тепер темна магія — не рідкість на сторінках роману, і героям доведеться протистояти їй. Хто завгодно може виявитися зрадником. Темрява згущується над Гоґвортсом і настає битва. Чи встигнуть наші герої зупинити вторгнення Пожирателів смерті?", false, false, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 576, 240m, "Гаррі Поттер і Напівкровний Принц" },
                    { new Guid("94e02e22-0e4d-4273-bbf8-b4e169208a2a"), new Guid("b5b2fa2f-3022-4c15-88fd-759ac416f881"), "000025", "Книжка Боріса Джонсона — це історія про те, як ексцентричний геній Черчилля, прем’єр-міністра Великої Британії, одного з найвизначніших лідерів ХХ століття, формував світову політику. Аналізуючи досягнення й помилки, а також міфи, які супроводжували цю видатну постать, Джонсон зобразив Черчилля як незрівнянного стратега, людину надзвичайно сміливу, красномовну й глибоко гуманну. Безстрашний на полі бою, один із найкращих ораторів усіх часів, лауреат Нобелівської премії з літератури, Вінстон Черчилль спростував думку, що історія — це місце дії великих знеособлених сил. Життя цього чоловіка є доказом того, що одна людина — відважна, геніальна, визначна — може впливати на світовий лад.", false, false, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 400, 230m, "Фактор Черчилля: Як одна людина змінила історію" },
                    { new Guid("962a5dc2-31df-4a77-9905-be964050a687"), new Guid("379e25f5-1c74-4500-95d2-0470934b5eb2"), "000002", "Незважаючи на художню вигадку, книга настільки реалістична, що перевертає свідомість, причому так, що хочеться перевернути її назад. Сам письменник стверджував, що «найкращі книги говорять тобі те, що ти вже сам знаєш». І роман-утопія «1984» описує речі, про які ми самі здогадувалися, але у існування яких боялися повірити. Основний конфлікт простий: особистість проти системи, тяга до любові й свободи проти поневолення та диктату. Але автор нещадний. Зображену ним систему неможливо перемогти, тому що не можна перемогти те, у чого немає ні серця, ні мозку, ні сенсу, ні цілей існування. Як такої немає й ідеології, де можна було б шукати вади: усе знищено укупі зі здоровим ґлуздом. Є лише влада заради влади. А людство надіслано на бійню, де гинуть не стільки тіла, скільки душі, оскільки розум, пам’ять, гуманізм та любов оголошені поза законом.", true, false, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 312, 150m, "1984" },
                    { new Guid("a583a9ea-035a-4838-a0d7-e8374c0b205b"), new Guid("3109af45-ba2b-47a2-9c50-8c007613605f"), "000031", "Ця книга – ваш особистий тренер. Щодня вона підніматиме бойовий дух і заряджатиме на успіх. Її автор, знаменита Джен Сінсеро, закликає не зменшувати обертів на шляху до успіху та щодня накачувати 'язи крутості' у Духовному тренажерному залі. Успіх - це спосіб існування, постійної адаптації та зростання.Чим більше ви робите для свого успіху, тим легше стає шлях', - лунає бойовий клич Сінсеро. Не тупи - це книга-тренінг із вправами та техніками для щоденної роботи над собою. З її допомогою ви підвищите рівень енергії і збережете усвідомленість і мотивацію навіть у найважчі часи.", false, false, new Guid("e5515da2-5fdb-4d85-8827-4b23b2b16e3c"), 208, 75m, "НЕ ТУПИ. Только тот, кто ежедневно работает над собой, живет жизнью мечты" },
                    { new Guid("aba8e784-2865-4962-8a8a-231234435067"), new Guid("bb3b661c-7c4c-46ed-8a1d-7bcd1eeb9fd5"), "000041", "Чарлі Гордон — розумово відсталий, але у нього є друзі, робота, захоплення і непереборне бажання вчитися. Він погоджується взяти участь у небезпечному науковому експерименті, який може зробити його найрозумнішим... Яку ціну маємо ми платити за свої бажання? Чи варті вони того? Пронизлива історія, яка нікого не залишить байдужим! Мабуть, один з найлюдяніших творів у світовій літературі.", true, false, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 304, 190m, "Квіти для Елджернона" },
                    { new Guid("ac0e8f04-e390-4049-8685-15c7b74a44e8"), new Guid("ca2629c3-4bf9-43f2-b699-0e16ae65d1a6"), "000048", "Джейн Остен (1775—1817) і досі по праву вважається «першою леді» англійської літератури. Її романи сповнені тонкого психологізму. «Гордість і упередженість» — це історія про кохання  заможного красеня-аристократа та скромної, але гордої доньки власника невеликого маєтку в провінції.", false, false, new Guid("49077420-6517-46c1-b29e-223c1736593c"), 287, 55m, "Pride and Prejudice" },
                    { new Guid("ac67e8c4-b93c-4ce0-8d57-2ac6c173446c"), new Guid("1372c186-8bac-4d1d-9ce0-cec3eed85400"), "000035", "Найкращий твір уславленого автора! Цей роман став культовим! Він захоплює з першої сторінки! В юні роки ми не боїмося мріяти, все здається можливим. Але з часами невідома сила починає переконувати нас, що мрії незбутні, а прагнення нездійсненні. Ця книжка навчить жити, не втрачаючи віру в кохання і добро, навчить цінувати кожний день, кожну годину, кожну мить, радіти, сумувати,  але ніколи не опускати руки.", true, false, new Guid("e5515da2-5fdb-4d85-8827-4b23b2b16e3c"), 120, 155m, "Алхимик" },
                    { new Guid("afa9b1ab-a039-49ec-9492-e523c9c9fcba"), new Guid("d5404759-d9f0-4dd0-abb4-b8dead014a3a"), "000026", "У цій книжці зібрано документи з шеститомної кримінальної справи, яка зберігається на полицях колишнього архіву КДБ УРСР у Києві (нині — Галузевий державний архів СБУ). Як і за що було заарештовано поета? Хто під час процесу намагався врятувати Василя Стуса, а хто зробив усе, щоб він назавжди зник за ґратами політичної зони на Уралі? У книжці вміщено архівні документи зі справи Василя Стуса (протоколи обшуків, допитів, листи, статті і т. д.), фотографії, статті Вахтанга Кіпіані, а також відомих дисидентів — Василя Овсієнка, Леоніда Бородіна. Додано останні прижиттєві нотатки Стуса — «З таборового зошита», таємно передані з табору на волю.", false, false, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 688, 260m, "Справа Василя Стуса. Збірка документів з архіву колишнього КДБ УРСР" },
                    { new Guid("b13a18d3-673c-4851-b792-e6080a3b3ac1"), new Guid("dd297ede-c0fa-476a-90a1-662e9aa27d7a"), "000009", "Руїни великого міста... Невідома небезпека, що таїться серед зруйнованих будинків... Тут виявилися Джейкоб та його друзі — мешканці будинку дивних дітей. Кожен із них наділений дивовижними здібностями, і всі вони хочуть лише одного — зняти страшне закляття з наставниці міс Сапсан. Вороги, жорстокі монстри з порожніми очима, невідступно переслідують героїв. Джейкоб навіть уявити було, які пастки їм приготували. Він готовий будь-що врятувати міс Сапсан і свою кохану Емму. Чому ж Емма сама просить його піти? Як може Джейкоб чути наближення Порожніх? Звідки в нього дар розуміти монстрів і наказувати ними?", true, true, new Guid("e5515da2-5fdb-4d85-8827-4b23b2b16e3c"), 464, 240m, "Город Пустых. Побег из дома странных детей" },
                    { new Guid("b40c9898-55fb-4979-bce7-19e8c1a5f285"), new Guid("3440424a-aa80-44ff-be38-e43d5bc682ca"), "000040", "Сучасне суспільство пропагує культ успіху: будь розумнішим, багатшим, продуктивнішим - будь кращим за всіх. Соцмережі рясніють історіями на тему, як якийсь хлопець придумав додаток і заробив купу грошей, статтями на кшталт 'Тисяча і один спосіб бути щасливим', а фото у френдленті створюють враження, що оточуючі живуть краще та цікавіше, ніж ми. Проте наша зацикленість на позитиві та успіху лише нагадує про те, чого ми не досягли, про мрії, які не справдилися. Як стати по-справжньому щасливим?", true, false, new Guid("e5515da2-5fdb-4d85-8827-4b23b2b16e3c"), 192, 230m, "Тонкое искусство пофигизма" },
                    { new Guid("bc6eed6b-697a-4bb1-ad90-4ee13102b826"), new Guid("8340d844-bedf-4089-81b3-1e8ef7ee2146"), "000010", "У кожного в житті є люди, які мають для нас величезне значення. Це можуть бути батьки, друзі, кохані або навіть сусіди. Якщо однієї ночі така людина залізе до вас у вікно та попросить про допомогу, навряд ви їй відмовите. От і Квентин не зміг. І поїхав разом зі своєю сусідкою Марго, в яку закоханий із самого дитинства, щоб помститися її кривдникам. Як заїдеш у паперові міста, то вже не повернешся.Паперові міста Джона Гріна - це не просто оповідка з життя американських підлітків. Це книга про пошуки себе та свого місця в світі. Про те, чи треба миритися з ситуацією, яка тобі не подобається, чи спробувати її змінити. Про те, чи справді ми закохані в людину, чи створюємо собі її образ? І що взагалі реально? Минуле? Майбутнє? Картини, які малює твоя уява? Чи момент 'тут і тепер' в якому ти існуєш?", false, true, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 320, 220m, "Паперові міста" },
                    { new Guid("bf199974-924e-4c05-a028-6bb93b5da8bf"), new Guid("942aebca-56b1-4e58-a89c-5724f4b5d94f"), "000045", "Пропонуємо вашій увазі п'яту книгу про Гаррі Поттера! Джоан Роулінг писала книгу «Гаррі Поттер і Орден Фенікса» протягом трьох років. Вона вийшла найдовшою у циклі і розкриває несподівані таємниці життя улюблених героїв. Гаррі щороку з нетерпінням чекає кінця літніх канікул і повернення до магічного світу. Але в цьому році магічний світ порушив спокій спекотного вечора Тисовий вулиці - на Дадлі та Гаррі нападають дементори, змусивши використати патронус. Тепер юного чарівника судитимуть в Міністерстві магії і, можливо, його чекає відрахування з Ґогвортсу. Ця частина про Хлопчика-який-вижив стала похмурішою і серйознішою. Головний герой зростає разом з читачами — тепер йому доводиться приймати дорослі рішення і миритися з важкими втратами.", false, false, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 814, 240m, "Гаррі Поттер i Орден Фенікса" },
                    { new Guid("bfc8b452-c8fb-4fc8-9707-f5389d94d6fb"), new Guid("96f11f14-d899-43ec-bb90-43b871b75b68"), "000018", "Єдина і неповторна книга про відносини чоловіка і жінки. Час не владний над нею, вона незмінно залишається актуальною та надзвичайно затребуваною. Секрет її феноменального успіху в тому, що вона дійсно допомагає чоловікам і жінкам зрозуміти та прийняти, наскільки по-різному вони мислять, відчувають та поводяться. Завдяки цьому мільйони читачів змогли не лише покращити, а й навіть урятувати свої стосунки.", true, false, new Guid("e5515da2-5fdb-4d85-8827-4b23b2b16e3c"), 352, 105m, "Мужчины с Марса, женщины с Венеры" },
                    { new Guid("bfcb70d3-8e4e-47da-b245-11554df6101c"), new Guid("f3ec65d8-2f2e-44c2-82f1-292267763bb4"), "000019", "Бути веганом чи вегетаріанцем не означає їсти самі паростки бобів і насіння чіа. Але це обов’язково означає цінувати кожне життя. Зокрема своє. Якщо ви збилися з ніг, шукаючи швидкі, апетитні та стовідсотково здорові рецепти, то це ваша рятівна зупинка: як щодо рослинного молока, мікрозелені й суперфудів не за всі гроші світу? Під час створення цієї книжки жодна жива істота не постраждала. Тож можете насолоджуватися трапезою, любі вегани, лакто-, ово-, оволактовегетаріанці! Тут є все: від овочевих чипсів і горіхової пасти до фаршированих печериць і бананових мафінів — буде чим поласувати, переглядаючи фільм із вегетаріанцями в головних ролях. Готуйте, смакуйте, харчуйтеся корисно, розмаїто та з піклуванням про навколишнє середовище. Go vegan, do what you can — і хтозна, може, на Вегетаріанській Алеї Слави займеться нова зірка кіно, літератури, науки чи спорту!", false, false, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 256, 250m, "Вегетаріанська кухня. Смачно, етично, естетично" },
                    { new Guid("c092bd1d-5424-42cf-b518-a7e107440fe2"), new Guid("dd297ede-c0fa-476a-90a1-662e9aa27d7a"), "000008", "Джейкоб Портман - підліток зі страшними снами - знаходить в архіві справи та фотографії дітей і вирішує з'ясувати, скільки правди в тих історіях, які він чув у дитинстві про монстрів та дітей з надздібностями. Відмінно побудована розповідь змушує читача підозрювати Джейкоба у шизофренії через візити до психотерапевта, який прагне пояснити те, що відбувається з раціонального погляду. Неординарний сюжет, детально прописані характери затягують читача в особливу атмосферу життя крипто сапієнсів — дітей, які вирізняються незвичайними, дивними можливостями. Реальність тут переплетена з фантазією, і ми вже не впевнені, чи був тунгуський метеорит, чи це витівки дивних дітей?", true, true, new Guid("e5515da2-5fdb-4d85-8827-4b23b2b16e3c"), 432, 240m, "Дом странных детей" },
                    { new Guid("cab8aa18-7480-4ed7-806f-0d80dc8beb21"), new Guid("942aebca-56b1-4e58-a89c-5724f4b5d94f"), "000053", "Перша частина пригод Гаррі Поттера — книга №1 для маленьких читачів. Саме вона сколихнула хвилю любові до читання, яка захопила цілий світ! Такого успіху не мала жодне художне ходожнє видання, доки 1997 року на полицях англійських магазинів не з'явилася перша книга Джоан Роулінг «Гаррі Поттер і філософський камінь». Сирота на ім'я Гаррі живе під сходами у будинку дядька ті тітоньски Дурслі. Він поки що не знає, як потрапити на платформу дев'ять і три чверті, користуватися чарівною паличкою, та літати на мітлі. Але одного разу до нього прилетить сова із загаднковим листом, який запрошує його до навчання у Школі Чарів та Чаклунства, і з того дня його життя докорінно зміниться...", true, false, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 320, 210m, "Гаррі Поттер і філософський камінь" },
                    { new Guid("cadad1f0-b1af-47c7-9cc1-a9e295a15381"), new Guid("bfbd4d2b-84f6-4ae6-bf10-7e3ccd67c80c"), "000033", "Депресія — це не вигадка, не відмовка й не вияв лінощів. Це справжня серйозна проблема, яку варто й потрібно розв’язувати. То що ж робити, коли світ навколо стає темносірим і звичні приємності вже давно не радують? Чи можна якось цьому зарадити? Шукайте відповіді й натхнення в чудовій арт-книжці Саші Скочиленко!", false, false, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 48, 190m, "Книжка про депресію" }
                });

            migrationBuilder.InsertData(
                table: "Books",
                columns: new[] { "Id", "AuthorId", "Code", "Description", "IsBestseller", "IsNew", "LanguageId", "NumberOfPages", "Price", "Title" },
                values: new object[,]
                {
                    { new Guid("d573fd17-06c1-46a9-b570-3a57a781198e"), new Guid("35ef4bd1-1f6c-4ee0-b46c-8ff5888ac38f"), "000005", "Той, хто врятував єдине життя, врятував увесь світ» – ці слова з Талмуда написали ув'язнені на кільці, яке подарували своєму рятівнику – Оскару Шиндлеру. Дія роману ґрунтується на справжніх подіях, що відбувалися в окупованій Польщі під час Другої світової війни. Німецький промисловець, начальник концентраційного табору Оскар Шиндлер поодинці врятував від смерті в газових камерах більше людей, ніж будь-хто за всю історію війни.", false, false, new Guid("e5515da2-5fdb-4d85-8827-4b23b2b16e3c"), 480, 190m, "Список Шиндлера" },
                    { new Guid("de78c549-04d7-411b-a7e3-688da4beb1a3"), new Guid("5e210f19-8a1c-4948-975c-52a5928d5321"), "000038", "Коротка історія часу Стівена Хокінга - один із стовпів сучасної науково-популярної літератури. У цій книзі автор намагається відповісти на найскладніші та інтригуючі питання про світобудову: коли і чому почався Всесвіт? чи безмежний Всесвіт чи має межу? Чи завжди час проходив уздовж звичного для нас вектора? чи можливі подорожі у часі? що очікує космос у далекому майбутньому? Стівен Хокінг не робить однозначних висновків, оскільки на такі не наважується поки що ніхто на передньому краї науки. Однак він перекладає живою та образною мовою найскладніші положення теоретичної фізики та переносить читача на дивовижні простори антиматерії, суперструн, кварків, чорних дірок та гравітаційних хвиль — туди, де Бог здається ще більш незбагненним та всемогутнім. Вдаючись до несподіваних образів, а не формул - виняток становить лише ейнштейнівська Е = mc2, - Стівен Хокінг наближає нас до розгадки таємниці, що лежить у самому серці Всесвіту.", true, false, new Guid("e5515da2-5fdb-4d85-8827-4b23b2b16e3c"), 272, 180m, "Краткая история времени" },
                    { new Guid("df3c53cd-4bf5-4990-a4b4-877fd55a2df5"), new Guid("562253df-de02-4336-8109-b10f01438742"), "000020", "Ця інтерактивна книжка занурить вас у мандри, які розпочнуться у Великій Британії у 1600-х димним брунатним пивом та завершаться сучасними пивними інноваціями, проведе крізь час та простір і познайомить з історією найвідоміших стилів сучасності — з німецькими лагерами, стаутами, портерами, пільзнерами, ІРА, кислим пивом і не лише. Кожний розділ зосереджує увагу на одному з основних складників пива — солоді, воді, хмелі чи дріжджах — і розповідає, як вони змінювалися з часом, як виникали нові смаки, аромати й стилі. До кожного прикладу такої зміни Наталя Вотсон пропонує сучасний зразок пива, який допоможе сповна відчути смак історії та простежити еволюцію пива. «Пиво: еволюція смаку в 50 стилях» містить п’ять сторіч пивних знань, історії та цікаві факти, які дають можливість по-новому поглянути на пізнання одного з найстаріших напоїв світу, який і досі з нами.", false, true, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 216, 300m, "Пиво. Еволюція смаку в 50 стилях" },
                    { new Guid("e08dcf45-ea18-41d9-ae77-94bfc8558183"), new Guid("69996b5e-e2ec-422b-bccc-17f964daba5d"), "000034", "Перед вами не звичайна розмальовка, не розмальовка-антистрес, не розмальовка для певного віку. Перед вами — екстремальна розмальовка Кербі Розанеса «Аніморфія»! Автор пропонує поринути у надзвичайний світ чудернацьких образів, фантазій, закликає дати волю своїй уяві та бажанням і за допомогою кольорових олівців вивільнити свою енергію на папері. Розмальовка зацікавить людину будь-якого віку. Адже головне, щоб ваш характер, ваш настрій співпадали з сюрреалістичними малюнками Кербі Розанеса.", false, false, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 96, 185m, "Аніморфія" },
                    { new Guid("ed1d9793-5927-4ad9-9aee-3e5056d666c1"), new Guid("7e9646ae-e171-4d73-8f4b-b75e287c056c"), "000014", "Що робить нас блискучими? Що робить нас смертельними? Що робить нас розумними? Юваль Ной Харарі кидає виклик всьому, що ми знаємо про людину. Вік Землі 4,5 мільярда років. Лише за частку цього часу один вид серед незліченної кількості інших підкорив його: ми. У цій сміливій та провокаційній книзі Юваль Ноа Харарі досліджує, хто ми, як ми сюди потрапили і куди йдемо. Ідеальний подарунок для допитливих читачів цього Різдва.", true, false, new Guid("49077420-6517-46c1-b29e-223c1736593c"), 512, 544m, "Sapiens. A Brief History of Humankind" },
                    { new Guid("ef310549-80f0-4d46-aecf-c344b171b303"), new Guid("7639ba87-5278-43a1-9b2b-ade053b8f466"), "000049", "Найголовніше сховано всередині. Фрамбуаза отримала дивний спадок від матері. Її брату дісталася ферма, старшій сестрі — винний погріб, сповнений бурштиново-сонячних пляшок, а Фрамбуазі — альбом з кулінарними рецептами. Не багацько… Але на берегах зошита впереміш зі старовинними рецептами різноманітних смаколиків вона знаходить загадкові нотатки. Секрети та зізнання матері. Життя, яке вона так ретельно приховувала від дітей, у часи війни й окупації. Маленькі радощі й прикрощі, потаємні думки, дитячі образи та щира сповідь жінки, що боялася любити. Минуло стільки років. Лишилося стільки запитань. Відповіді на них — у цьому старому щоденнику. Настав час розкрити моторошні таємниці минулого.", false, false, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 352, 131m, "П'ять четвертинок апельсина" },
                    { new Guid("f2a09a1a-0faf-4c77-a7a6-c4b5e6c39672"), new Guid("b0f57400-9491-4452-b1e2-7f2854eb9155"), "000028", "У книзі наведено огляд і ґенезу основних питань щодо походження і структури Всесвіту та законів і фізичних явищ, які підтримують його існування; щодо будови Землі й походження людини, особливості її психофізіології й уміння оперувати інтелектом. У книзі наведено термінологічно-понятійний апарат відповідних напрямків науки, а також віхи життя і діяльності найвизначніших діячів, які долучилися до становлення й розвитку цих наук. Для широкого загалу й усіх, хто цікавиться науковою думку і прагне розширити й поглибити свій кругозір.", false, false, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 160, 250m, "Теорії за 30 секунд" },
                    { new Guid("f5e80a9a-42b2-4195-8f63-32601920c755"), new Guid("7404bbbc-bcab-47b9-8d4d-7a856c747d22"), "000012", "Робота лікарів схожа на їхній химерний почерк — звичайний пацієнт нічого в цьому не втямить. Без допомоги спеціаліста, звісно. Такого, як Адам Кей — лікар-ординатор, який ділиться реальними історіями зі своєї багаторічної практики. Він знає все про будні звичайного медпрацівника, про те, що відбувається в кабінетах і палатах: daily routine британських лікарів та їхніх пацієнтів, складні діагнози і фатальні помилки, кумедні випадки та дивовижні зцілення, моральні дилеми і професійні хитрощі, понаднормові години праці та майже цілковиту відсутність особистого життя. Будні супергероїв у білих халатах, приправлені тонким англійським гумором та щирим співпереживанням. Ви будете здивовані тим, як багато спільного мають британські doctors та українські лікарі. Тож заходьте, вдягайте халат і приготуйтеся до того, що буде трішки боляче, смішно, сумно та загалом більш ніж просто захопливо.", false, true, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 256, 120m, "Буде боляче" },
                    { new Guid("f6fe6aae-f959-4cc1-99fc-27abb433b525"), new Guid("50913db2-b104-4d70-a69f-b79d24420415"), "000007", "1950-ті роки. Бет Гармон — звичайна сирота із незвичайним хистом до шахів. Гра — її єдина пристрасть. Доки Бет у грі, доти контролює себе та... згубну жагу до транквілізаторів і алкоголю. Дівчина відчайдушно бажає стати феноменальною шахісткою, але водночас усе дужче хоче втекти подалі від цієї реальності. Наближається вирішальний шаховий двобій із чемпіоном світу, російським гросмейстером Василієм Борґовим. Одна хибна комбінація може зіпсувати всю гру, а неправильний хід — зламати майбутнє. Бет укотре втрачає контроль над собою. Але цього разу заради перемоги вона не шкодуватиме ні шахових фігур, ні живих людей.", false, true, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 352, 265m, "Хід Королеви" },
                    { new Guid("ffc437e5-dc6d-44a1-94f2-ff590c4b3cd2"), new Guid("b2b95678-0797-45ae-8471-dbc476ec45ef"), "000032", "24 лютого 2022 року, коли українські міста прокинулись від вибухів, ворог планував «блискавично» підкорити країну, вбиваючи мирних мешканців і знищуючи міста, але українці героїчно чинили опір. Кремль не зміг реалізувати план швидкого захоплення України та зміни влади в країні. Головною перепоною для окупантів стали єдність українців та стійкість ЗСУ разом з іншими силами оборони України, а ціною опору — тисячі жертв серед цивільного населення, зруйновані долі мільйонів українців через втрату близьких, дому, колишнього життя. Документальна хроніка повномасштабного вторгнення Росії в Україну нагадує читачу про трагічні і героїчні події першого місяця війни.  Ця книга — нагадування світовому суспільству про те, що російсько-українська війна триває, а українці зі зброєю в руках захищають благополуччя Європи.", false, true, new Guid("d617c829-4595-45ef-ad64-629a9720607e"), 383, 190m, "Місяць війни. Хроніка подій. Промови та звернення Президента України Володимира Зеленського" }
                });

            migrationBuilder.InsertData(
                table: "Genres",
                columns: new[] { "Id", "CategoryId", "Name" },
                values: new object[,]
                {
                    { new Guid("148dddbc-6a18-4fa4-a46d-6d4efd13d810"), new Guid("86cdea4b-f3df-4b87-b59a-3818c3b8e71a"), "Науково-популярна література" },
                    { new Guid("442d1f92-912c-4d45-a073-d21d7a8bd392"), new Guid("86cdea4b-f3df-4b87-b59a-3818c3b8e71a"), "Філософія" },
                    { new Guid("6e685927-2741-436e-b364-118ba8033218"), new Guid("86cdea4b-f3df-4b87-b59a-3818c3b8e71a"), "Псилохолія" },
                    { new Guid("766717b2-ec30-491e-980f-35b2e8f21a36"), new Guid("86cdea4b-f3df-4b87-b59a-3818c3b8e71a"), "Саморозвиток" },
                    { new Guid("7a298ea0-49e8-4551-b112-fb7711d6e32a"), new Guid("86cdea4b-f3df-4b87-b59a-3818c3b8e71a"), "Документальна література" },
                    { new Guid("8bd0506a-2d5c-462f-aef9-2667f3b3ecfe"), new Guid("e999c968-8637-4152-92e9-60805fc23438"), "Пригоди" },
                    { new Guid("9bba5121-2e70-4e40-b48e-9ac455eb015c"), new Guid("86cdea4b-f3df-4b87-b59a-3818c3b8e71a"), "Кулінарія" },
                    { new Guid("adeac2fd-98b8-46a1-b2db-6ae19cee44fa"), new Guid("86cdea4b-f3df-4b87-b59a-3818c3b8e71a"), "Політика" },
                    { new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc"), new Guid("e999c968-8637-4152-92e9-60805fc23438"), "Роман" },
                    { new Guid("ddddf1e2-dab0-4fff-b288-3d740a634826"), new Guid("e999c968-8637-4152-92e9-60805fc23438"), "Драма" },
                    { new Guid("e97e3fcf-e33f-4cb5-a5f9-55fc49c4a1a0"), new Guid("e999c968-8637-4152-92e9-60805fc23438"), "Детектив" },
                    { new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd"), new Guid("e999c968-8637-4152-92e9-60805fc23438"), "Фантастика" }
                });

            migrationBuilder.InsertData(
                table: "BookGenres",
                columns: new[] { "BookId", "GenreId" },
                values: new object[,]
                {
                    { new Guid("0617034e-cdde-41f1-9c26-f6c1451db5c9"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") },
                    { new Guid("0617034e-cdde-41f1-9c26-f6c1451db5c9"), new Guid("ddddf1e2-dab0-4fff-b288-3d740a634826") },
                    { new Guid("0b0c81e3-b883-475f-99ee-e804694fc441"), new Guid("6e685927-2741-436e-b364-118ba8033218") },
                    { new Guid("0b0c81e3-b883-475f-99ee-e804694fc441"), new Guid("766717b2-ec30-491e-980f-35b2e8f21a36") },
                    { new Guid("16c2cf19-16e0-49e0-a954-97bdc3b1f865"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") },
                    { new Guid("16c2cf19-16e0-49e0-a954-97bdc3b1f865"), new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd") },
                    { new Guid("17d798a7-6595-49a7-a6e1-c3ebde48a673"), new Guid("7a298ea0-49e8-4551-b112-fb7711d6e32a") },
                    { new Guid("18e0aa90-7a79-4b14-84f2-bf3fd409d3fe"), new Guid("766717b2-ec30-491e-980f-35b2e8f21a36") },
                    { new Guid("2188047e-7756-44f6-a678-7a7efa9d5e8b"), new Guid("148dddbc-6a18-4fa4-a46d-6d4efd13d810") },
                    { new Guid("350beb5c-aef1-4f31-996d-b3d405b82ed6"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") },
                    { new Guid("38f24dde-c993-4d88-9b53-cf598813f815"), new Guid("6e685927-2741-436e-b364-118ba8033218") },
                    { new Guid("39408675-5c3b-4f94-929e-5f7aa4e92031"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") },
                    { new Guid("39408675-5c3b-4f94-929e-5f7aa4e92031"), new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd") },
                    { new Guid("4493bc96-6700-40da-b5f0-8f624f385632"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") },
                    { new Guid("4493bc96-6700-40da-b5f0-8f624f385632"), new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd") },
                    { new Guid("4a3ab21f-c55f-48ec-85ec-95037732a52b"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") },
                    { new Guid("4a3ab21f-c55f-48ec-85ec-95037732a52b"), new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd") },
                    { new Guid("54f8b531-7fb6-41d1-bf1f-f434c7faa905"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") },
                    { new Guid("54f8b531-7fb6-41d1-bf1f-f434c7faa905"), new Guid("ddddf1e2-dab0-4fff-b288-3d740a634826") },
                    { new Guid("59c556ca-eb2d-4d94-a9e4-7b273fed96bc"), new Guid("148dddbc-6a18-4fa4-a46d-6d4efd13d810") },
                    { new Guid("5e720b18-2217-4fc3-9919-80dbebfd5b17"), new Guid("9bba5121-2e70-4e40-b48e-9ac455eb015c") },
                    { new Guid("5eee3176-7998-42e8-8459-dd85f9412170"), new Guid("442d1f92-912c-4d45-a073-d21d7a8bd392") },
                    { new Guid("602dc562-2768-49f8-ab93-f17c6689cb0b"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") },
                    { new Guid("602dc562-2768-49f8-ab93-f17c6689cb0b"), new Guid("e97e3fcf-e33f-4cb5-a5f9-55fc49c4a1a0") },
                    { new Guid("61ea5103-1569-468b-92d7-bb934b47d1c3"), new Guid("766717b2-ec30-491e-980f-35b2e8f21a36") },
                    { new Guid("684a01e0-d62d-4087-ad1b-acaa4626e9ca"), new Guid("148dddbc-6a18-4fa4-a46d-6d4efd13d810") },
                    { new Guid("688c9373-179a-47ee-bb87-85fda22ae70c"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") },
                    { new Guid("688c9373-179a-47ee-bb87-85fda22ae70c"), new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd") },
                    { new Guid("7172bd43-970f-4f3e-af58-55eb3815e55f"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") },
                    { new Guid("7172bd43-970f-4f3e-af58-55eb3815e55f"), new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd") },
                    { new Guid("74272c7c-afc9-4d86-9ac2-b2af244966f0"), new Guid("9bba5121-2e70-4e40-b48e-9ac455eb015c") },
                    { new Guid("797f7ead-a732-4a67-88e9-c8d19a9b3344"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") },
                    { new Guid("797f7ead-a732-4a67-88e9-c8d19a9b3344"), new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd") },
                    { new Guid("86400a7e-ac30-4489-a5b1-ba14f21f13dc"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") },
                    { new Guid("86400a7e-ac30-4489-a5b1-ba14f21f13dc"), new Guid("ddddf1e2-dab0-4fff-b288-3d740a634826") },
                    { new Guid("86400a7e-ac30-4489-a5b1-ba14f21f13dc"), new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd") },
                    { new Guid("89f5b15b-3e3e-46ec-b77f-5fbba3a62604"), new Guid("6e685927-2741-436e-b364-118ba8033218") },
                    { new Guid("92c6f78e-375f-44c3-901f-0cc32fc2e966"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") },
                    { new Guid("92c6f78e-375f-44c3-901f-0cc32fc2e966"), new Guid("e97e3fcf-e33f-4cb5-a5f9-55fc49c4a1a0") },
                    { new Guid("93ee36c7-ef13-4377-b206-c3458ef318e7"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") },
                    { new Guid("93ee36c7-ef13-4377-b206-c3458ef318e7"), new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd") },
                    { new Guid("94e02e22-0e4d-4273-bbf8-b4e169208a2a"), new Guid("7a298ea0-49e8-4551-b112-fb7711d6e32a") }
                });

            migrationBuilder.InsertData(
                table: "BookGenres",
                columns: new[] { "BookId", "GenreId" },
                values: new object[,]
                {
                    { new Guid("962a5dc2-31df-4a77-9905-be964050a687"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") },
                    { new Guid("962a5dc2-31df-4a77-9905-be964050a687"), new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd") },
                    { new Guid("a583a9ea-035a-4838-a0d7-e8374c0b205b"), new Guid("766717b2-ec30-491e-980f-35b2e8f21a36") },
                    { new Guid("aba8e784-2865-4962-8a8a-231234435067"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") },
                    { new Guid("aba8e784-2865-4962-8a8a-231234435067"), new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd") },
                    { new Guid("ac0e8f04-e390-4049-8685-15c7b74a44e8"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") },
                    { new Guid("ac0e8f04-e390-4049-8685-15c7b74a44e8"), new Guid("ddddf1e2-dab0-4fff-b288-3d740a634826") },
                    { new Guid("ac67e8c4-b93c-4ce0-8d57-2ac6c173446c"), new Guid("8bd0506a-2d5c-462f-aef9-2667f3b3ecfe") },
                    { new Guid("ac67e8c4-b93c-4ce0-8d57-2ac6c173446c"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") },
                    { new Guid("ac67e8c4-b93c-4ce0-8d57-2ac6c173446c"), new Guid("ddddf1e2-dab0-4fff-b288-3d740a634826") },
                    { new Guid("afa9b1ab-a039-49ec-9492-e523c9c9fcba"), new Guid("7a298ea0-49e8-4551-b112-fb7711d6e32a") },
                    { new Guid("b13a18d3-673c-4851-b792-e6080a3b3ac1"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") },
                    { new Guid("b13a18d3-673c-4851-b792-e6080a3b3ac1"), new Guid("e97e3fcf-e33f-4cb5-a5f9-55fc49c4a1a0") },
                    { new Guid("b13a18d3-673c-4851-b792-e6080a3b3ac1"), new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd") },
                    { new Guid("b40c9898-55fb-4979-bce7-19e8c1a5f285"), new Guid("766717b2-ec30-491e-980f-35b2e8f21a36") },
                    { new Guid("bc6eed6b-697a-4bb1-ad90-4ee13102b826"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") },
                    { new Guid("bc6eed6b-697a-4bb1-ad90-4ee13102b826"), new Guid("ddddf1e2-dab0-4fff-b288-3d740a634826") },
                    { new Guid("bc6eed6b-697a-4bb1-ad90-4ee13102b826"), new Guid("e97e3fcf-e33f-4cb5-a5f9-55fc49c4a1a0") },
                    { new Guid("bf199974-924e-4c05-a028-6bb93b5da8bf"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") },
                    { new Guid("bf199974-924e-4c05-a028-6bb93b5da8bf"), new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd") },
                    { new Guid("bfc8b452-c8fb-4fc8-9707-f5389d94d6fb"), new Guid("6e685927-2741-436e-b364-118ba8033218") },
                    { new Guid("bfc8b452-c8fb-4fc8-9707-f5389d94d6fb"), new Guid("766717b2-ec30-491e-980f-35b2e8f21a36") },
                    { new Guid("bfcb70d3-8e4e-47da-b245-11554df6101c"), new Guid("9bba5121-2e70-4e40-b48e-9ac455eb015c") },
                    { new Guid("c092bd1d-5424-42cf-b518-a7e107440fe2"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") },
                    { new Guid("c092bd1d-5424-42cf-b518-a7e107440fe2"), new Guid("e97e3fcf-e33f-4cb5-a5f9-55fc49c4a1a0") },
                    { new Guid("c092bd1d-5424-42cf-b518-a7e107440fe2"), new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd") },
                    { new Guid("cab8aa18-7480-4ed7-806f-0d80dc8beb21"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") },
                    { new Guid("cab8aa18-7480-4ed7-806f-0d80dc8beb21"), new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd") },
                    { new Guid("cadad1f0-b1af-47c7-9cc1-a9e295a15381"), new Guid("6e685927-2741-436e-b364-118ba8033218") },
                    { new Guid("d573fd17-06c1-46a9-b570-3a57a781198e"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") },
                    { new Guid("de78c549-04d7-411b-a7e3-688da4beb1a3"), new Guid("148dddbc-6a18-4fa4-a46d-6d4efd13d810") },
                    { new Guid("df3c53cd-4bf5-4990-a4b4-877fd55a2df5"), new Guid("9bba5121-2e70-4e40-b48e-9ac455eb015c") },
                    { new Guid("e08dcf45-ea18-41d9-ae77-94bfc8558183"), new Guid("6e685927-2741-436e-b364-118ba8033218") },
                    { new Guid("ed1d9793-5927-4ad9-9aee-3e5056d666c1"), new Guid("148dddbc-6a18-4fa4-a46d-6d4efd13d810") },
                    { new Guid("ef310549-80f0-4d46-aecf-c344b171b303"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") },
                    { new Guid("f2a09a1a-0faf-4c77-a7a6-c4b5e6c39672"), new Guid("148dddbc-6a18-4fa4-a46d-6d4efd13d810") },
                    { new Guid("f5e80a9a-42b2-4195-8f63-32601920c755"), new Guid("7a298ea0-49e8-4551-b112-fb7711d6e32a") },
                    { new Guid("f6fe6aae-f959-4cc1-99fc-27abb433b525"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") },
                    { new Guid("ffc437e5-dc6d-44a1-94f2-ff590c4b3cd2"), new Guid("7a298ea0-49e8-4551-b112-fb7711d6e32a") }
                });

            migrationBuilder.InsertData(
                table: "OrderDetails",
                columns: new[] { "BookId", "OrderId", "Price", "Quantity" },
                values: new object[,]
                {
                    { new Guid("602dc562-2768-49f8-ab93-f17c6689cb0b"), new Guid("438de794-1ede-42fb-8d50-5f0e98939767"), 240m, (short)1 },
                    { new Guid("797f7ead-a732-4a67-88e9-c8d19a9b3344"), new Guid("e1f06da2-5eda-4668-887c-6f87bc92653e"), 870m, (short)3 },
                    { new Guid("f5e80a9a-42b2-4195-8f63-32601920c755"), new Guid("438de794-1ede-42fb-8d50-5f0e98939767"), 120m, (short)1 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("0617034e-cdde-41f1-9c26-f6c1451db5c9"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("0617034e-cdde-41f1-9c26-f6c1451db5c9"), new Guid("ddddf1e2-dab0-4fff-b288-3d740a634826") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("0b0c81e3-b883-475f-99ee-e804694fc441"), new Guid("6e685927-2741-436e-b364-118ba8033218") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("0b0c81e3-b883-475f-99ee-e804694fc441"), new Guid("766717b2-ec30-491e-980f-35b2e8f21a36") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("16c2cf19-16e0-49e0-a954-97bdc3b1f865"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("16c2cf19-16e0-49e0-a954-97bdc3b1f865"), new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("17d798a7-6595-49a7-a6e1-c3ebde48a673"), new Guid("7a298ea0-49e8-4551-b112-fb7711d6e32a") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("18e0aa90-7a79-4b14-84f2-bf3fd409d3fe"), new Guid("766717b2-ec30-491e-980f-35b2e8f21a36") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("2188047e-7756-44f6-a678-7a7efa9d5e8b"), new Guid("148dddbc-6a18-4fa4-a46d-6d4efd13d810") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("350beb5c-aef1-4f31-996d-b3d405b82ed6"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("38f24dde-c993-4d88-9b53-cf598813f815"), new Guid("6e685927-2741-436e-b364-118ba8033218") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("39408675-5c3b-4f94-929e-5f7aa4e92031"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("39408675-5c3b-4f94-929e-5f7aa4e92031"), new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("4493bc96-6700-40da-b5f0-8f624f385632"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("4493bc96-6700-40da-b5f0-8f624f385632"), new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("4a3ab21f-c55f-48ec-85ec-95037732a52b"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("4a3ab21f-c55f-48ec-85ec-95037732a52b"), new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("54f8b531-7fb6-41d1-bf1f-f434c7faa905"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("54f8b531-7fb6-41d1-bf1f-f434c7faa905"), new Guid("ddddf1e2-dab0-4fff-b288-3d740a634826") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("59c556ca-eb2d-4d94-a9e4-7b273fed96bc"), new Guid("148dddbc-6a18-4fa4-a46d-6d4efd13d810") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("5e720b18-2217-4fc3-9919-80dbebfd5b17"), new Guid("9bba5121-2e70-4e40-b48e-9ac455eb015c") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("5eee3176-7998-42e8-8459-dd85f9412170"), new Guid("442d1f92-912c-4d45-a073-d21d7a8bd392") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("602dc562-2768-49f8-ab93-f17c6689cb0b"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("602dc562-2768-49f8-ab93-f17c6689cb0b"), new Guid("e97e3fcf-e33f-4cb5-a5f9-55fc49c4a1a0") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("61ea5103-1569-468b-92d7-bb934b47d1c3"), new Guid("766717b2-ec30-491e-980f-35b2e8f21a36") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("684a01e0-d62d-4087-ad1b-acaa4626e9ca"), new Guid("148dddbc-6a18-4fa4-a46d-6d4efd13d810") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("688c9373-179a-47ee-bb87-85fda22ae70c"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("688c9373-179a-47ee-bb87-85fda22ae70c"), new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("7172bd43-970f-4f3e-af58-55eb3815e55f"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("7172bd43-970f-4f3e-af58-55eb3815e55f"), new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("74272c7c-afc9-4d86-9ac2-b2af244966f0"), new Guid("9bba5121-2e70-4e40-b48e-9ac455eb015c") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("797f7ead-a732-4a67-88e9-c8d19a9b3344"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("797f7ead-a732-4a67-88e9-c8d19a9b3344"), new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("86400a7e-ac30-4489-a5b1-ba14f21f13dc"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("86400a7e-ac30-4489-a5b1-ba14f21f13dc"), new Guid("ddddf1e2-dab0-4fff-b288-3d740a634826") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("86400a7e-ac30-4489-a5b1-ba14f21f13dc"), new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("89f5b15b-3e3e-46ec-b77f-5fbba3a62604"), new Guid("6e685927-2741-436e-b364-118ba8033218") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("92c6f78e-375f-44c3-901f-0cc32fc2e966"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("92c6f78e-375f-44c3-901f-0cc32fc2e966"), new Guid("e97e3fcf-e33f-4cb5-a5f9-55fc49c4a1a0") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("93ee36c7-ef13-4377-b206-c3458ef318e7"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("93ee36c7-ef13-4377-b206-c3458ef318e7"), new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("94e02e22-0e4d-4273-bbf8-b4e169208a2a"), new Guid("7a298ea0-49e8-4551-b112-fb7711d6e32a") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("962a5dc2-31df-4a77-9905-be964050a687"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("962a5dc2-31df-4a77-9905-be964050a687"), new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("a583a9ea-035a-4838-a0d7-e8374c0b205b"), new Guid("766717b2-ec30-491e-980f-35b2e8f21a36") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("aba8e784-2865-4962-8a8a-231234435067"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("aba8e784-2865-4962-8a8a-231234435067"), new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("ac0e8f04-e390-4049-8685-15c7b74a44e8"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("ac0e8f04-e390-4049-8685-15c7b74a44e8"), new Guid("ddddf1e2-dab0-4fff-b288-3d740a634826") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("ac67e8c4-b93c-4ce0-8d57-2ac6c173446c"), new Guid("8bd0506a-2d5c-462f-aef9-2667f3b3ecfe") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("ac67e8c4-b93c-4ce0-8d57-2ac6c173446c"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("ac67e8c4-b93c-4ce0-8d57-2ac6c173446c"), new Guid("ddddf1e2-dab0-4fff-b288-3d740a634826") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("afa9b1ab-a039-49ec-9492-e523c9c9fcba"), new Guid("7a298ea0-49e8-4551-b112-fb7711d6e32a") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("b13a18d3-673c-4851-b792-e6080a3b3ac1"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("b13a18d3-673c-4851-b792-e6080a3b3ac1"), new Guid("e97e3fcf-e33f-4cb5-a5f9-55fc49c4a1a0") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("b13a18d3-673c-4851-b792-e6080a3b3ac1"), new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("b40c9898-55fb-4979-bce7-19e8c1a5f285"), new Guid("766717b2-ec30-491e-980f-35b2e8f21a36") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("bc6eed6b-697a-4bb1-ad90-4ee13102b826"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("bc6eed6b-697a-4bb1-ad90-4ee13102b826"), new Guid("ddddf1e2-dab0-4fff-b288-3d740a634826") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("bc6eed6b-697a-4bb1-ad90-4ee13102b826"), new Guid("e97e3fcf-e33f-4cb5-a5f9-55fc49c4a1a0") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("bf199974-924e-4c05-a028-6bb93b5da8bf"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("bf199974-924e-4c05-a028-6bb93b5da8bf"), new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("bfc8b452-c8fb-4fc8-9707-f5389d94d6fb"), new Guid("6e685927-2741-436e-b364-118ba8033218") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("bfc8b452-c8fb-4fc8-9707-f5389d94d6fb"), new Guid("766717b2-ec30-491e-980f-35b2e8f21a36") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("bfcb70d3-8e4e-47da-b245-11554df6101c"), new Guid("9bba5121-2e70-4e40-b48e-9ac455eb015c") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("c092bd1d-5424-42cf-b518-a7e107440fe2"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("c092bd1d-5424-42cf-b518-a7e107440fe2"), new Guid("e97e3fcf-e33f-4cb5-a5f9-55fc49c4a1a0") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("c092bd1d-5424-42cf-b518-a7e107440fe2"), new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("cab8aa18-7480-4ed7-806f-0d80dc8beb21"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("cab8aa18-7480-4ed7-806f-0d80dc8beb21"), new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("cadad1f0-b1af-47c7-9cc1-a9e295a15381"), new Guid("6e685927-2741-436e-b364-118ba8033218") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("d573fd17-06c1-46a9-b570-3a57a781198e"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("de78c549-04d7-411b-a7e3-688da4beb1a3"), new Guid("148dddbc-6a18-4fa4-a46d-6d4efd13d810") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("df3c53cd-4bf5-4990-a4b4-877fd55a2df5"), new Guid("9bba5121-2e70-4e40-b48e-9ac455eb015c") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("e08dcf45-ea18-41d9-ae77-94bfc8558183"), new Guid("6e685927-2741-436e-b364-118ba8033218") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("ed1d9793-5927-4ad9-9aee-3e5056d666c1"), new Guid("148dddbc-6a18-4fa4-a46d-6d4efd13d810") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("ef310549-80f0-4d46-aecf-c344b171b303"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("f2a09a1a-0faf-4c77-a7a6-c4b5e6c39672"), new Guid("148dddbc-6a18-4fa4-a46d-6d4efd13d810") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("f5e80a9a-42b2-4195-8f63-32601920c755"), new Guid("7a298ea0-49e8-4551-b112-fb7711d6e32a") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("f6fe6aae-f959-4cc1-99fc-27abb433b525"), new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc") });

            migrationBuilder.DeleteData(
                table: "BookGenres",
                keyColumns: new[] { "BookId", "GenreId" },
                keyValues: new object[] { new Guid("ffc437e5-dc6d-44a1-94f2-ff590c4b3cd2"), new Guid("7a298ea0-49e8-4551-b112-fb7711d6e32a") });

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: new Guid("adeac2fd-98b8-46a1-b2db-6ae19cee44fa"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumns: new[] { "BookId", "OrderId" },
                keyValues: new object[] { new Guid("602dc562-2768-49f8-ab93-f17c6689cb0b"), new Guid("438de794-1ede-42fb-8d50-5f0e98939767") });

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumns: new[] { "BookId", "OrderId" },
                keyValues: new object[] { new Guid("797f7ead-a732-4a67-88e9-c8d19a9b3344"), new Guid("e1f06da2-5eda-4668-887c-6f87bc92653e") });

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumns: new[] { "BookId", "OrderId" },
                keyValues: new object[] { new Guid("f5e80a9a-42b2-4195-8f63-32601920c755"), new Guid("438de794-1ede-42fb-8d50-5f0e98939767") });

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("036590f1-41f9-4518-9ce5-5d2c0b6de155"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("f79f3e1f-fca1-4eec-b1d8-c4adcda2a2c8"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("0617034e-cdde-41f1-9c26-f6c1451db5c9"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("0b0c81e3-b883-475f-99ee-e804694fc441"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("16c2cf19-16e0-49e0-a954-97bdc3b1f865"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("17d798a7-6595-49a7-a6e1-c3ebde48a673"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("18e0aa90-7a79-4b14-84f2-bf3fd409d3fe"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("2188047e-7756-44f6-a678-7a7efa9d5e8b"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("350beb5c-aef1-4f31-996d-b3d405b82ed6"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("38f24dde-c993-4d88-9b53-cf598813f815"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("39408675-5c3b-4f94-929e-5f7aa4e92031"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("4493bc96-6700-40da-b5f0-8f624f385632"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("4a3ab21f-c55f-48ec-85ec-95037732a52b"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("54f8b531-7fb6-41d1-bf1f-f434c7faa905"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("59c556ca-eb2d-4d94-a9e4-7b273fed96bc"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("5e720b18-2217-4fc3-9919-80dbebfd5b17"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("5eee3176-7998-42e8-8459-dd85f9412170"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("602dc562-2768-49f8-ab93-f17c6689cb0b"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("61ea5103-1569-468b-92d7-bb934b47d1c3"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("684a01e0-d62d-4087-ad1b-acaa4626e9ca"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("688c9373-179a-47ee-bb87-85fda22ae70c"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("7172bd43-970f-4f3e-af58-55eb3815e55f"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("74272c7c-afc9-4d86-9ac2-b2af244966f0"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("797f7ead-a732-4a67-88e9-c8d19a9b3344"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("86400a7e-ac30-4489-a5b1-ba14f21f13dc"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("89f5b15b-3e3e-46ec-b77f-5fbba3a62604"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("92c6f78e-375f-44c3-901f-0cc32fc2e966"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("93ee36c7-ef13-4377-b206-c3458ef318e7"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("94e02e22-0e4d-4273-bbf8-b4e169208a2a"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("962a5dc2-31df-4a77-9905-be964050a687"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("a583a9ea-035a-4838-a0d7-e8374c0b205b"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("aba8e784-2865-4962-8a8a-231234435067"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("ac0e8f04-e390-4049-8685-15c7b74a44e8"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("ac67e8c4-b93c-4ce0-8d57-2ac6c173446c"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("afa9b1ab-a039-49ec-9492-e523c9c9fcba"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("b13a18d3-673c-4851-b792-e6080a3b3ac1"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("b40c9898-55fb-4979-bce7-19e8c1a5f285"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("bc6eed6b-697a-4bb1-ad90-4ee13102b826"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("bf199974-924e-4c05-a028-6bb93b5da8bf"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("bfc8b452-c8fb-4fc8-9707-f5389d94d6fb"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("bfcb70d3-8e4e-47da-b245-11554df6101c"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("c092bd1d-5424-42cf-b518-a7e107440fe2"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("cab8aa18-7480-4ed7-806f-0d80dc8beb21"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("cadad1f0-b1af-47c7-9cc1-a9e295a15381"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("d573fd17-06c1-46a9-b570-3a57a781198e"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("de78c549-04d7-411b-a7e3-688da4beb1a3"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("df3c53cd-4bf5-4990-a4b4-877fd55a2df5"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("e08dcf45-ea18-41d9-ae77-94bfc8558183"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("ed1d9793-5927-4ad9-9aee-3e5056d666c1"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("ef310549-80f0-4d46-aecf-c344b171b303"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("f2a09a1a-0faf-4c77-a7a6-c4b5e6c39672"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("f5e80a9a-42b2-4195-8f63-32601920c755"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("f6fe6aae-f959-4cc1-99fc-27abb433b525"));

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: new Guid("ffc437e5-dc6d-44a1-94f2-ff590c4b3cd2"));

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: new Guid("148dddbc-6a18-4fa4-a46d-6d4efd13d810"));

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: new Guid("442d1f92-912c-4d45-a073-d21d7a8bd392"));

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: new Guid("6e685927-2741-436e-b364-118ba8033218"));

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: new Guid("766717b2-ec30-491e-980f-35b2e8f21a36"));

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: new Guid("7a298ea0-49e8-4551-b112-fb7711d6e32a"));

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: new Guid("8bd0506a-2d5c-462f-aef9-2667f3b3ecfe"));

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: new Guid("9bba5121-2e70-4e40-b48e-9ac455eb015c"));

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: new Guid("c9a3dbd9-2172-4ad8-94d6-59ba454705cc"));

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: new Guid("ddddf1e2-dab0-4fff-b288-3d740a634826"));

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: new Guid("e97e3fcf-e33f-4cb5-a5f9-55fc49c4a1a0"));

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: new Guid("ee220cce-8814-43ba-a8cb-5fdc0b8b59bd"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("02714691-8d89-4980-b2aa-1b141e80f56a"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("0ed3b9f8-bc8e-46f2-89a6-0220cc2894f3"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("12e9aa82-0433-401d-9023-a9ca493ea179"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("1372c186-8bac-4d1d-9ce0-cec3eed85400"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("2fad2e23-b525-4332-a934-55dfed8000cd"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("30e1db22-c8e8-47bf-8955-e3e00907a26b"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("3109af45-ba2b-47a2-9c50-8c007613605f"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("3440424a-aa80-44ff-be38-e43d5bc682ca"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("34528118-7296-4c00-ab29-e5d3527e8b77"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("35ef4bd1-1f6c-4ee0-b46c-8ff5888ac38f"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("379e25f5-1c74-4500-95d2-0470934b5eb2"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("41b0dc17-889a-4907-99fc-2f49bf6b8800"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("50913db2-b104-4d70-a69f-b79d24420415"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("5494f0f2-f436-4484-b0a1-891e157ff38d"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("549d5647-68e4-4f89-9b3a-5caf2182b346"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("562253df-de02-4336-8109-b10f01438742"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("5e210f19-8a1c-4948-975c-52a5928d5321"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("69996b5e-e2ec-422b-bccc-17f964daba5d"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("69d83a5e-2d96-4f7e-a412-33b31a77f849"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("7404bbbc-bcab-47b9-8d4d-7a856c747d22"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("7639ba87-5278-43a1-9b2b-ade053b8f466"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("7e9646ae-e171-4d73-8f4b-b75e287c056c"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("81db6a68-66ef-47b3-926a-07598804df9b"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("8340d844-bedf-4089-81b3-1e8ef7ee2146"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("8add3043-422f-4695-9680-8a06f26a19f3"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("942aebca-56b1-4e58-a89c-5724f4b5d94f"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("96b4741e-5304-4b68-8aaf-0095795bc0d5"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("96f11f14-d899-43ec-bb90-43b871b75b68"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("987f3d90-0c0b-485e-b175-e25c1e9f6eee"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("aa938510-5d23-4c14-a644-a5e4f28340b5"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("ababf6b8-374d-4dd2-bc1c-28ee5031e463"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("b0f57400-9491-4452-b1e2-7f2854eb9155"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("b2b95678-0797-45ae-8471-dbc476ec45ef"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("b5b2fa2f-3022-4c15-88fd-759ac416f881"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("bb3b661c-7c4c-46ed-8a1d-7bcd1eeb9fd5"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("bfbd4d2b-84f6-4ae6-bf10-7e3ccd67c80c"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("ca2629c3-4bf9-43f2-b699-0e16ae65d1a6"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("d5404759-d9f0-4dd0-abb4-b8dead014a3a"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("d8206aaa-9b66-4141-a990-6719c25dd8e7"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("dd297ede-c0fa-476a-90a1-662e9aa27d7a"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("e6bc8173-af91-4228-a49e-642c9172a5e0"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("ec7d0a17-8fc7-4f15-9410-0b9e187f789c"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("f069e340-ea06-4af4-b257-53835f9250d3"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("f3ec65d8-2f2e-44c2-82f1-292267763bb4"));

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("86cdea4b-f3df-4b87-b59a-3818c3b8e71a"));

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("e999c968-8637-4152-92e9-60805fc23438"));

            migrationBuilder.DeleteData(
                table: "Languages",
                keyColumn: "Id",
                keyValue: new Guid("49077420-6517-46c1-b29e-223c1736593c"));

            migrationBuilder.DeleteData(
                table: "Languages",
                keyColumn: "Id",
                keyValue: new Guid("d617c829-4595-45ef-ad64-629a9720607e"));

            migrationBuilder.DeleteData(
                table: "Languages",
                keyColumn: "Id",
                keyValue: new Guid("e5515da2-5fdb-4d85-8827-4b23b2b16e3c"));

            migrationBuilder.InsertData(
                table: "Authors",
                columns: new[] { "Id", "FullName" },
                values: new object[,]
                {
                    { new Guid("003966d2-f5fd-45a2-9973-8f53f355b374"), "Джо Аберкромби" },
                    { new Guid("04d6e5bf-ce01-4d24-a858-978abcd391a1"), "Пол Парсонс" },
                    { new Guid("0a697f2a-9c3c-47b6-8fb3-4cfc7066fd83"), "Стівен Кінг" },
                    { new Guid("0ee07cb8-2b4f-49cd-ac3f-1bdd6f5a5bbc"), "Боріс Джонсон" },
                    { new Guid("0f208bab-8a58-4d6f-85cd-476f25a6145e"), "Рейчел Ліппінкот" },
                    { new Guid("15069387-a9c3-43a2-ad25-8f7493b37410"), "Річард Докінз" },
                    { new Guid("192a13dc-d53f-4b84-acad-951df2244351"), "Джон Грін" },
                    { new Guid("1e747a08-731f-4723-80da-01a0dc3dff26"), "Пауло Коєльо" },
                    { new Guid("20ca17d1-2f41-4e58-b7b5-0b5a7c8724ce"), "Ден Дубравін" },
                    { new Guid("254ac9fa-05b1-4ac5-9aec-9b36bba55167"), "Кербі Розанес" },
                    { new Guid("29959a5f-3263-47c3-985a-42441771de74"), "Роальд Дал" },
                    { new Guid("3299e2da-00c6-4ac0-8d8e-06e9b6632203"), "Адам Кей" },
                    { new Guid("38e393aa-2f14-41e4-9894-8d212f07400d"), "Деніел Кіз" },
                    { new Guid("50170e48-862f-45f2-9fba-60bfd74c5ae8"), "Джон Грей" },
                    { new Guid("5f8b0ecb-2247-4725-9b5e-63e4edb8b749"), "Жоель Діккер" },
                    { new Guid("6345ac34-35f7-45e2-9dda-841894f9661f"), "Олександр Красовицький" },
                    { new Guid("64b80a39-72b3-49e5-8660-de4fb8e368e4"), "Ірвінг Стоун" },
                    { new Guid("6ee6d936-9f76-4eb0-8fc0-dd51cbebbecc"), "Крістін Мюльке" },
                    { new Guid("7c440fd0-6b3d-4d30-86a9-d87b810a8e64"), "Ерік Берн" },
                    { new Guid("87254d48-87de-4719-b811-1ce2d8f8513e"), "Джордж Гілдер" },
                    { new Guid("8b0384f2-3950-4d3f-96ad-17a597154149"), "Марк Менсон" },
                    { new Guid("96f6510c-154b-4459-b9a1-194ea20526c7"), "Ренсом Ріггз" },
                    { new Guid("97106cd6-8c27-4306-bf90-9d14bdd32f75"), "Джен Сінсеро" },
                    { new Guid("9ac72d18-80c2-44f9-9101-f50fb405f3ab"), "Вальтер Тевіс" },
                    { new Guid("9d239583-1b8b-47e6-8102-556822f5eeab"), "Маршалл Розенберг" },
                    { new Guid("9f39ee28-f276-4815-81bd-de8b4f96d0cd"), "Саша Сколиченко" },
                    { new Guid("a7f3f865-d45b-4234-8665-9e04e7f8660c"), "Ірина Василенко" },
                    { new Guid("a85c843e-60bd-4bd8-a126-8b853cfcd502"), "Ден Браун" },
                    { new Guid("ad23527e-00e4-4ab1-8eb2-f1d371009037"), "Вахтанг Кіпіані" },
                    { new Guid("b46f0991-27e7-4826-99f7-a75941162c03"), "Джоан Роулінг" },
                    { new Guid("b5ab23dd-4139-4212-bd3e-d9c155924fc0"), "Стефані Шталь" },
                    { new Guid("b8b16048-4f4a-43ee-843b-025a4de23f7f"), "Джейн Остен" },
                    { new Guid("b90a3d56-4ca7-41a1-86fb-0dc3f124460c"), "Анна Тодд" },
                    { new Guid("ba043179-7347-4cd0-95bf-57124a484409"), "Джордж Орвелл" },
                    { new Guid("bc9a9e5e-4c52-4cf9-9e61-0a892dae4b16"), "Джоан Гарріс" },
                    { new Guid("bd37bc3a-970e-4788-bcdf-957af81e3a46"), "Нассим Таллеб" },
                    { new Guid("bdbfdb10-0979-402b-a2ac-5f192c73ada2"), "Стівен Хокінг" },
                    { new Guid("bfa2d8d6-98cf-4154-b82b-4f6828f7a2c9"), "Ювал Ной Харарі" },
                    { new Guid("c15afa54-56d0-45cd-8115-32753e5f18e1"), "Томас Кеніллі" },
                    { new Guid("ca3d2c69-2c73-4cd7-a30a-d8ed2f31e566"), "Наталя Вотсон" },
                    { new Guid("cbdd691b-df84-4bb7-830f-e379440b8c36"), "Гєри Чепмен" },
                    { new Guid("e56005b5-51ee-4c55-bd6e-576f1db5f847"), "Берджесс Ентоні" }
                });

            migrationBuilder.InsertData(
                table: "Authors",
                columns: new[] { "Id", "FullName" },
                values: new object[,]
                {
                    { new Guid("ed2414f2-f485-4b8c-bda9-3d52f1ad545b"), "Анна Мойсеенко" },
                    { new Guid("f7ab551c-7b22-4ca5-a116-1110c22bcb4f"), "Джон Фаулз" }
                });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("1aa747e3-b416-4c8c-a8db-c0f96ccbcd57"), "Фікшн література" },
                    { new Guid("3c358b2b-0b33-4817-a26d-63339c0afce2"), "Нон-фікшн література" }
                });

            migrationBuilder.InsertData(
                table: "Languages",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), "Українська" },
                    { new Guid("8a9f010f-457d-4c76-85f3-9ef59df2eb53"), "Російська" },
                    { new Guid("ae8984c3-d127-47f1-be98-3491486060ac"), "Англійська" }
                });

            migrationBuilder.UpdateData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("438de794-1ede-42fb-8d50-5f0e98939767"),
                columns: new[] { "CustomerId", "OrderDate" },
                values: new object[] { new Guid("ab8c180b-8bec-43f1-ae3d-c0d7c2a07008"), new DateTime(2022, 6, 17, 7, 36, 56, 121, DateTimeKind.Local).AddTicks(6397) });

            migrationBuilder.UpdateData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("e1f06da2-5eda-4668-887c-6f87bc92653e"),
                columns: new[] { "CustomerId", "OrderDate" },
                values: new object[] { new Guid("ab8c180b-8bec-43f1-ae3d-c0d7c2a07008"), new DateTime(2022, 6, 17, 7, 36, 56, 121, DateTimeKind.Local).AddTicks(6455) });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "City", "Email", "FirstName", "LastName", "Password", "Phone", "PostalCode", "Role", "StoredSalt" },
                values: new object[,]
                {
                    { new Guid("a1ad8f8d-33f0-4f91-90d5-102415ce770f"), "Харків", "admin@gmail.com", "Admin", "Also admin", "jovnJbl7fEPIm5e1JHo0gCIJHfDiTWQFT5a/XEf7fIo=", "+38099445165", "61015", "Administrator", new byte[] { 179, 137, 193, 187, 182, 238, 115, 74, 37, 215, 68, 238, 63, 47, 56, 183 } },
                    { new Guid("ab8c180b-8bec-43f1-ae3d-c0d7c2a07008"), "Київ", "user@gmail.com", "Іван", "Сірко", "KI1tvZIeiXKBLWJAC7ec4NWjoSkcaTJTdrRDMiwANs8=", "+380671239253", "61600", "User", new byte[] { 67, 92, 15, 46, 125, 178, 137, 130, 148, 214, 71, 138, 132, 73, 134, 237 } }
                });

            migrationBuilder.InsertData(
                table: "Books",
                columns: new[] { "Id", "AuthorId", "Code", "Description", "IsBestseller", "IsNew", "LanguageId", "NumberOfPages", "Price", "Title" },
                values: new object[,]
                {
                    { new Guid("0b2cceb2-629d-46db-9578-b3d2b96e92cd"), new Guid("bfa2d8d6-98cf-4154-b82b-4f6828f7a2c9"), "000022", "Протягом минулого століття людству вдалося зробити неможливе: приборкати голод, епідемії і війни. Здається, що в це важко повірити, проте, як пояснює Харарі в своєму фірмовому стилі - досконалому і захопливому - голод, епідемія і війна трансформувалися з незрозумілих і неконтрольованих сил природи у виклики, якими можна керувати. Вперше в історії більше людей помирає від переїдання, ніж від голодування; більше людей помирає від старості, ніж від інфекційних захворювань; і більше людей скоюють самогубство, ніж вбито солдатами, терористами і злочинцями взятими разом. У середнього американця в тисячу разів більше можливостей померти від харчування у МакДоналдсі, ніж бути вбитим Аль Каїдою.", false, false, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 512, 250m, "Homo Deus. Людина божественна. За лаштунками майбутнього" },
                    { new Guid("0e9ce81e-0296-4c2b-9302-255a24ef999f"), new Guid("bc9a9e5e-4c52-4cf9-9e61-0a892dae4b16"), "000049", "Найголовніше сховано всередині. Фрамбуаза отримала дивний спадок від матері. Її брату дісталася ферма, старшій сестрі — винний погріб, сповнений бурштиново-сонячних пляшок, а Фрамбуазі — альбом з кулінарними рецептами. Не багацько… Але на берегах зошита впереміш зі старовинними рецептами різноманітних смаколиків вона знаходить загадкові нотатки. Секрети та зізнання матері. Життя, яке вона так ретельно приховувала від дітей, у часи війни й окупації. Маленькі радощі й прикрощі, потаємні думки, дитячі образи та щира сповідь жінки, що боялася любити. Минуло стільки років. Лишилося стільки запитань. Відповіді на них — у цьому старому щоденнику. Настав час розкрити моторошні таємниці минулого.", false, false, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 352, 131m, "П'ять четвертинок апельсина" },
                    { new Guid("1365032e-6311-4f78-8118-bd441aaccee2"), new Guid("b46f0991-27e7-4826-99f7-a75941162c03"), "000053", "Перша частина пригод Гаррі Поттера — книга №1 для маленьких читачів. Саме вона сколихнула хвилю любові до читання, яка захопила цілий світ! Такого успіху не мала жодне художне ходожнє видання, доки 1997 року на полицях англійських магазинів не з'явилася перша книга Джоан Роулінг «Гаррі Поттер і філософський камінь». Сирота на ім'я Гаррі живе під сходами у будинку дядька ті тітоньски Дурслі. Він поки що не знає, як потрапити на платформу дев'ять і три чверті, користуватися чарівною паличкою, та літати на мітлі. Але одного разу до нього прилетить сова із загаднковим листом, який запрошує його до навчання у Школі Чарів та Чаклунства, і з того дня його життя докорінно зміниться...", true, false, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 320, 210m, "Гаррі Поттер і філософський камінь" },
                    { new Guid("13e2ae3c-09b5-46c3-868d-a0cb8e30801e"), new Guid("a85c843e-60bd-4bd8-a126-8b853cfcd502"), "000050", "Ця історія про неймовірне розслідування і приголомшливі відкриття Роберта Ленґдона та Софі Неве, без перебільшення, перевернула світ. Ден Браун створив легенду, в яку повірили всі, — попри відчайдушний опір Ватикану. Скандали навкруг роману, здається, не вгамуються ніколи, чому сприяє недавня його екранізація. А секрет цього величезного успіху полягає в тому, що Денові Брауну вдалося, як нікому до нього, наочно довести, що так звана історія та політика — це тільки ширма, за якою приховані величезні таємниці...", true, false, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 480, 240m, "Код Да Вінчі" },
                    { new Guid("17c9218e-04b7-47fc-969e-53872d8ac017"), new Guid("9f39ee28-f276-4815-81bd-de8b4f96d0cd"), "000033", "Депресія — це не вигадка, не відмовка й не вияв лінощів. Це справжня серйозна проблема, яку варто й потрібно розв’язувати. То що ж робити, коли світ навколо стає темносірим і звичні приємності вже давно не радують? Чи можна якось цьому зарадити? Шукайте відповіді й натхнення в чудовій арт-книжці Саші Скочиленко!", false, false, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 48, 190m, "Книжка про депресію" },
                    { new Guid("1c2c89ce-86c8-4751-8917-247052fab5d7"), new Guid("ed2414f2-f485-4b8c-bda9-3d52f1ad545b"), "000030", "Відкрийте світ традиційної польської кухні! Капустняк та білий борщ, ароматні супи, відварний яловичий язик та ніжний рулет із грудинки з пікантними травами, качка та курка по-польськи, знаменитий бігос, риба у різноманітних соусах, жарке зі свинини, солодкі пляцки та сирники, фруктові супи з вишні , пишні бабки та хрумке печиво – найкращі рецепти приголомшливих страв польської кухні, класичні частування та їх оригінальні сучасні інтерпретації. Відкрийте для себе неймовірну різноманітність смаку національних кухонь світу! Адже кожна з них – це унікальні та самобутні страви, яскраві та незвичайні гастрономічні відкриття! Сміливо додайте щось нове у свої кулінарні будні з рецептами найпопулярніших національних частування.", false, false, new Guid("8a9f010f-457d-4c76-85f3-9ef59df2eb53"), 72, 140m, "Польская кухня" },
                    { new Guid("1fd7bd85-befa-4d7d-9ff1-671eee40ff13"), new Guid("87254d48-87de-4719-b811-1ce2d8f8513e"), "000023", "Дивовижна здатність Google «шукати та сортувати» приваблює весь світ своєю пошуковою системою і незліченними іншими бонусами — відео, картами, електронною поштою, календарями... І все, що вона пропонує, є безкоштовним, або принаймні так здається. Замість платити безпосередньо, користувачі долучаються до реклами. Попри те, що розвиток штучного інтелекту викликає марення всемогутності й трансцендентності, Кремнієва долина майже відмовилася від безпеки. Інтернет-брандмауери, які нібито захищають усі ці паролі й особисту інформацію, виявилися безнадійно проникними. У книзі «Життя після Google» Джордж Ґілдер — неперевершений візіонер технологій і культури — пояснює, чому Кремнієва долина страждає на нервовий зрив і на що очікувати, коли настає ера після Google.", false, false, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 320, 320m, "Життя після Google.Занепад великих даних і становлення блокчейн-економіки" },
                    { new Guid("2341255d-748a-4e7f-a0de-886c9dadabdd"), new Guid("e56005b5-51ee-4c55-bd6e-576f1db5f847"), "000037", "Один із найпопулярніших і найулюбленіших серед критиків графічний роман усіх часів, відзначений нагородами шедевр Ніла Ґеймана «СЕНДМЕН» установив нові стандарти для дорослої та ліричної фантастики у галузі коміксів. Проілюстрована популярними художниками жанру, ця серія — насичений коктейль із сучасної та давньої міфології, у який майстерно вплетено сучасну літературу, історичну драму та легенди. Перша з дванадцяти книг, які складають повну серію про «СЕНДМЕНА», відкриває унікальну для графічної літератури сагу і знайомить читачів із чарівним і темним світом снів та жахіть — домом Морфея, Повелителя снів, і його роду, Безмежних.", true, false, new Guid("8a9f010f-457d-4c76-85f3-9ef59df2eb53"), 256, 67m, "Заводной апельсин" },
                    { new Guid("23f52275-0d2d-4363-9e8b-ce64a7eb7a7f"), new Guid("5f8b0ecb-2247-4725-9b5e-63e4edb8b749"), "000003", "Одного грудневого ранку на підлозі в номері 622 розкішного готелю в швейцарських Альпах знаходять труп. Поліції так і не вдається розкрити справу, а з плином часу чимало людей забуло про те, що сталося. Через багато років письменник Жоель Діккер приїжджає в той самий готель, щоб оговтатися від розриву стосунків. Він навіть не уявляє, що закінчить розслідувати давній злочин, та ще й не сам: Скарлетт, прекрасна гостя й письменниця-початківка, що зупинилась у сусідньому номері, буде супроводжувати його в пошуку, а також намагатиметься збагнути, як написати хорошу книжку. Що сталося тієї ночі? Відповідь на це питання знайдемо в цьому диявольському детективі, побудованому з точністю швейцарського годинника.", true, false, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 712, 280m, "Загадка 622" },
                    { new Guid("2eab26b3-7493-4192-879e-fe499b121646"), new Guid("b46f0991-27e7-4826-99f7-a75941162c03"), "000046", "Пропонуємо вашій увазі шосту книгу про Гаррі Поттера - «Гаррі Поттер і Принц-напівкровка»! Після подій у Відділі таємниць у Міністерства магії не залишилося сумнівів - Темний Лорд повернувся. Тепер він діє відкрито, викрадаючи чарівників та вчиняючи терор у всій Британії. У книзі «Гаррі Поттер і напівкровний Принц» Джоан Роулінг занурює читачів у похмуру атмосферу очікування небезпеки. Тепер темна магія — не рідкість на сторінках роману, і героям доведеться протистояти їй. Хто завгодно може виявитися зрадником. Темрява згущується над Гоґвортсом і настає битва. Чи встигнуть наші герої зупинити вторгнення Пожирателів смерті?", false, false, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 576, 240m, "Гаррі Поттер і Напівкровний Принц" },
                    { new Guid("329ff545-1784-4dc0-b9ee-15bff62ae53d"), new Guid("ca3d2c69-2c73-4cd7-a30a-d8ed2f31e566"), "000020", "Ця інтерактивна книжка занурить вас у мандри, які розпочнуться у Великій Британії у 1600-х димним брунатним пивом та завершаться сучасними пивними інноваціями, проведе крізь час та простір і познайомить з історією найвідоміших стилів сучасності — з німецькими лагерами, стаутами, портерами, пільзнерами, ІРА, кислим пивом і не лише. Кожний розділ зосереджує увагу на одному з основних складників пива — солоді, воді, хмелі чи дріжджах — і розповідає, як вони змінювалися з часом, як виникали нові смаки, аромати й стилі. До кожного прикладу такої зміни Наталя Вотсон пропонує сучасний зразок пива, який допоможе сповна відчути смак історії та простежити еволюцію пива. «Пиво: еволюція смаку в 50 стилях» містить п’ять сторіч пивних знань, історії та цікаві факти, які дають можливість по-новому поглянути на пізнання одного з найстаріших напоїв світу, який і досі з нами.", false, true, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 216, 300m, "Пиво. Еволюція смаку в 50 стилях" },
                    { new Guid("33e093c6-0d8c-4917-a6ab-aa4e63745c91"), new Guid("96f6510c-154b-4459-b9a1-194ea20526c7"), "000009", "Руїни великого міста... Невідома небезпека, що таїться серед зруйнованих будинків... Тут виявилися Джейкоб та його друзі — мешканці будинку дивних дітей. Кожен із них наділений дивовижними здібностями, і всі вони хочуть лише одного — зняти страшне закляття з наставниці міс Сапсан. Вороги, жорстокі монстри з порожніми очима, невідступно переслідують героїв. Джейкоб навіть уявити було, які пастки їм приготували. Він готовий будь-що врятувати міс Сапсан і свою кохану Емму. Чому ж Емма сама просить його піти? Як може Джейкоб чути наближення Порожніх? Звідки в нього дар розуміти монстрів і наказувати ними?", true, true, new Guid("8a9f010f-457d-4c76-85f3-9ef59df2eb53"), 464, 240m, "Город Пустых. Побег из дома странных детей" },
                    { new Guid("3850c786-2726-4166-90f8-f4ba9d05836e"), new Guid("7c440fd0-6b3d-4d30-86a9-d87b810a8e64"), "000015", "Доктор Ерік Берн розкриває таємні прийоми, які керують нашим життям. Усі ми, не усвідомлюючи цього, постійно граємо в ігри – подружні та сексуальні, серйозні ігри з керівниками та суперницькі ігри з друзями, в ігри, що визначають статус та сімейні сутички.  Ерік Берн детально описує понад 120 ігор, у які ми граємо самі та у які нас залучають навколишні. А також дає поради для тих, хто хоче навчитись протистояти прийомам ігор, у які вас намагаються втягнути. Він допомагає проаналізувати нюанси поведінки, позбавитись стереотипів у спілкуванні й дає розуміння сутності людських вчинків. Книжка, що вийшла майже 40 років тому, і досі є популярною та актуальною!", false, false, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 256, 220m, "Ігри, у які грають люди" },
                    { new Guid("3b263589-953d-430d-a22d-26d51fa77c05"), new Guid("29959a5f-3263-47c3-985a-42441771de74"), "000004", "Всім шанувальникам творчості Джоан Роулінг, без сумніву, буде цікаво познайомитися з цією книгою, автора якої, Роальда Дала, прийнято вважати «літературним батьком» знаменитої письменниці. Хоча, можливо, цей сюжет вам уже знайомий, адже «Чарлі та шоколадна фабрика» – найпопулярніша книга Дала. Бідолашному хлопчику, який на кожен день народження отримував у подарунок лише маленький шоколадний батончик, чекає дивовижна пригода, адже його доброта є прикладом для інших, а добро завжди отримує свою нагороду.", true, false, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 240, 170m, "Чарлі і шоколадна фабрика" },
                    { new Guid("3d918f1a-d700-4c81-a5fa-06b66e893658"), new Guid("c15afa54-56d0-45cd-8115-32753e5f18e1"), "000005", "Той, хто врятував єдине життя, врятував увесь світ» – ці слова з Талмуда написали ув'язнені на кільці, яке подарували своєму рятівнику – Оскару Шиндлеру. Дія роману ґрунтується на справжніх подіях, що відбувалися в окупованій Польщі під час Другої світової війни. Німецький промисловець, начальник концентраційного табору Оскар Шиндлер поодинці врятував від смерті в газових камерах більше людей, ніж будь-хто за всю історію війни.", false, false, new Guid("8a9f010f-457d-4c76-85f3-9ef59df2eb53"), 480, 190m, "Список Шиндлера" },
                    { new Guid("4183bc98-b2ca-471e-9d31-246ebf86f28f"), new Guid("6ee6d936-9f76-4eb0-8fc0-dd51cbebbecc"), "000021", "Замість написати підручник, я зібрав під однією обкладинкою найпростішу та найцікавішу інформацію— і вам варто прочитати цю книжку від початку до кінця! Використайте її, щоб зрозуміти основи, а потім повертайтеся назад і перечитайте вже після того, як склали власну думку, щоб збагнути, які місця в ній можуть бути для вас корисними. Можливо, коли ви з’ясуєте, що любите легкі, ароматні білі вина, а не фруктові та повнотілі, як вважали раніше, вам вдасться відкрити на дегустаціях нові сорти. Пізніше використовуйте книжку як довідник, коли, скажімо, ви готові випити вина на день народження або коли потрапили в Шампань і хочете скуштувати щось, окрім традиційного стилю екстра-брют. Тож нехай ці рядки стануть вашим посібником, з яким ви будете купувати вина, дегустувати їх і впевнено навчатися. На цих сторінках вам трапиться багато технічних термінів, які виз найдете у глосарії.", false, false, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 272, 500m, "Wine Simple. Про вино від сомельє світового класу" },
                    { new Guid("45ee2451-3709-4c53-9991-f01d240687eb"), new Guid("bdbfdb10-0979-402b-a2ac-5f192c73ada2"), "000038", "Коротка історія часу Стівена Хокінга - один із стовпів сучасної науково-популярної літератури. У цій книзі автор намагається відповісти на найскладніші та інтригуючі питання про світобудову: коли і чому почався Всесвіт? чи безмежний Всесвіт чи має межу? Чи завжди час проходив уздовж звичного для нас вектора? чи можливі подорожі у часі? що очікує космос у далекому майбутньому? Стівен Хокінг не робить однозначних висновків, оскільки на такі не наважується поки що ніхто на передньому краї науки. Однак він перекладає живою та образною мовою найскладніші положення теоретичної фізики та переносить читача на дивовижні простори антиматерії, суперструн, кварків, чорних дірок та гравітаційних хвиль — туди, де Бог здається ще більш незбагненним та всемогутнім. Вдаючись до несподіваних образів, а не формул - виняток становить лише ейнштейнівська Е = mc2, - Стівен Хокінг наближає нас до розгадки таємниці, що лежить у самому серці Всесвіту.", true, false, new Guid("8a9f010f-457d-4c76-85f3-9ef59df2eb53"), 272, 180m, "Краткая история времени" },
                    { new Guid("4a635251-3a99-429d-99a0-5f0d0e7dea25"), new Guid("20ca17d1-2f41-4e58-b7b5-0b5a7c8724ce"), "000024", "Управління гнівом – книга, в якій зібрано колекцію технік та підходів, які підвищать контроль над цією вибухонебезпечною емоцією. Вона буде цікава людям, які часто відчувають гнів та роздратування. Ви можете використовувати прочитане в будь-якій сфері, починаючи від спілкування з дітьми, закінчуючи роботою в організаціях.", false, false, new Guid("8a9f010f-457d-4c76-85f3-9ef59df2eb53"), 208, 295m, "Управление гневом" },
                    { new Guid("516ca249-5be1-4661-abdd-58aa7f1d22cc"), new Guid("04d6e5bf-ce01-4d24-a858-978abcd391a1"), "000028", "У книзі наведено огляд і ґенезу основних питань щодо походження і структури Всесвіту та законів і фізичних явищ, які підтримують його існування; щодо будови Землі й походження людини, особливості її психофізіології й уміння оперувати інтелектом. У книзі наведено термінологічно-понятійний апарат відповідних напрямків науки, а також віхи життя і діяльності найвизначніших діячів, які долучилися до становлення й розвитку цих наук. Для широкого загалу й усіх, хто цікавиться науковою думку і прагне розширити й поглибити свій кругозір.", false, false, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 160, 250m, "Теорії за 30 секунд" },
                    { new Guid("55b276db-ec5a-4832-8aa5-60b1c68eae0f"), new Guid("96f6510c-154b-4459-b9a1-194ea20526c7"), "000008", "Джейкоб Портман - підліток зі страшними снами - знаходить в архіві справи та фотографії дітей і вирішує з'ясувати, скільки правди в тих історіях, які він чув у дитинстві про монстрів та дітей з надздібностями. Відмінно побудована розповідь змушує читача підозрювати Джейкоба у шизофренії через візити до психотерапевта, який прагне пояснити те, що відбувається з раціонального погляду. Неординарний сюжет, детально прописані характери затягують читача в особливу атмосферу життя крипто сапієнсів — дітей, які вирізняються незвичайними, дивними можливостями. Реальність тут переплетена з фантазією, і ми вже не впевнені, чи був тунгуський метеорит, чи це витівки дивних дітей?", true, true, new Guid("8a9f010f-457d-4c76-85f3-9ef59df2eb53"), 432, 240m, "Дом странных детей" },
                    { new Guid("59cb50ed-8d0c-4c70-86d6-8c3730c6c434"), new Guid("64b80a39-72b3-49e5-8660-de4fb8e368e4"), "000027", "Жоден художник не був більш безжально керований своїм творчим поривом і не був більш ізольованим ним від звичайних джерел людського щастя, ніж Вінсент Ван Гог. Життя цього геніального постімпрессіоніста було безперервною боротьбою з бідністю, смутком, божевіллям і відчаєм. «Жага до життя» Ірвінга Стоуна вміло передає захоплюючу атмосферу Парижа постімпресіоністів і з глибоким розумінням реконструює розвиток творінь Ван Гога. Художник постає перед читачем не тільки як митець, а й як особистість, і ця розповідь про його насичене, яскраве і стражденне життя не залишить байдужим навіть тих, хто не є шанувальником творчості Ван Гога.", false, false, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 488, 250m, "Ван Гог. Жага до життя" },
                    { new Guid("630bfd2f-0691-46cb-9287-499a37610447"), new Guid("b46f0991-27e7-4826-99f7-a75941162c03"), "000047", "Закінчилося дитинство Гаррі, і разом з ним пішла наївність, казковість та уявна безпека з навколишнього світу. Де гра в квідич, перші уроки, мудрі та добрі вчителі? Хто тепер взагалі мудрий, хто добрий, і чи друзі залишаться поруч? Казковий світ перетворився на смерть, що крадеться. Але це самотність, в яку занурюється Гаррі на своєму останньому шляху – це самота більше, ніж смерть. Виявилося, занадто у багатьох є справжнє обличчя, яке до пори невидиме. Яке темне минуле приховував великий маг сучасності Дамблдор? Хто залишиться живим в останньому полюванні на хоркруксів і Волан-де-Морта?", false, false, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 640, 240m, "Гаррі Поттер i Смертельні реліквії" },
                    { new Guid("64d5b701-d3ad-457b-9f8f-eeb001a75906"), new Guid("ba043179-7347-4cd0-95bf-57124a484409"), "000002", "Незважаючи на художню вигадку, книга настільки реалістична, що перевертає свідомість, причому так, що хочеться перевернути її назад. Сам письменник стверджував, що «найкращі книги говорять тобі те, що ти вже сам знаєш». І роман-утопія «1984» описує речі, про які ми самі здогадувалися, але у існування яких боялися повірити. Основний конфлікт простий: особистість проти системи, тяга до любові й свободи проти поневолення та диктату. Але автор нещадний. Зображену ним систему неможливо перемогти, тому що не можна перемогти те, у чого немає ні серця, ні мозку, ні сенсу, ні цілей існування. Як такої немає й ідеології, де можна було б шукати вади: усе знищено укупі зі здоровим ґлуздом. Є лише влада заради влади. А людство надіслано на бійню, де гинуть не стільки тіла, скільки душі, оскільки розум, пам’ять, гуманізм та любов оголошені поза законом.", true, false, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 312, 150m, "1984" },
                    { new Guid("6563cdb6-1d94-48b5-b24f-2fb3f7459bfe"), new Guid("f7ab551c-7b22-4ca5-a116-1110c22bcb4f"), "000011", "Фредерик Клеґґ — нічим не примітний банківський клерк. Єдина його пристрасть — метелики. Впіймати, посадити під скло, зберегти їхню красу, щоб милуватися в будь-який момент... Але якось він знаходить екземпляр, набагато цікавіший за метеликів...", false, true, new Guid("ae8984c3-d127-47f1-be98-3491486060ac"), 368, 200m, "The Collector" },
                    { new Guid("6b0880ba-7add-4ec0-a560-348ad48590dc"), new Guid("b46f0991-27e7-4826-99f7-a75941162c03"), "000043", "Написана Джоан Роулінг книга «Гаррі Поттер і в'язень Азкабану» - третя книга про пригоди Хлопчика-зі-шрамом, яка підкорила цілий світ! Гаррі Поттер - незвичайний хлопчик. Він дуже не любить літні канікули, адже цей час він проводить в будинку своїх дядька й тітки Дурслі. А вони терпіти не можуть чарівників. Одного разу, втікши від ненависних родичів, Гаррі підбирає автобус «Нічний Лицар», який везе його прямісінько до рятівного магічного світу. Але тепер він не такий безпечний як раніше. Через дванадцять довгих років на свободу вибрався найнебезпечніший злочинець в'язниці Азкабан і спадкоємець самого Волан-де-Морта - Сіріус Блек! Кажуть, ніби це він зрадив Джеймса та Лілі Поттер, а тепер шукає самого Гаррі щоб закінчити почате Темним Лордом. У 3 книзі Гаррі Поттеру і його друзям, Рону і Герміоні, доведеться знову зіткнутися зі злими чарами і розкрити старі секрети.", false, false, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 384, 210m, "Гаррі Поттер і в'язень Азкабану" },
                    { new Guid("7018f385-d5d5-4586-b797-93a5586e56a9"), new Guid("0a697f2a-9c3c-47b6-8fb3-4cfc7066fd83"), "000036", "22 листопада 1963 року — дата вбивства Джона Кеннеді. Пролунали три постріли — і світ змінився назавжди. Сьогодення. Дізнавшись, що в барі його приятеля розташований портал до 1958 року, звичайний шкільний учитель Джейк Еппінг не може опиратися спокусі почати нове життя у рок-н-рольні часи Елвіса Преслі. Хіба гостю з майбутнього важко познайомитися з відлюдником Лі Гарві Освальдом і завадити йому вбити президента? Та чи варто гратися з минулим? Яким буде світ, де Кеннеді виживе?", false, false, new Guid("8a9f010f-457d-4c76-85f3-9ef59df2eb53"), 976, 280m, "11/22/63" },
                    { new Guid("7335982a-0762-43e1-9411-1a75ff1c577a"), new Guid("bd37bc3a-970e-4788-bcdf-957af81e3a46"), "000051", "Цілком очевидно, що життя — це сумарний ефект небагатьох важливих потрясінь. Згадайте своє життя. Перерахуйте важливі події й технологічні винаходи, що припали на ваш вік, і порівняйте з колись напрогнозованим. Скільки їх «приїхало за розкладом»? Подивіться на особисте життя, згадайте, як вибирали професію, зустріли свою половинку, емігрували, стик- нулися зі зрадою, несподівано розбагатіли або впали у злидні. Чи часто ці події відбувалися за планом? Малоймовірну подію, яка справляє колосальний ефект, колишній трейдер Насім Талеб влучно називав Чорним лебедем.", true, false, new Guid("8a9f010f-457d-4c76-85f3-9ef59df2eb53"), 736, 500m, "Черный лебедь. Под знаком непредсказуемости" },
                    { new Guid("7cedb874-c290-491e-9be7-c61eb8cb0dff"), new Guid("15069387-a9c3-43a2-ad25-8f7493b37410"), "000029", "Уже кілька десятиліть Річард Докінз блискуче пояснює наукові явища та дива природи, відстоюючи раціональний підхід та борючись із забобонами та помилками. У цій серії із 42 есе Докінз віддає належне складності світу природи. Він заглиблюється у древні часи, показуючи, наприклад, як історія гігантських та морських черепах може краще пояснити еволюцію. Річард Докінз - еволюційний біолог, відомий популяризатор науки, а також критик креаціонізму та теорії розумного задуму. До 2008 року обіймав посаду професора Оксфордського університету. Член Лондонського королівського товариства, що займається розвитком знань про природу.", false, false, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 384, 210m, "Наука для душі. Нотатки раціоналіста" },
                    { new Guid("81e399ec-ec52-4287-92b2-cfddef7c7832"), new Guid("8b0384f2-3950-4d3f-96ad-17a597154149"), "000040", "Сучасне суспільство пропагує культ успіху: будь розумнішим, багатшим, продуктивнішим - будь кращим за всіх. Соцмережі рясніють історіями на тему, як якийсь хлопець придумав додаток і заробив купу грошей, статтями на кшталт 'Тисяча і один спосіб бути щасливим', а фото у френдленті створюють враження, що оточуючі живуть краще та цікавіше, ніж ми. Проте наша зацикленість на позитиві та успіху лише нагадує про те, чого ми не досягли, про мрії, які не справдилися. Як стати по-справжньому щасливим?", true, false, new Guid("8a9f010f-457d-4c76-85f3-9ef59df2eb53"), 192, 230m, "Тонкое искусство пофигизма" },
                    { new Guid("8b04ff96-45b9-4a9e-bb2e-ca1eee1557e9"), new Guid("b46f0991-27e7-4826-99f7-a75941162c03"), "000045", "Пропонуємо вашій увазі п'яту книгу про Гаррі Поттера! Джоан Роулінг писала книгу «Гаррі Поттер і Орден Фенікса» протягом трьох років. Вона вийшла найдовшою у циклі і розкриває несподівані таємниці життя улюблених героїв. Гаррі щороку з нетерпінням чекає кінця літніх канікул і повернення до магічного світу. Але в цьому році магічний світ порушив спокій спекотного вечора Тисовий вулиці - на Дадлі та Гаррі нападають дементори, змусивши використати патронус. Тепер юного чарівника судитимуть в Міністерстві магії і, можливо, його чекає відрахування з Ґогвортсу. Ця частина про Хлопчика-який-вижив стала похмурішою і серйознішою. Головний герой зростає разом з читачами — тепер йому доводиться приймати дорослі рішення і миритися з важкими втратами.", false, false, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 814, 240m, "Гаррі Поттер i Орден Фенікса" },
                    { new Guid("8e52cab5-47ec-48e6-905f-91ee506ac22c"), new Guid("ad23527e-00e4-4ab1-8eb2-f1d371009037"), "000026", "У цій книжці зібрано документи з шеститомної кримінальної справи, яка зберігається на полицях колишнього архіву КДБ УРСР у Києві (нині — Галузевий державний архів СБУ). Як і за що було заарештовано поета? Хто під час процесу намагався врятувати Василя Стуса, а хто зробив усе, щоб він назавжди зник за ґратами політичної зони на Уралі? У книжці вміщено архівні документи зі справи Василя Стуса (протоколи обшуків, допитів, листи, статті і т. д.), фотографії, статті Вахтанга Кіпіані, а також відомих дисидентів — Василя Овсієнка, Леоніда Бородіна. Додано останні прижиттєві нотатки Стуса — «З таборового зошита», таємно передані з табору на волю.", false, false, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 688, 260m, "Справа Василя Стуса. Збірка документів з архіву колишнього КДБ УРСР" },
                    { new Guid("9fb43d5a-d5d7-4f82-8524-877e378709dc"), new Guid("cbdd691b-df84-4bb7-830f-e379440b8c36"), "000016", "Ніщо так не сприяє зростанню почуття власної гідності, як уміння кохати та бути коханим. Навіть якщо ви розлучені, або пережили смерть чоловіка, або взагалі ніколи не одружувалися, ваша потреба в коханні нікуди не зникає. З іншого боку, найбільший успіх у житті приходить до тих, хто дарує кохання іншим. Ця книга допоможе вам навчитися приймати та віддавати любов. У перших двох розділах йдеться про те, що ж являють собою безшлюбні люди і чому любов така важлива для побудови успішних взаємин. З 3-го по 7-му розділ ви ознайомитеся з п'ятьма мовами любові. У 8-му розділі ви навчитеся визначати свою основну мову любові та розпізнавати її в інших.", false, false, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 240, 150m, "Пять языков любви" },
                    { new Guid("a01c6032-6c3d-44c9-9eab-766b49e13199"), new Guid("b46f0991-27e7-4826-99f7-a75941162c03"), "000044", "Четверта книга про пригоди юного чарівника Гаррі Поттера. Нові заклинання, нові зілля, нові вчителі, нові предмети - Гаррі з нетерпінням чекає на початок навчального року. Крім того, у школі магії та чарівництва Хогвартс проходитиме турнір чарівників. У ньому братимуть участь учні 3-х шкіл. Але для когось цей турнір закінчиться трагічно. Волан-де-Морт повернеться знову і цього разу вже в тілі.", true, false, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 670, 210m, "Гаррі Поттер і келих вогню" },
                    { new Guid("c35d3f62-16a4-48a7-af9f-3a337fe703fe"), new Guid("003966d2-f5fd-45a2-9973-8f53f355b374"), "000052", "У світі, де сталь — відповідь на все, а люди моляться Матері Війні, важко знайти своє місце однорукому хлопчині. Ярві, молодший син короля Ґеттландії, мав стати служителем Батька Миру, але доля розпорядилася інакше. Батькова й братова смерть не лишають йому іншого вибору, як зійти на престол, якого він ніколи не прагнув. Однак втримати трон однією рукою нелегко. Ярві зазнáє зради, пройде крізь багато випробувань і знайде справжніх друзів, перш ніж стане тим, ким він є. Та чи не втратить він на своєму шляху до помсти чогось важливого?", true, false, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 296, 290m, "Пів Короля" },
                    { new Guid("c51700ae-e4f6-4e6f-83f3-6d960560c12c"), new Guid("97106cd6-8c27-4306-bf90-9d14bdd32f75"), "000031", "Ця книга – ваш особистий тренер. Щодня вона підніматиме бойовий дух і заряджатиме на успіх. Її автор, знаменита Джен Сінсеро, закликає не зменшувати обертів на шляху до успіху та щодня накачувати 'язи крутості' у Духовному тренажерному залі. Успіх - це спосіб існування, постійної адаптації та зростання.Чим більше ви робите для свого успіху, тим легше стає шлях', - лунає бойовий клич Сінсеро. Не тупи - це книга-тренінг із вправами та техніками для щоденної роботи над собою. З її допомогою ви підвищите рівень енергії і збережете усвідомленість і мотивацію навіть у найважчі часи.", false, false, new Guid("8a9f010f-457d-4c76-85f3-9ef59df2eb53"), 208, 75m, "НЕ ТУПИ. Только тот, кто ежедневно работает над собой, живет жизнью мечты" },
                    { new Guid("c6b657da-4561-4d04-a50a-5c578dc28d84"), new Guid("9d239583-1b8b-47e6-8102-556822f5eeab"), "000013", "Метод ненасильницького спілкування реально покращує життя тисяч людей. Він застосовується і в подружній спальні, і в класній кімнаті, і за круглим столом, і на лінії фронту. Корпорації, організації та уряди, які приймають метод ННО, швидко досягають значного прогресу у вирішенні своїх внутрішніх та зовнішніх проблем. За всієї своєї революційності метод ННО дуже простий, логічний і раціональний. Він ніяк не пов'язаний з релігійно-духовними навчаннями та психологічними школами та доступний абсолютно будь-якій людині.", true, true, new Guid("8a9f010f-457d-4c76-85f3-9ef59df2eb53"), 288, 168m, "Ненасильственное общение. Язык жизни" },
                    { new Guid("c7521805-6d35-4902-93b1-d9e843fe699e"), new Guid("a7f3f865-d45b-4234-8665-9e04e7f8660c"), "000019", "Бути веганом чи вегетаріанцем не означає їсти самі паростки бобів і насіння чіа. Але це обов’язково означає цінувати кожне життя. Зокрема своє. Якщо ви збилися з ніг, шукаючи швидкі, апетитні та стовідсотково здорові рецепти, то це ваша рятівна зупинка: як щодо рослинного молока, мікрозелені й суперфудів не за всі гроші світу? Під час створення цієї книжки жодна жива істота не постраждала. Тож можете насолоджуватися трапезою, любі вегани, лакто-, ово-, оволактовегетаріанці! Тут є все: від овочевих чипсів і горіхової пасти до фаршированих печериць і бананових мафінів — буде чим поласувати, переглядаючи фільм із вегетаріанцями в головних ролях. Готуйте, смакуйте, харчуйтеся корисно, розмаїто та з піклуванням про навколишнє середовище. Go vegan, do what you can — і хтозна, може, на Вегетаріанській Алеї Слави займеться нова зірка кіно, літератури, науки чи спорту!", false, false, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 256, 250m, "Вегетаріанська кухня. Смачно, етично, естетично" },
                    { new Guid("c81da85c-3dcf-43f8-a378-973bce0b1341"), new Guid("b90a3d56-4ca7-41a1-86fb-0dc3f124460c"), "000001", "Роман «Опісля» молодої американської письменниці Анни Тодд - це історія кохання Тесс і Гардіна, «хорошої» дівчинки і «поганого» хлопця, кохання пристрасного, відвертого і водночас надзвичайно ніжного й довірливого. Автор майстерно тримає читача у напрузі: чи будуть Тесс і Гардін після всіх перипетій разом, чи не зруйнують вони своє кохання вечірками, алкоголем, сексом та бійками? Чи збережуть тепло ­своїх стосунків? Молоді герої припали до смаку багатьом шанувальникам творчості Анни Тодд, яким подобається читати книжки про диво першої зустрічі, про іскру, що спалахує між чоловіком і жінкою, яка переростає у справжнє кохання.", false, false, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 624, 321m, "ОПісля" },
                    { new Guid("c9eb874e-cb85-49b5-96b1-19944d0b06a0"), new Guid("0f208bab-8a58-4d6f-85cd-476f25a6145e"), "000006", "П’ять кроків і в жодному разі не ближче. Інакше це може вбити Стеллу. Чому? Рідкісна хвороба, яка з дитинства не давала жити нормальним життям. Не наближайся, не варто. Незабаром на неї чекає операція, і нарешті все буде добре. Та все одно тримай дистанцію. Коли тобі хочеться обійняти її чи торкнутися волосся - не наближайся до неї, Вілле. Ти саме той, кого вона мусить уникати попри все. навіть один твій подих може зруйнувати все її життя. Якщо ти хочеш, щоб вона жила, — тримайся на відстані. Але я знаю, ти не можеш. Чому саме ти закохався в неї? Чому вона покохала саме тебе? Разом - не можна. Окремо - неможливо…", false, true, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 240, 116m, "За п'ять кроків до кохання" },
                    { new Guid("cadce17d-1091-4da4-aaf6-328d3de418d2"), new Guid("192a13dc-d53f-4b84-acad-951df2244351"), "000010", "У кожного в житті є люди, які мають для нас величезне значення. Це можуть бути батьки, друзі, кохані або навіть сусіди. Якщо однієї ночі така людина залізе до вас у вікно та попросить про допомогу, навряд ви їй відмовите. От і Квентин не зміг. І поїхав разом зі своєю сусідкою Марго, в яку закоханий із самого дитинства, щоб помститися її кривдникам. Як заїдеш у паперові міста, то вже не повернешся.Паперові міста Джона Гріна - це не просто оповідка з життя американських підлітків. Це книга про пошуки себе та свого місця в світі. Про те, чи треба миритися з ситуацією, яка тобі не подобається, чи спробувати її змінити. Про те, чи справді ми закохані в людину, чи створюємо собі її образ? І що взагалі реально? Минуле? Майбутнє? Картини, які малює твоя уява? Чи момент 'тут і тепер' в якому ти існуєш?", false, true, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 320, 220m, "Паперові міста" },
                    { new Guid("cc7a2bb3-b2a1-4dc4-9710-f84e73e27dc0"), new Guid("0ee07cb8-2b4f-49cd-ac3f-1bdd6f5a5bbc"), "000025", "Книжка Боріса Джонсона — це історія про те, як ексцентричний геній Черчилля, прем’єр-міністра Великої Британії, одного з найвизначніших лідерів ХХ століття, формував світову політику. Аналізуючи досягнення й помилки, а також міфи, які супроводжували цю видатну постать, Джонсон зобразив Черчилля як незрівнянного стратега, людину надзвичайно сміливу, красномовну й глибоко гуманну. Безстрашний на полі бою, один із найкращих ораторів усіх часів, лауреат Нобелівської премії з літератури, Вінстон Черчилль спростував думку, що історія — це місце дії великих знеособлених сил. Життя цього чоловіка є доказом того, що одна людина — відважна, геніальна, визначна — може впливати на світовий лад.", false, false, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 400, 230m, "Фактор Черчилля: Як одна людина змінила історію" },
                    { new Guid("d4151b28-0f7e-485f-b315-48d8133b7ba3"), new Guid("1e747a08-731f-4723-80da-01a0dc3dff26"), "000035", "Найкращий твір уславленого автора! Цей роман став культовим! Він захоплює з першої сторінки! В юні роки ми не боїмося мріяти, все здається можливим. Але з часами невідома сила починає переконувати нас, що мрії незбутні, а прагнення нездійсненні. Ця книжка навчить жити, не втрачаючи віру в кохання і добро, навчить цінувати кожний день, кожну годину, кожну мить, радіти, сумувати,  але ніколи не опускати руки.", true, false, new Guid("8a9f010f-457d-4c76-85f3-9ef59df2eb53"), 120, 155m, "Алхимик" }
                });

            migrationBuilder.InsertData(
                table: "Books",
                columns: new[] { "Id", "AuthorId", "Code", "Description", "IsBestseller", "IsNew", "LanguageId", "NumberOfPages", "Price", "Title" },
                values: new object[,]
                {
                    { new Guid("d9d5ed67-6f0e-4937-a11f-5cb08da82e1a"), new Guid("b46f0991-27e7-4826-99f7-a75941162c03"), "000042", "Друга частина пригод Гаррі Поттера від Джоан Роулінг — книга «Гаррі Поттер і таємна кімната»! На вас чекають нові пригоди у світі чарівників, де старі секрети, літаючі автомобілі, грізна верба, дивний щоденник та небезпечні улюбленці Хегріда оживають на сторінках книги! Юному чарівникові не пощастило з канікулами на Привіт-драйв у сім'ї Дурслів. Він потайки мріє повернутися до чарівного світу, проте від його друзів усе літо не було звістки і здається, що цілий світ проти його повернення... Цілий світ чи маленький ельф на ім'я Доббі, який хоче захистити його від смертельної небезпеки. Та цього хлопця не так просто налякати, особливо коли доводиться обирати між навчанням у чарівній школі та життям з родичами.", true, false, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 352, 210m, "Гаррі Поттер і таємна кімната" },
                    { new Guid("dff9d08f-a5eb-4b48-8ad0-e5514ad6ce88"), new Guid("38e393aa-2f14-41e4-9894-8d212f07400d"), "000041", "Чарлі Гордон — розумово відсталий, але у нього є друзі, робота, захоплення і непереборне бажання вчитися. Він погоджується взяти участь у небезпечному науковому експерименті, який може зробити його найрозумнішим... Яку ціну маємо ми платити за свої бажання? Чи варті вони того? Пронизлива історія, яка нікого не залишить байдужим! Мабуть, один з найлюдяніших творів у світовій літературі.", true, false, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 304, 190m, "Квіти для Елджернона" },
                    { new Guid("e2c8715a-2f25-49ba-806d-efc9f28d4bd9"), new Guid("3299e2da-00c6-4ac0-8d8e-06e9b6632203"), "000012", "Робота лікарів схожа на їхній химерний почерк — звичайний пацієнт нічого в цьому не втямить. Без допомоги спеціаліста, звісно. Такого, як Адам Кей — лікар-ординатор, який ділиться реальними історіями зі своєї багаторічної практики. Він знає все про будні звичайного медпрацівника, про те, що відбувається в кабінетах і палатах: daily routine британських лікарів та їхніх пацієнтів, складні діагнози і фатальні помилки, кумедні випадки та дивовижні зцілення, моральні дилеми і професійні хитрощі, понаднормові години праці та майже цілковиту відсутність особистого життя. Будні супергероїв у білих халатах, приправлені тонким англійським гумором та щирим співпереживанням. Ви будете здивовані тим, як багато спільного мають британські doctors та українські лікарі. Тож заходьте, вдягайте халат і приготуйтеся до того, що буде трішки боляче, смішно, сумно та загалом більш ніж просто захопливо.", false, true, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 256, 120m, "Буде боляче" },
                    { new Guid("ef5d4786-384a-49d8-bf97-9bcabe7482a6"), new Guid("b5ab23dd-4139-4212-bd3e-d9c155924fc0"), "000039", "Дитячі травми і образи не проходять безслідно. Вони проявляються в дорослому житті, і часто - самим несподіваним чином: не складаються стосунки, рушиться кар'єра, виникають хвороби... Проблеми ставлять нас у безвихідь, і ми навіть не підозрюємо, що усі відповіді і рішення - в нас самих. Відомий німецький психотерапевт і автор бестселерів Стефани Шталь стверджує: якщо ви подружитеся зі своїм ltamp;quot;внутрішнім ребенкомltamp;quot; подаруєте йому турботу і підтримку, яка так бракувало в дитинстві, на місце болю і бід прийдуть спокій, гармонія і легкість. Ефективні вправи і рекомендації, викладені в книзі, допоможуть вашому ltamp;quot;внутрішньому ребенкуltamp;quot; (тобто і вам!) стати нарешті щасливим.", false, false, new Guid("8a9f010f-457d-4c76-85f3-9ef59df2eb53"), 304, 320m, "Ребенок в тебе должен обрести дом" },
                    { new Guid("f6c5a42f-5c05-41b6-9c5a-f4f12ed60c6b"), new Guid("9ac72d18-80c2-44f9-9101-f50fb405f3ab"), "000007", "1950-ті роки. Бет Гармон — звичайна сирота із незвичайним хистом до шахів. Гра — її єдина пристрасть. Доки Бет у грі, доти контролює себе та... згубну жагу до транквілізаторів і алкоголю. Дівчина відчайдушно бажає стати феноменальною шахісткою, але водночас усе дужче хоче втекти подалі від цієї реальності. Наближається вирішальний шаховий двобій із чемпіоном світу, російським гросмейстером Василієм Борґовим. Одна хибна комбінація може зіпсувати всю гру, а неправильний хід — зламати майбутнє. Бет укотре втрачає контроль над собою. Але цього разу заради перемоги вона не шкодуватиме ні шахових фігур, ні живих людей.", false, true, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 352, 265m, "Хід Королеви" },
                    { new Guid("f8020fe9-51ab-43e9-8c5b-7f89926ba5cb"), new Guid("6345ac34-35f7-45e2-9dda-841894f9661f"), "000032", "24 лютого 2022 року, коли українські міста прокинулись від вибухів, ворог планував «блискавично» підкорити країну, вбиваючи мирних мешканців і знищуючи міста, але українці героїчно чинили опір. Кремль не зміг реалізувати план швидкого захоплення України та зміни влади в країні. Головною перепоною для окупантів стали єдність українців та стійкість ЗСУ разом з іншими силами оборони України, а ціною опору — тисячі жертв серед цивільного населення, зруйновані долі мільйонів українців через втрату близьких, дому, колишнього життя. Документальна хроніка повномасштабного вторгнення Росії в Україну нагадує читачу про трагічні і героїчні події першого місяця війни.  Ця книга — нагадування світовому суспільству про те, що російсько-українська війна триває, а українці зі зброєю в руках захищають благополуччя Європи.", false, true, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 383, 190m, "Місяць війни. Хроніка подій. Промови та звернення Президента України Володимира Зеленського" },
                    { new Guid("fa7b67d1-f00c-4d13-818d-23c1db93a2dc"), new Guid("50170e48-862f-45f2-9fba-60bfd74c5ae8"), "000018", "Єдина і неповторна книга про відносини чоловіка і жінки. Час не владний над нею, вона незмінно залишається актуальною та надзвичайно затребуваною. Секрет її феноменального успіху в тому, що вона дійсно допомагає чоловікам і жінкам зрозуміти та прийняти, наскільки по-різному вони мислять, відчувають та поводяться. Завдяки цьому мільйони читачів змогли не лише покращити, а й навіть урятувати свої стосунки.", true, false, new Guid("8a9f010f-457d-4c76-85f3-9ef59df2eb53"), 352, 105m, "Мужчины с Марса, женщины с Венеры" },
                    { new Guid("fec0e801-9a7a-41f8-baf5-67e1c4b540c5"), new Guid("bfa2d8d6-98cf-4154-b82b-4f6828f7a2c9"), "000014", "Що робить нас блискучими? Що робить нас смертельними? Що робить нас розумними? Юваль Ной Харарі кидає виклик всьому, що ми знаємо про людину. Вік Землі 4,5 мільярда років. Лише за частку цього часу один вид серед незліченної кількості інших підкорив його: ми. У цій сміливій та провокаційній книзі Юваль Ноа Харарі досліджує, хто ми, як ми сюди потрапили і куди йдемо. Ідеальний подарунок для допитливих читачів цього Різдва.", true, false, new Guid("ae8984c3-d127-47f1-be98-3491486060ac"), 512, 544m, "Sapiens. A Brief History of Humankind" },
                    { new Guid("ff0293d0-2109-4014-836d-d15e9cd5bc3a"), new Guid("b8b16048-4f4a-43ee-843b-025a4de23f7f"), "000048", "Джейн Остен (1775—1817) і досі по праву вважається «першою леді» англійської літератури. Її романи сповнені тонкого психологізму. «Гордість і упередженість» — це історія про кохання  заможного красеня-аристократа та скромної, але гордої доньки власника невеликого маєтку в провінції.", false, false, new Guid("ae8984c3-d127-47f1-be98-3491486060ac"), 287, 55m, "Pride and Prejudice" },
                    { new Guid("ff6f1b2a-b970-42d9-9924-143cf21cc2dc"), new Guid("254ac9fa-05b1-4ac5-9aec-9b36bba55167"), "000034", "Перед вами не звичайна розмальовка, не розмальовка-антистрес, не розмальовка для певного віку. Перед вами — екстремальна розмальовка Кербі Розанеса «Аніморфія»! Автор пропонує поринути у надзвичайний світ чудернацьких образів, фантазій, закликає дати волю своїй уяві та бажанням і за допомогою кольорових олівців вивільнити свою енергію на папері. Розмальовка зацікавить людину будь-якого віку. Адже головне, щоб ваш характер, ваш настрій співпадали з сюрреалістичними малюнками Кербі Розанеса.", false, false, new Guid("42c04fe1-3f6c-4c59-b01a-233d1c7d756d"), 96, 185m, "Аніморфія" }
                });

            migrationBuilder.InsertData(
                table: "Genres",
                columns: new[] { "Id", "CategoryId", "Name" },
                values: new object[,]
                {
                    { new Guid("1802fdd2-c264-473b-afb6-5ece9753d527"), new Guid("3c358b2b-0b33-4817-a26d-63339c0afce2"), "Науково-популярна література" },
                    { new Guid("3e040948-5dbf-4c4e-b77e-b992223ce869"), new Guid("3c358b2b-0b33-4817-a26d-63339c0afce2"), "Документальна література" },
                    { new Guid("48ebbfc6-bf96-4f39-b1ed-6c1d1aaeb55c"), new Guid("1aa747e3-b416-4c8c-a8db-c0f96ccbcd57"), "Драма" },
                    { new Guid("615f3a31-17d3-42d5-be5f-c466eae26a06"), new Guid("3c358b2b-0b33-4817-a26d-63339c0afce2"), "Псилохолія" },
                    { new Guid("632a9f47-5efb-45ac-b1f1-72e61eb4d40d"), new Guid("1aa747e3-b416-4c8c-a8db-c0f96ccbcd57"), "Детектив" },
                    { new Guid("699e5d59-067e-4d2a-8f31-c40b80dd63c8"), new Guid("1aa747e3-b416-4c8c-a8db-c0f96ccbcd57"), "Пригоди" },
                    { new Guid("70276efb-6e62-4303-bda8-23601d6c3824"), new Guid("3c358b2b-0b33-4817-a26d-63339c0afce2"), "Саморозвиток" },
                    { new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1"), new Guid("1aa747e3-b416-4c8c-a8db-c0f96ccbcd57"), "Роман" },
                    { new Guid("aba782f8-1257-4c0d-a2a1-a633397a7655"), new Guid("3c358b2b-0b33-4817-a26d-63339c0afce2"), "Кулінарія" },
                    { new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5"), new Guid("1aa747e3-b416-4c8c-a8db-c0f96ccbcd57"), "Фантастика" },
                    { new Guid("c51ef25c-3e3b-4a1d-9ed0-e9fef963242e"), new Guid("3c358b2b-0b33-4817-a26d-63339c0afce2"), "Політика" },
                    { new Guid("fbeff58e-b5ea-40bc-b987-b64f65730135"), new Guid("3c358b2b-0b33-4817-a26d-63339c0afce2"), "Філософія" }
                });

            migrationBuilder.InsertData(
                table: "BookGenres",
                columns: new[] { "BookId", "GenreId" },
                values: new object[,]
                {
                    { new Guid("0b2cceb2-629d-46db-9578-b3d2b96e92cd"), new Guid("1802fdd2-c264-473b-afb6-5ece9753d527") },
                    { new Guid("0e9ce81e-0296-4c2b-9302-255a24ef999f"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") },
                    { new Guid("1365032e-6311-4f78-8118-bd441aaccee2"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") },
                    { new Guid("1365032e-6311-4f78-8118-bd441aaccee2"), new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5") },
                    { new Guid("13e2ae3c-09b5-46c3-868d-a0cb8e30801e"), new Guid("632a9f47-5efb-45ac-b1f1-72e61eb4d40d") },
                    { new Guid("13e2ae3c-09b5-46c3-868d-a0cb8e30801e"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") },
                    { new Guid("17c9218e-04b7-47fc-969e-53872d8ac017"), new Guid("615f3a31-17d3-42d5-be5f-c466eae26a06") },
                    { new Guid("1c2c89ce-86c8-4751-8917-247052fab5d7"), new Guid("aba782f8-1257-4c0d-a2a1-a633397a7655") },
                    { new Guid("1fd7bd85-befa-4d7d-9ff1-671eee40ff13"), new Guid("1802fdd2-c264-473b-afb6-5ece9753d527") },
                    { new Guid("2341255d-748a-4e7f-a0de-886c9dadabdd"), new Guid("48ebbfc6-bf96-4f39-b1ed-6c1d1aaeb55c") },
                    { new Guid("2341255d-748a-4e7f-a0de-886c9dadabdd"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") },
                    { new Guid("2341255d-748a-4e7f-a0de-886c9dadabdd"), new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5") },
                    { new Guid("23f52275-0d2d-4363-9e8b-ce64a7eb7a7f"), new Guid("632a9f47-5efb-45ac-b1f1-72e61eb4d40d") },
                    { new Guid("23f52275-0d2d-4363-9e8b-ce64a7eb7a7f"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") },
                    { new Guid("2eab26b3-7493-4192-879e-fe499b121646"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") },
                    { new Guid("2eab26b3-7493-4192-879e-fe499b121646"), new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5") },
                    { new Guid("329ff545-1784-4dc0-b9ee-15bff62ae53d"), new Guid("aba782f8-1257-4c0d-a2a1-a633397a7655") },
                    { new Guid("33e093c6-0d8c-4917-a6ab-aa4e63745c91"), new Guid("632a9f47-5efb-45ac-b1f1-72e61eb4d40d") },
                    { new Guid("33e093c6-0d8c-4917-a6ab-aa4e63745c91"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") },
                    { new Guid("33e093c6-0d8c-4917-a6ab-aa4e63745c91"), new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5") },
                    { new Guid("3850c786-2726-4166-90f8-f4ba9d05836e"), new Guid("70276efb-6e62-4303-bda8-23601d6c3824") },
                    { new Guid("3b263589-953d-430d-a22d-26d51fa77c05"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") },
                    { new Guid("3b263589-953d-430d-a22d-26d51fa77c05"), new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5") },
                    { new Guid("3d918f1a-d700-4c81-a5fa-06b66e893658"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") },
                    { new Guid("4183bc98-b2ca-471e-9d31-246ebf86f28f"), new Guid("aba782f8-1257-4c0d-a2a1-a633397a7655") },
                    { new Guid("45ee2451-3709-4c53-9991-f01d240687eb"), new Guid("1802fdd2-c264-473b-afb6-5ece9753d527") },
                    { new Guid("4a635251-3a99-429d-99a0-5f0d0e7dea25"), new Guid("615f3a31-17d3-42d5-be5f-c466eae26a06") },
                    { new Guid("516ca249-5be1-4661-abdd-58aa7f1d22cc"), new Guid("1802fdd2-c264-473b-afb6-5ece9753d527") },
                    { new Guid("55b276db-ec5a-4832-8aa5-60b1c68eae0f"), new Guid("632a9f47-5efb-45ac-b1f1-72e61eb4d40d") },
                    { new Guid("55b276db-ec5a-4832-8aa5-60b1c68eae0f"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") },
                    { new Guid("55b276db-ec5a-4832-8aa5-60b1c68eae0f"), new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5") },
                    { new Guid("59cb50ed-8d0c-4c70-86d6-8c3730c6c434"), new Guid("3e040948-5dbf-4c4e-b77e-b992223ce869") },
                    { new Guid("630bfd2f-0691-46cb-9287-499a37610447"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") },
                    { new Guid("630bfd2f-0691-46cb-9287-499a37610447"), new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5") },
                    { new Guid("64d5b701-d3ad-457b-9f8f-eeb001a75906"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") },
                    { new Guid("64d5b701-d3ad-457b-9f8f-eeb001a75906"), new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5") },
                    { new Guid("6563cdb6-1d94-48b5-b24f-2fb3f7459bfe"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") },
                    { new Guid("6b0880ba-7add-4ec0-a560-348ad48590dc"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") },
                    { new Guid("6b0880ba-7add-4ec0-a560-348ad48590dc"), new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5") },
                    { new Guid("7018f385-d5d5-4586-b797-93a5586e56a9"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") },
                    { new Guid("7018f385-d5d5-4586-b797-93a5586e56a9"), new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5") },
                    { new Guid("7335982a-0762-43e1-9411-1a75ff1c577a"), new Guid("fbeff58e-b5ea-40bc-b987-b64f65730135") }
                });

            migrationBuilder.InsertData(
                table: "BookGenres",
                columns: new[] { "BookId", "GenreId" },
                values: new object[,]
                {
                    { new Guid("7cedb874-c290-491e-9be7-c61eb8cb0dff"), new Guid("1802fdd2-c264-473b-afb6-5ece9753d527") },
                    { new Guid("81e399ec-ec52-4287-92b2-cfddef7c7832"), new Guid("70276efb-6e62-4303-bda8-23601d6c3824") },
                    { new Guid("8b04ff96-45b9-4a9e-bb2e-ca1eee1557e9"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") },
                    { new Guid("8b04ff96-45b9-4a9e-bb2e-ca1eee1557e9"), new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5") },
                    { new Guid("8e52cab5-47ec-48e6-905f-91ee506ac22c"), new Guid("3e040948-5dbf-4c4e-b77e-b992223ce869") },
                    { new Guid("9fb43d5a-d5d7-4f82-8524-877e378709dc"), new Guid("615f3a31-17d3-42d5-be5f-c466eae26a06") },
                    { new Guid("9fb43d5a-d5d7-4f82-8524-877e378709dc"), new Guid("70276efb-6e62-4303-bda8-23601d6c3824") },
                    { new Guid("a01c6032-6c3d-44c9-9eab-766b49e13199"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") },
                    { new Guid("a01c6032-6c3d-44c9-9eab-766b49e13199"), new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5") },
                    { new Guid("c35d3f62-16a4-48a7-af9f-3a337fe703fe"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") },
                    { new Guid("c35d3f62-16a4-48a7-af9f-3a337fe703fe"), new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5") },
                    { new Guid("c51700ae-e4f6-4e6f-83f3-6d960560c12c"), new Guid("70276efb-6e62-4303-bda8-23601d6c3824") },
                    { new Guid("c6b657da-4561-4d04-a50a-5c578dc28d84"), new Guid("615f3a31-17d3-42d5-be5f-c466eae26a06") },
                    { new Guid("c7521805-6d35-4902-93b1-d9e843fe699e"), new Guid("aba782f8-1257-4c0d-a2a1-a633397a7655") },
                    { new Guid("c81da85c-3dcf-43f8-a378-973bce0b1341"), new Guid("48ebbfc6-bf96-4f39-b1ed-6c1d1aaeb55c") },
                    { new Guid("c81da85c-3dcf-43f8-a378-973bce0b1341"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") },
                    { new Guid("c9eb874e-cb85-49b5-96b1-19944d0b06a0"), new Guid("48ebbfc6-bf96-4f39-b1ed-6c1d1aaeb55c") },
                    { new Guid("c9eb874e-cb85-49b5-96b1-19944d0b06a0"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") },
                    { new Guid("cadce17d-1091-4da4-aaf6-328d3de418d2"), new Guid("48ebbfc6-bf96-4f39-b1ed-6c1d1aaeb55c") },
                    { new Guid("cadce17d-1091-4da4-aaf6-328d3de418d2"), new Guid("632a9f47-5efb-45ac-b1f1-72e61eb4d40d") },
                    { new Guid("cadce17d-1091-4da4-aaf6-328d3de418d2"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") },
                    { new Guid("cc7a2bb3-b2a1-4dc4-9710-f84e73e27dc0"), new Guid("3e040948-5dbf-4c4e-b77e-b992223ce869") },
                    { new Guid("d4151b28-0f7e-485f-b315-48d8133b7ba3"), new Guid("48ebbfc6-bf96-4f39-b1ed-6c1d1aaeb55c") },
                    { new Guid("d4151b28-0f7e-485f-b315-48d8133b7ba3"), new Guid("699e5d59-067e-4d2a-8f31-c40b80dd63c8") },
                    { new Guid("d4151b28-0f7e-485f-b315-48d8133b7ba3"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") },
                    { new Guid("d9d5ed67-6f0e-4937-a11f-5cb08da82e1a"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") },
                    { new Guid("d9d5ed67-6f0e-4937-a11f-5cb08da82e1a"), new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5") },
                    { new Guid("dff9d08f-a5eb-4b48-8ad0-e5514ad6ce88"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") },
                    { new Guid("dff9d08f-a5eb-4b48-8ad0-e5514ad6ce88"), new Guid("af857099-0ea3-4087-a589-6e12f5aa0aa5") },
                    { new Guid("e2c8715a-2f25-49ba-806d-efc9f28d4bd9"), new Guid("3e040948-5dbf-4c4e-b77e-b992223ce869") },
                    { new Guid("ef5d4786-384a-49d8-bf97-9bcabe7482a6"), new Guid("70276efb-6e62-4303-bda8-23601d6c3824") },
                    { new Guid("f6c5a42f-5c05-41b6-9c5a-f4f12ed60c6b"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") },
                    { new Guid("f8020fe9-51ab-43e9-8c5b-7f89926ba5cb"), new Guid("3e040948-5dbf-4c4e-b77e-b992223ce869") },
                    { new Guid("fa7b67d1-f00c-4d13-818d-23c1db93a2dc"), new Guid("615f3a31-17d3-42d5-be5f-c466eae26a06") },
                    { new Guid("fa7b67d1-f00c-4d13-818d-23c1db93a2dc"), new Guid("70276efb-6e62-4303-bda8-23601d6c3824") },
                    { new Guid("fec0e801-9a7a-41f8-baf5-67e1c4b540c5"), new Guid("1802fdd2-c264-473b-afb6-5ece9753d527") },
                    { new Guid("ff0293d0-2109-4014-836d-d15e9cd5bc3a"), new Guid("48ebbfc6-bf96-4f39-b1ed-6c1d1aaeb55c") },
                    { new Guid("ff0293d0-2109-4014-836d-d15e9cd5bc3a"), new Guid("8f554a5e-13cf-447c-a090-8e57cc2de3a1") },
                    { new Guid("ff6f1b2a-b970-42d9-9924-143cf21cc2dc"), new Guid("615f3a31-17d3-42d5-be5f-c466eae26a06") }
                });

            migrationBuilder.InsertData(
                table: "OrderDetails",
                columns: new[] { "BookId", "OrderId", "Price", "Quantity" },
                values: new object[,]
                {
                    { new Guid("13e2ae3c-09b5-46c3-868d-a0cb8e30801e"), new Guid("438de794-1ede-42fb-8d50-5f0e98939767"), 240m, (short)1 },
                    { new Guid("c35d3f62-16a4-48a7-af9f-3a337fe703fe"), new Guid("e1f06da2-5eda-4668-887c-6f87bc92653e"), 870m, (short)3 },
                    { new Guid("e2c8715a-2f25-49ba-806d-efc9f28d4bd9"), new Guid("438de794-1ede-42fb-8d50-5f0e98939767"), 120m, (short)1 }
                });
        }
    }
}

﻿using YourBookStore.DAL.Interfaces;
using YourBookStore.Domain.DTOs;
using YourBookStore.Domain.Entities;

namespace YourBookStore.DAL.Infrastructure.Filters.BookFilters;

public class BookSortFilter : IFilter<IQueryable<Book>, BookFilterDTO>
{
    public IQueryable<Book> DoFilter(IQueryable<Book> books, BookFilterDTO filter)
    {
        if (filter is null)
        {
            return books;
        }

        books = filter.FilterStatus switch
        {
            ("PriceASC") => books.OrderBy(t => t.Price),
            ("PriceDESC") => books.OrderByDescending(t => t.Price),
            ("None") => books,
            ("New") => books.Where(t => t.IsNew == true),
            ("Bestsellers") => books.Where(t => t.IsBestseller == true),
            _ => throw new NotImplementedException("Incorrect sort data")
        };

        return books;
    }
}
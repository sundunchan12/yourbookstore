﻿using Castle.Core.Internal;
using YourBookStore.DAL.Interfaces;
using YourBookStore.Domain.DTOs;
using YourBookStore.Domain.Entities;

namespace YourBookStore.DAL.Infrastructure.Filters.BookFilters;

public class BookLanguageFilter : IFilter<IQueryable<Book>, BookFilterDTO>
{
    public IQueryable<Book> DoFilter(IQueryable<Book> books, BookFilterDTO filter)
    {
        if (filter is null)
        {
            return books;
        }

        if (!filter.Languages.IsNullOrEmpty())
        {
            books = books.Where(b => filter.Languages.Contains(b.Language.Name));
        }
        
        return books;
    }
    
}
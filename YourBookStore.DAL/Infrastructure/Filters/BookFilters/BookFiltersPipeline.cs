﻿using YourBookStore.DAL.Interfaces;
using YourBookStore.Domain.DTOs;
using YourBookStore.Domain.Entities;

namespace YourBookStore.DAL.Infrastructure.Filters.BookFilters;

public class BookFiltersPipeline : FiltersPipeline<IQueryable<Book>, BookFilterDTO>
{
    public override IQueryable<Book> Process(IQueryable<Book> input, BookFilterDTO filterParams)
    {
        foreach (var filter in filters)
        {
            input = filter.DoFilter(input, filterParams);
        }

        return input;
    }

    
}
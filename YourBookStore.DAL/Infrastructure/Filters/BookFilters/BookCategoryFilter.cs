﻿using Castle.Core.Internal;
using YourBookStore.DAL.Interfaces;
using YourBookStore.Domain.DTOs;
using YourBookStore.Domain.Entities;

namespace YourBookStore.DAL.Infrastructure.Filters.BookFilters;

public class BookCategoryFilter : IFilter<IQueryable<Book>, BookFilterDTO>
{
    public IQueryable<Book> DoFilter(IQueryable<Book> books, BookFilterDTO filter)
    {
        if (filter is null)
        {
            return books;
        }

        if (!filter.Categories.IsNullOrEmpty())
        {
           
            books = books.Where(b => b.BookGenres.Any(bg => filter.Categories.Contains(bg.Genre.Category.Name)));
        }
        
        return books;
    }
}
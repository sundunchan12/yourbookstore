﻿using Castle.Core.Internal;
using YourBookStore.DAL.Interfaces;
using YourBookStore.Domain.DTOs;
using YourBookStore.Domain.Entities;

namespace YourBookStore.DAL.Infrastructure.Filters.BookFilters;

public class BookAuthorFilter : IFilter<IQueryable<Book>, BookFilterDTO>
{
    public IQueryable<Book> DoFilter(IQueryable<Book> books, BookFilterDTO filter)
    {
        if (filter is null)
        {
            return books;
        }

        if (!filter.Authors.IsNullOrEmpty())
        {
            books = books.Where(t => filter.Authors.Contains(t.Author.FullName));
        }

        return books;
    }
}
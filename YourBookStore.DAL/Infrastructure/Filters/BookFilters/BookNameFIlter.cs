﻿using YourBookStore.DAL.Interfaces;
using YourBookStore.Domain.DTOs;
using YourBookStore.Domain.Entities;

namespace YourBookStore.DAL.Infrastructure.Filters.BookFilters;

public class BookNameFilter : IFilter<IQueryable<Book>, BookFilterDTO>
{
    public IQueryable<Book> DoFilter(IQueryable<Book> books, BookFilterDTO filter)
    {
        if (filter is null)
        {
            return books;
        }

        if (!string.IsNullOrEmpty(filter.SearchName))
        {
            books = books.Where(t => t.Title.Contains(filter.SearchName));
        }

        return books;
    }
}
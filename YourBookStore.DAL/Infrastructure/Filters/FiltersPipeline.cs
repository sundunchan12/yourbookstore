﻿using YourBookStore.DAL.Interfaces;

namespace YourBookStore.DAL.Infrastructure.Filters;

public abstract class FiltersPipeline<T, F>
{
    protected readonly List<IFilter<T, F>> filters = new List<IFilter<T, F>>();

    public FiltersPipeline<T, F> Register(IFilter<T, F> filter)
    {
        filters.Add(filter);
        return this;
    }

    public abstract T Process(T input, F filter);
}

﻿using YourBookStore.DAL.Infrastructure.Filters;
using YourBookStore.DAL.Interfaces;
using YourBookStore.DAL.Interfaces.RepositoryInterfaces;
using YourBookStore.Domain.DTOs;
using YourBookStore.Domain.Entities;

namespace YourBookStore.DAL.Infrastructure.Extensions;

public static class FilterExtensions
{
    public static void DefaultPipelineConfig<T, F>(this FiltersPipeline<T, F> pipeline, IFilter<T, F>[] filters)
    {
        for(int i = 0; i < filters.Length; i++)
        {
            if (i >= filters.Length)
            {
                break;
            }

            pipeline.Register(filters[i]);
        }
    }

    
}
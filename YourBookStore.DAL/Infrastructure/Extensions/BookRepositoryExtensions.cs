﻿using YourBookStore.DAL.Infrastructure.Filters.BookFilters;
using YourBookStore.DAL.Interfaces;
using YourBookStore.DAL.Interfaces.RepositoryInterfaces;
using YourBookStore.Domain.DTOs;
using YourBookStore.Domain.Entities;

namespace YourBookStore.DAL.Infrastructure.Extensions;

public static class BookRepositoryExtensions
{
    public static async Task<IEnumerable<Book>> GetPaginationBookList(this IGenericEFRepository<Book> bookRepository, IFilter<IQueryable<Book>, BookFilterDTO>[] filters, BookFiltersPipeline pipeline, BookFilterDTO filterPoints, int pageSize = 12, int pageNumber = 1)
    {
        pageNumber = pageNumber > 0 ? pageNumber - 1 : 0;
        return await Task.FromResult(pipeline
            .Process(bookRepository.GetAll(), filterPoints)
            .Skip(pageSize * pageNumber)
            .Take(pageSize).ToList());
    }

    public static async Task<IQueryable<Book>> GetFilterBookList(this IGenericEFRepository<Book> bookRepository, IFilter<IQueryable<Book>, BookFilterDTO>[] filters, BookFiltersPipeline pipeline, BookFilterDTO filterPoints)
    {
        return await Task.FromResult(pipeline
            .Process(bookRepository.GetAll(), filterPoints));
    }

    public static async Task<int> GetFilterBookCount(this IGenericEFRepository<Book> bookRepository, IFilter<IQueryable<Book>, BookFilterDTO>[] filters, BookFiltersPipeline pipeline, BookFilterDTO filterPoints)
    {
        return await Task.FromResult(pipeline
            .Process(bookRepository.GetAll(), filterPoints).ToList().Count);
    }
}
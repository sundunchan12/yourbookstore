﻿using System.ComponentModel;

namespace YourBookStore.Utility.GlobalEnums;

public enum OrderStatus
{
    [Description("Оплачено")]
    Paid,
    [Description("Доставлено")]
    Shipped,
    [Description("Очікування підтверждення")]
    ConfirmationPending
}
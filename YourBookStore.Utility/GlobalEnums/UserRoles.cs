﻿namespace YourBookStore.Utility.GlobalEnums;

public enum UserRoles
{
    Administrator,
    Manager,
    Moderator,
    User
}
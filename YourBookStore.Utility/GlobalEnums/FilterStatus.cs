﻿namespace YourBookStore.Utility.GlobalEnums;

public enum FilterStatus
{
    Bestsellers,
    PriceASC,
    PriceDESC,
    New
}
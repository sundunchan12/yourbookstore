﻿namespace YourBookStore.Utility.GlobalConstants;

public static class ExceptionMessages
{
    public const string EntityNullMessage = "Entity should not be null";
    public const string EntityIdNullMessage = "Id of entity should not be null";
    public const string BookNullMessage = "Book should not be null";
    public const string CodeNullMessage = "Code should not be null";
    public const string OrderNullMessage = "Order should not be null";
    public const string AuthorNullMessage = "Author should not be null";
    public const string UserNullMessage = "User should not be null";
    public const string UserIsExists = "User with current userName has been created";
    public const string IncorrectPasswordHash = "Incorrect password hash";

    public static string GetShouldNotBeMessage(string data) =>
        data is null ? "Data should not be null" : data + " should not be null";

}
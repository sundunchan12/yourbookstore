﻿using AutoMapper;
using YourBookStore.Domain.DTOs;
using YourBookStore.Domain.Entities;

namespace YourBookStore.Utility.Mapper;

public class CategoryMappingProfile : Profile
{
    public CategoryMappingProfile()
    {
        CreateMap<Category, CategoryDTO>()
            .ForMember(c => c.GenreNames, map => map.MapFrom(category => category.Genres.Select(g => g.Name)));                                
        CreateMap<CategoryDTO, Category>();
    }
}
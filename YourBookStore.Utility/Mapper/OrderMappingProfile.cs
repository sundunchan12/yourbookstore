﻿using AutoMapper;
using YourBookStore.Domain.DTOs;
using YourBookStore.Domain.Entities;

namespace YourBookStore.Utility.Mapper;

public class OrderMappingProfile : Profile
{
    public OrderMappingProfile()
    {
        CreateMap<Order, OrderDTO>().ReverseMap();
        CreateMap<OrderDetails, OrderDetailsDTO>().ReverseMap();
    }
}
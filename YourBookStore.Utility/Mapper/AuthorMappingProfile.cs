﻿using AutoMapper;
using YourBookStore.Domain.DTOs;
using YourBookStore.Domain.Entities;

namespace YourBookStore.Utility.Mapper;

public class AuthorMappingProfile : Profile
{
    public AuthorMappingProfile()
    {
        CreateMap<Author, AuthorDTO>().ReverseMap();
        CreateMap<AuthorDTO, String>().ConvertUsing(r => r.FullName);
        CreateMap<BookLanguageDTO, String>().ConvertUsing(r => r.Name);
    }
}
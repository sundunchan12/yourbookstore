﻿using AutoMapper;
using YourBookStore.Domain.DTOs;
using YourBookStore.Domain.Entities;

namespace YourBookStore.Utility.Mapper;

public class UserMappingProfile : Profile
{
    public UserMappingProfile()
    {
        CreateMap<User, UserDTO>().ReverseMap();
    }

}
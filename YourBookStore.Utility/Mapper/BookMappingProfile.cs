﻿using AutoMapper;
using YourBookStore.Domain.DTOs;
using YourBookStore.Domain.Entities;

namespace YourBookStore.Utility.Mapper;

public class BookMappingProfile : Profile
{
    public BookMappingProfile()
    {
        CreateMap<Book, BookDTO>()
            .ForMember(book => book.GenreNames, map => map.MapFrom(t => t.BookGenres.Select(o => o.Genre.Name)));
            /*.ForMember(book => book.Author, map => map.MapFrom(t => t.Author.FullName))
            .ForMember(book => book.Language, map => map.MapFrom(t => t.Language.Name));*/
        
       
    }
}
﻿using AutoMapper;
using YourBookStore.Domain.DTOs;
using YourBookStore.Domain.Entities;

namespace YourBookStore.Utility.Mapper;

public class GenreMappingProfile : Profile
{
    public GenreMappingProfile()
    {
        CreateMap<Genre, GenreDTO>().ReverseMap();
    }
}
﻿using System.Runtime.Serialization;

namespace YourBookStore.BLL.Validations;

[Serializable]
public class YourBookStoreException : Exception
{
    public YourBookStoreException(string message) : base(message)
    { }

    public YourBookStoreException()
    { }

    protected YourBookStoreException(SerializationInfo info, StreamingContext context) : base(info, context)
    { }

    protected YourBookStoreException(string message, Exception innerException) : base(message, innerException)
    { }
}
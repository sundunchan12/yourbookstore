﻿using System;
using System.Collections.Generic;
using System.Linq;

using YourBookStore.BLL.Infrastructure.Extensions;
using System.Text.Json;
using System.Threading.Tasks;
using Castle.Core.Internal;
using Microsoft.AspNetCore.Http;
using YourBookStore.BLL.Interfaces;
using YourBookStore.BLL.Validations;
using YourBookStore.DAL.Interfaces.RepositoryInterfaces;
using YourBookStore.Domain.DTOs;
using YourBookStore.Domain.Entities;
using YourBookStore.Utility.GlobalConstants;

namespace YourBookStore.BLL.Services;

 public class BasketService : IBasketService
    {
        private readonly IGenericEFRepository<Book> _bookRepository;
        private readonly IGenericEFRepository<User> _userRepository;
        
        public BasketService(IGenericEFRepository<Book> bookRepository, IGenericEFRepository<User> userRepository)
        {
            _userRepository = userRepository;
            _bookRepository = bookRepository;
        }

        public async Task<bool> AddOrderToBasketAsync(OrderDetailsDTO orderDetails, ISession session, string email)
        {
            var order = session.Get<OrderDTO>("basket");
            
            
            var item = await Task.FromResult(_bookRepository.GetAllAsNoTracking().FirstOrDefault(t=>t.Code == orderDetails.Code) ??
                _bookRepository.GetAllAsNoTracking().FirstOrDefault(t => t.Code == orderDetails.Code));

            if (item is null)
            {
                throw new YourBookStoreException(ExceptionMessages.EntityNullMessage);
            }

            Guid customerId;
            if(!email.IsNullOrEmpty())
            {
                var user = _userRepository.GetAllAsNoTracking().FirstOrDefault(t => t.Email == email);
                if (user == null)
                {
                    customerId = Guid.NewGuid();
                }
                else
                {
                    customerId = user.Id;
                }
            }
            else
            {
                customerId = Guid.NewGuid();
            }

            if (order is null)
            {
                session.Set("basket", new OrderDTO
                {
                    Id = Guid.NewGuid(),
                    OrderDate = DateTime.Now,
                    CustomerId = customerId,
                    OrderDetails = new List<OrderDetailsDTO> { orderDetails},
                    Status = "Оплачено"
                });
                
            }
            else
            {
                var currentOrderDetails = order.OrderDetails.FirstOrDefault(t => t.BookId == orderDetails.BookId);

                if (currentOrderDetails != null)
                {
                    currentOrderDetails.Quantity++;
                }
                else
                {
                    order.OrderDetails.Add(orderDetails);
                }

                session.Set("basket", order);
            }

            return true;
        }
    }
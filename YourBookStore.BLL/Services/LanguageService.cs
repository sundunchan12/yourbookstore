﻿using AutoMapper;
using YourBookStore.BLL.Interfaces;
using YourBookStore.DAL.Interfaces.RepositoryInterfaces;
using YourBookStore.Domain.Entities;

namespace YourBookStore.BLL.Services;

public class LanguageService : ILanguageService
{
    private readonly IGenericEFRepository<Language> _languageEfRepository;
    private readonly IMapper _mapper;

    public LanguageService( IGenericEFRepository<Language> languageEfRepository, IMapper mapper)
    {
        _languageEfRepository = languageEfRepository;
        _mapper = mapper;
    }

    public IEnumerable<string> GetAllLanguagesNames() => _languageEfRepository.GetAllAsNoTracking().Select(t => t.Name);

}
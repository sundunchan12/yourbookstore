﻿using System.Security.Claims;
using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using YourBookStore.BLL.Infrastructure.Extensions;
using YourBookStore.BLL.Interfaces;
using YourBookStore.BLL.Validations;
using YourBookStore.DAL.Interfaces;
using YourBookStore.DAL.Interfaces.RepositoryInterfaces;
using YourBookStore.Domain.DTOs;
using YourBookStore.Domain.Entities;
using YourBookStore.Utility.GlobalConstants;
using YourBookStore.Utility.GlobalEnums;

namespace YourBookStore.BLL.Services;

public class UserService : IUserService
{
    private readonly IMapper _mapper;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IPasswordHelper _passwordHelper;
    private readonly IGenericEFRepository<User> _userRepository;

    public UserService(IPasswordHelper passwordHelper, IGenericEFRepository<User> userRepository, IMapper mapper, IUnitOfWork unitOfWork)
    {
        _mapper = mapper;
        _unitOfWork = unitOfWork;
        _userRepository = userRepository;
        _passwordHelper = passwordHelper;
    }

    public async Task SignInAsync(UserDTO user, HttpContext context)
    {
        if (user is null)
        {
            throw new YourBookStoreException(ExceptionMessages.UserNullMessage);
        }

        var currentUser = _userRepository.GetAllAsNoTracking().FirstOrDefault(t => t.Email == user.Email);

        if (currentUser is null)
        {
            throw new YourBookStoreException(ExceptionMessages.UserNullMessage);
        }

        if (!_passwordHelper.VerifyPassword(user.Password, currentUser.StoredSalt, currentUser.Password))
        {
            throw new YourBookStoreException(ExceptionMessages.IncorrectPasswordHash);
        }
        
        var claims = new List<Claim>
        {
            new Claim("Name", currentUser.Email),
            new Claim("Id", currentUser.Id.ToString()),
            new Claim("Role", currentUser.Role.ToString()),
            
        };
        var claimsIdentity = new ClaimsIdentity(
            claims,
            CookieAuthenticationDefaults.AuthenticationScheme);

        await context.SignInAsync(
            CookieAuthenticationDefaults.AuthenticationScheme,
            new ClaimsPrincipal(claimsIdentity));
        
    }

    public async Task SignOutAsync(HttpContext context)
    {
        await context.SignOutAsync(
            CookieAuthenticationDefaults.AuthenticationScheme);
    }

    public async Task CreateAsync(UserDTO user)
    {
        if (user is null)
        {
            throw new YourBookStoreException(ExceptionMessages.UserNullMessage);
        }

        if (_userRepository.GetAllAsNoTracking().Any(t => t.Email == user.Email))
        {
            throw new YourBookStoreException(ExceptionMessages.UserIsExists);
        }

        var newUser = _mapper.Map<User>(user);
        var hashSalt = _passwordHelper.EncryptPassword(newUser.Password);
        newUser.Password = hashSalt.Hash;
        newUser.StoredSalt = hashSalt.Salt;
        
        if (string.IsNullOrEmpty(newUser.Password) || newUser.Password == user.Password)
        {
            throw new YourBookStoreException(ExceptionMessages.IncorrectPasswordHash);
        }

        newUser.Role = UserRoles.User.ToString();
        await _userRepository.AddAsync(newUser);
        await _unitOfWork.SaveAsync();
    }
    
    

    public UserDTO GetUserByEmail(string email)
    {
        if (string.IsNullOrEmpty(email))
        {
            throw new YourBookStoreException(ExceptionMessages.GetShouldNotBeMessage("Email"));
        }
        
        var currentUser = _userRepository.GetAllAsNoTracking().FirstOrDefault(t => t.Email == email);
        
        if (currentUser is null)
        {
            throw new YourBookStoreException(ExceptionMessages.UserNullMessage);
        }
        
        var resultUser = _mapper.Map<UserDTO>(currentUser);
        resultUser.Role = currentUser.Role;
        return resultUser;
    }
    
    public async Task UpdateUserAsync(UserDTO user)
    {
        if (user is null)
        {
            throw new YourBookStoreException(ExceptionMessages.UserNullMessage);
        }

        var currentUser = _userRepository.GetAllAsNoTracking().FirstOrDefault(t => t.Email == user.Email);

        if (currentUser is null)
        {
            throw new YourBookStoreException(ExceptionMessages.UserNullMessage);
        }

        currentUser.FirstName = user.FirstName ;
        currentUser.LastName = user.LastName;
        currentUser.PostalCode = user.PostalCode;
        currentUser.Email = user.Email;
        currentUser.Phone = user.Phone;
        
        await _userRepository.UpdateAsync(currentUser);
        await _unitOfWork.SaveAsync();
    }
    public IEnumerable<UserDTO> GetAllUsers()
    {
        var users = _userRepository.GetAllAsNoTracking();

        if (users is null)
        {
            throw new YourBookStoreException(ExceptionMessages.UserNullMessage);
        }

        return _mapper.Map<IEnumerable<UserDTO>>(users);
    }

    
    public bool IsUserExists(string email) => 
        _userRepository.GetAllAsNoTracking().Any(t => t.Email == email);
}
﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using YourBookStore.BLL.Interfaces;
using YourBookStore.BLL.Validations;
using YourBookStore.DAL.Interfaces.RepositoryInterfaces;
using YourBookStore.Domain.DTOs;
using YourBookStore.Domain.Entities;
using YourBookStore.Utility.GlobalConstants;

namespace YourBookStore.BLL.Services;

public class CategoryService : ICategoryService
{
    private readonly IGenericEFRepository<Category> _categoryEFRepository;
    private readonly IMapper _mapper;

    public CategoryService( IGenericEFRepository<Category> genreEFRepository, IMapper mapper)
    {
        _categoryEFRepository = genreEFRepository;
        _mapper = mapper;
    }

    public CategoryDTO GetCategoryByName(string name)
    {
        if (string.IsNullOrEmpty(name))
        {
            throw new YourBookStoreException(ExceptionMessages.GetShouldNotBeMessage("Name"));
        }

        var category = _categoryEFRepository.GetAllAsNoTracking().Include(c => c.Genres).FirstOrDefault(t => t.Name == name);

        if (category  is null)
        {
            throw new YourBookStoreException(ExceptionMessages.GetShouldNotBeMessage("category "));
        }

        return _mapper.Map<CategoryDTO>(category);
    }

    public IEnumerable<string> GetAllCategoriesNames() => _categoryEFRepository.GetAllAsNoTracking().Select(t => t.Name);
    
    
}
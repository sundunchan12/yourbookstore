﻿using AutoMapper;
using Castle.Core.Internal;
using Microsoft.EntityFrameworkCore;
using YourBookStore.BLL.Interfaces;
using YourBookStore.BLL.Validations;
using YourBookStore.DAL.Infrastructure.Extensions;
using YourBookStore.DAL.Infrastructure.Filters.BookFilters;
using YourBookStore.DAL.Interfaces;
using YourBookStore.DAL.Interfaces.RepositoryInterfaces;
using YourBookStore.Domain.DTOs;
using YourBookStore.Domain.Entities;
using YourBookStore.Utility.GlobalConstants;

namespace YourBookStore.BLL.Services;

 public class BookService : IBookService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IGenericEFRepository<Book> _bookRepository;
        private readonly IGenericEFRepository<Genre> _genreRepository;
        private readonly IGenericEFRepository<Category> _categoryRepository;
        private readonly IGenericEFRepository<Author> _authorsRepository;
        private readonly IGenericEFRepository<Language> _languageRepository;
        private IEnumerable<IFilter<IQueryable<Book>, BookFilterDTO>> _bookFilters;
        private readonly IMapper _mapper;

        public BookService(
            IUnitOfWork unitOfWork,
            IGenericEFRepository<Book> bookRepository,
            IGenericEFRepository<Genre> genreRepository,
            IGenericEFRepository<Author> authorRepository,
            IGenericEFRepository<Category> categoryRepository,
            IGenericEFRepository<Language> languageRepository,
            IEnumerable<IFilter<IQueryable<Book>, BookFilterDTO>> bookFilters,
            IMapper mapper)
        {
            _genreRepository = genreRepository;
            _bookRepository = bookRepository;
            _categoryRepository = categoryRepository;
            _bookFilters = bookFilters;
            _unitOfWork = unitOfWork;
            _languageRepository = languageRepository;
            _mapper = mapper;
            _authorsRepository = authorRepository;
            
        }

        public async Task CreateBookAsync(BookDTO book)
        {
            if (book is null)
            {
                throw new YourBookStoreException(ExceptionMessages.BookNullMessage);
            }

            var newBook = _mapper.Map<Book>(book);

            if (book.GenreNames.IsNullOrEmpty())
            {
                throw new YourBookStoreException(ExceptionMessages.BookNullMessage);
            }

            var genreNamesArray = book.GenreNames.ToArray();
            newBook.BookGenres = _genreRepository
                .Get(t => genreNamesArray.Contains(t.Name)).Select(t => new BookGenres { BookId = newBook.Id, Book = newBook, GenreId = t.Id, Genre = t }).ToList();

            
            
            await _bookRepository.AddAsync(newBook);
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteBookAsync(BookDTO book)
        {
            if (book is null)
            {
                throw new YourBookStoreException(ExceptionMessages.BookNullMessage);
            }

            var currentBook = _bookRepository.Get(t => t.Code == book.Code).FirstOrDefault();

            if (currentBook is null)
            {
                throw new YourBookStoreException($"Book with key: {book.Code} not found");
            }

            await _bookRepository.DeleteAsync(currentBook);
            await _unitOfWork.SaveAsync();
        }

        public Task<BookDTO> GetBookByIdAsync(Guid id)
        {
            if (Guid.Empty == id)
            {
                throw new YourBookStoreException(ExceptionMessages.EntityIdNullMessage);
            }

            var book = _bookRepository.Get(b=>b.Id == id).FirstOrDefault();

            if (book is null)
            {
                throw new YourBookStoreException(ExceptionMessages.BookNullMessage);
            }
            var currentBook = _mapper.Map<BookDTO>(book);
            return Task.FromResult(currentBook);
        }

       
        public Task<BookDTO> GetBookByCodeAsync(string bookCode)
        {
            if (string.IsNullOrEmpty(bookCode))
            {
                throw new YourBookStoreException(ExceptionMessages.CodeNullMessage);
            }

            var book = _bookRepository.Get(g => g.Code == bookCode).FirstOrDefault();

            if (book is null)
            {
                throw new YourBookStoreException(ExceptionMessages.BookNullMessage);
            }
            var currentBook = _mapper.Map<BookDTO>(book);
            return Task.FromResult(currentBook);
        }

       
        public IEnumerable<BookDTO> GetBooksByGenre(string genre)
        {
            if (string.IsNullOrEmpty(genre))
            {
                throw new YourBookStoreException(ExceptionMessages.GetShouldNotBeMessage("Genre"));
            }

            return _mapper.Map<IEnumerable<BookDTO>>(_bookRepository.Get(book => book.BookGenres.Select(g => g.Genre.Name).Contains(genre)));
        }

        public IEnumerable<BookDTO> GetBooksByCategoryName(string categoryName)
        {
            if (string.IsNullOrEmpty(categoryName))
            {
                throw new YourBookStoreException(ExceptionMessages.GetShouldNotBeMessage("Category"));
            }

            return _mapper.Map<IEnumerable<BookDTO>>(_bookRepository.Get(book => book.BookGenres.Select(p => p.Genre.Category.Name).Contains(categoryName)));
        }

        public CategoryDTO GetBookCategoryByCode(string code)
        {
            var book = _bookRepository.GetAll().FirstOrDefault(b => b.Code == code);
            return _mapper.Map<CategoryDTO>(book.BookGenres.ToList()[0].Genre.Category);
        }
        
        
        public Task SoftDeleteBookAsync(BookDTO book)
        {
            throw new NotImplementedException();
        }

        public bool IsBookExists(string key) =>
            _bookRepository.GetAllAsNoTracking().Any(t => t.Code == key);
        
        
        public int GetAllBooksCount() =>
            _bookRepository.GetAllAsNoTracking().Count();

        
        public bool IsBookCodeExists(string code) =>
            _bookRepository.GetAllAsNoTracking().Any(t => t.Code == code);

        public Task<int> GetQuantityByCodeAsync(string code)
        {
            throw new NotImplementedException();
        }

        public async Task<decimal> GetMaxBookPriceAsync() =>
            await _bookRepository.GetAllAsNoTracking().MaxAsync(t => t.Price);
    }
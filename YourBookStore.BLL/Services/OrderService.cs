﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using YourBookStore.BLL.Infrastructure.Enums;
using YourBookStore.BLL.Interfaces;
using YourBookStore.BLL.Validations;
using YourBookStore.DAL.Interfaces;
using YourBookStore.DAL.Interfaces.RepositoryInterfaces;
using YourBookStore.Domain.DTOs;
using YourBookStore.Domain.Entities;
using YourBookStore.Utility.GlobalConstants;
using YourBookStore.Utility.GlobalEnums;

namespace YourBookStore.BLL.Services
{
    public class OrderService : IOrderService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IGenericEFRepository<Order> _orderRepository;
        private readonly IGenericEFRepository<User> _userRepository;
        public OrderService(
            IGenericEFRepository<Order> orderRepository,
            IGenericEFRepository<User> userRepository,
            IMapper mapper,
            IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _orderRepository = orderRepository;
            _userRepository = userRepository;
        }
        public async Task AddItemToBasketAsync(OrderDetailsDTO order, Guid customerId)
        {
            if (order is null)
            {
                throw new YourBookStoreException(ExceptionMessages.OrderNullMessage);
            }
            if (customerId == Guid.Empty)
            {
                throw new YourBookStoreException(ExceptionMessages.GetShouldNotBeMessage("Id"));
            }

            var basket = _orderRepository.GetAllAsNoTracking().FirstOrDefault(t => t.CustomerId == customerId) ?? new Order { CustomerId = customerId };
            basket.OrderDetails.Add(_mapper.Map<OrderDetails>(order));
            await _orderRepository.UpdateAsync(_mapper.Map<Order>(basket));
            await _unitOfWork.SaveAsync();
        }

        public Task ChangeOrderStatusAsync(OrderStatus status, Guid orderId)
        {
            throw new NotImplementedException();
        }

        public async Task ChangeQuantityOfOrderDetailsAsync(OrderDTO order)
        {
            if (order is null)
            {
                throw new YourBookStoreException(ExceptionMessages.OrderNullMessage);
            }

            var currentOrder = await _orderRepository.GetByIdAsync(order.Id);

            foreach (var orderDetail in currentOrder.OrderDetails)
            {
                var currentOrderDetails = order.OrderDetails.FirstOrDefault(t => t.BookId == orderDetail.BookId);
                if (orderDetail != null && currentOrderDetails != null && currentOrderDetails.Quantity != orderDetail.Quantity)
                {
                    orderDetail.Quantity = currentOrderDetails.Quantity;
                }
            }
            await _orderRepository.UpdateAsync(currentOrder);
            await _unitOfWork.SaveAsync();
        }

        public async Task<(OrderDTO, CreateOrderStatus)> CreateOrderAsync(OrderDTO order)
        {
            if (order is null)
            {
                throw new YourBookStoreException(ExceptionMessages.OrderNullMessage);
            }

            order.Status = "Очікує підтверждення";
            order.OrderDate = DateTime.Now;
            
            
            var currentOrder = await _orderRepository.AddAndReturnResultAsync(_mapper.Map<Order>(order));
            await _unitOfWork.SaveAsync();
            return (_mapper.Map<OrderDTO>(currentOrder), CreateOrderStatus.Success);
        }
        
        
        public IEnumerable<OrderDTO> GetAllOrders()
        {
            var orders = _orderRepository.GetAllAsNoTracking().ToList();
            return _mapper.Map<IEnumerable<OrderDTO>>(orders);
        }

        public async Task<OrderDTO> GetOrderByIdAsync(Guid id)
        {
            if (id == Guid.Empty)
                throw new YourBookStoreException(ExceptionMessages.GetShouldNotBeMessage("Id"));

            var order = await _orderRepository.GetAllAsNoTracking().FirstOrDefaultAsync(t => t.Id == id);

            if (order is null)
                throw new YourBookStoreException(ExceptionMessages.OrderNullMessage);

            var orderDto = _mapper.Map<OrderDTO>(order);
            return orderDto;
        }

        public IEnumerable<OrderDTO> GetOrdersByUserName(string userName)
        {
            var user = _userRepository.GetAll().FirstOrDefault(t => t.Email == userName);

            if (user is null || user.Id == Guid.Empty)
                throw new YourBookStoreException(ExceptionMessages.UserNullMessage);

            var orders = _orderRepository.GetAllAsNoTracking().Where(t => t.CustomerId == user.Id);
            return _mapper.Map<IEnumerable<OrderDTO>>(orders);
        }

        public Task UpdateOrderAsync(OrderDTO order)
        {
            throw new NotImplementedException();
        }
    }
}

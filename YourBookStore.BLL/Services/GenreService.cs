﻿using AutoMapper;
using YourBookStore.BLL.Interfaces;
using YourBookStore.BLL.Validations;
using YourBookStore.DAL.Interfaces.RepositoryInterfaces;
using YourBookStore.Domain.DTOs;
using YourBookStore.Domain.Entities;
using YourBookStore.Utility.GlobalConstants;

namespace YourBookStore.BLL.Services;

public class GenreService : IGenreService
{
    private readonly IGenericEFRepository<Genre> _genreEFRepository;
    private readonly IMapper _mapper;

    public GenreService( IGenericEFRepository<Genre> genreEFRepository, IMapper mapper)
    {
        _genreEFRepository = genreEFRepository;
        _mapper = mapper;
    }

    public GenreDTO GetGenreByName(string name)
    {
        if (string.IsNullOrEmpty(name))
        {
            throw new YourBookStoreException(ExceptionMessages.GetShouldNotBeMessage("Name"));
        }

        var genre = _genreEFRepository.GetAllAsNoTracking().FirstOrDefault(t => t.Name == name);

        if (genre is null)
        {
            throw new YourBookStoreException(ExceptionMessages.GetShouldNotBeMessage("Genre"));
        }

        return _mapper.Map<GenreDTO>(genre);
    }
    
    public IEnumerable<string> GetGenreNamesByCategoryName(string name)
    {
        if (string.IsNullOrEmpty(name))
        {
            throw new YourBookStoreException(ExceptionMessages.GetShouldNotBeMessage("Name"));
        }

        var genres = _genreEFRepository.GetAllAsNoTracking().Where(t => t.Category.Name == name).Select(g => g.Name);

        if (genres is null)
        {
            throw new YourBookStoreException(ExceptionMessages.GetShouldNotBeMessage("Genre"));
        }

        return genres;
    }

    public IEnumerable<string> GetAllGenreNames() =>
        _genreEFRepository.GetAll().Select(t => t.Name);

    
}

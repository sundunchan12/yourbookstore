﻿using AutoMapper;
using YourBookStore.BLL.Interfaces;
using YourBookStore.DAL.Interfaces.RepositoryInterfaces;
using YourBookStore.Domain.Entities;

namespace YourBookStore.BLL.Services;

public class AuthorService : IAuthorService
{
    private readonly IGenericEFRepository<Author> _authorEfRepository;
    private readonly IMapper _mapper;

    public AuthorService( IGenericEFRepository<Author> languageEfRepository, IMapper mapper)
    {
        _authorEfRepository = languageEfRepository;
        _mapper = mapper;
    }

    public IEnumerable<string> GetAllAuthorsNames() => _authorEfRepository.GetAllAsNoTracking().Select(t => t.FullName);

}
﻿using System.Security.Policy;
using AutoMapper;
using YourBookStore.DAL.Infrastructure.Extensions;
using YourBookStore.DAL.Infrastructure.Filters.BookFilters;
using YourBookStore.DAL.Interfaces;
using YourBookStore.DAL.Interfaces.RepositoryInterfaces;
using YourBookStore.Domain.DTOs;
using YourBookStore.Domain.Entities;
using YourBookStore.Utility.GlobalConstants;

namespace YourBookStore.BLL.Services;

public class PaginationService : IPaginationService
    {
        private readonly IMapper _mapper;
        private readonly IGenericEFRepository<Book> _bookRepository;
        private readonly IGenericEFRepository<Genre> _genreRepository;
        private IEnumerable<IFilter<IQueryable<Book>, BookFilterDTO>> _bookFilters;
        
        public PaginationService(
            IGenericEFRepository<Genre> genreRepository,
            IEnumerable<IFilter<IQueryable<Book>, BookFilterDTO>> bookFilters,
            IGenericEFRepository<Book> bookRepository, 
            IMapper mapper)
        {
            _bookFilters = bookFilters;
            _genreRepository = genreRepository;
            _bookRepository = bookRepository;
            _mapper = mapper;
        }

        public async Task<(int, IList<BookDTO>)> GetPaginateItemsAsync(BookFilterDTO filter)
        {
            
            filter.PageNumber = filter.PageNumber > 0 ? filter.PageNumber - 1 : 0;
            var bookFiltersArray = _bookFilters.ToArray();

            var bookPipeline = new BookFiltersPipeline();
            bookPipeline.DefaultPipelineConfig(bookFiltersArray);

            var books = (await _bookRepository.GetFilterBookList(bookFiltersArray, bookPipeline, filter));
         
            var count = books.Count();
            books = books.Skip(filter.PageSize * filter.PageNumber).Take(filter.PageSize);

            var itemsDto = _mapper.Map<IList<BookDTO>>(books.ToList());
            return (count, itemsDto);
        }
    }
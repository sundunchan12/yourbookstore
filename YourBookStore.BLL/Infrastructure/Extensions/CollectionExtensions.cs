﻿namespace YourBookStore.BLL.Infrastructure.Extensions;

public static class CollectionExtensions
{
    public static ICollection<T> AddRange<T>(this ICollection<T> destination, IEnumerable<T> source)
    {
        foreach (var item in source)
        {
            destination.Add(item);
        }

        return destination;
    }

    public static ICollection<T> AddRangeWithValidation<T>(this ICollection<T> destination, IEnumerable<T> source)
    {
        destination ??= new List<T>();

        if (source != null)
        {
            foreach (var item in source)
            {
                destination.Add(item);
            }
        }

        return destination;
    }
}
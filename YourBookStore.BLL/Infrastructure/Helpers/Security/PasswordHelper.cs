﻿using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using YourBookStore.BLL.Interfaces;
using YourBookStore.Domain.DTOs;

namespace YourBookStore.BLL.Infrastructure.Helpers.Security;

public class PasswordHelper : IPasswordHelper
{
    public HashSalt EncryptPassword(string password)
    {
        var salt = new byte[128 / 8];
        using var rng = RandomNumberGenerator.Create();
        rng.GetBytes(salt);
        var encryptedPassword = Convert.ToBase64String(KeyDerivation.Pbkdf2(
            password: password,
            salt: salt,
            prf: KeyDerivationPrf.HMACSHA1,
            iterationCount: 10000,
            numBytesRequested: 256 / 8
        ));
        return new HashSalt { Hash = encryptedPassword, Salt = salt };
    }

    public bool VerifyPassword(string enteredPassword, byte[] salt, string storedPassword)
    {
        var encryptedPassword = Convert.ToBase64String(KeyDerivation.Pbkdf2(
            password: enteredPassword,
            salt: salt,
            prf: KeyDerivationPrf.HMACSHA1,
            iterationCount: 10000,
            numBytesRequested: 256 / 8
        ));
        return encryptedPassword == storedPassword;
    }
}
﻿using Microsoft.AspNetCore.Http;
using YourBookStore.Domain.DTOs;

namespace YourBookStore.BLL.Interfaces;

public interface IUserService
{
    public Task SignOutAsync(HttpContext context);

    public Task SignInAsync(UserDTO user, HttpContext context);

    public Task CreateAsync(UserDTO user);

    public UserDTO GetUserByEmail(string email);

    public Task UpdateUserAsync(UserDTO user);

    public IEnumerable<UserDTO> GetAllUsers();

    public bool IsUserExists(string email);
}
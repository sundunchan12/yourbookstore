﻿using Microsoft.AspNetCore.Http;
using YourBookStore.Domain.DTOs;

namespace YourBookStore.BLL.Interfaces;

public interface IBasketService
{
    public Task<bool> AddOrderToBasketAsync(OrderDetailsDTO orderDetails, ISession session, string userName);
}
﻿using YourBookStore.Domain.DTOs;

public interface IPaginationService
{
    public Task<(int, IList<BookDTO>)> GetPaginateItemsAsync(BookFilterDTO filter);
}
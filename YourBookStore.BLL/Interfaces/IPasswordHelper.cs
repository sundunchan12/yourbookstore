﻿using YourBookStore.Domain.DTOs;

namespace YourBookStore.BLL.Interfaces;

public interface IPasswordHelper
{
    public HashSalt EncryptPassword(string password);

    public bool VerifyPassword(string enteredPassword, byte[] salt, string storedPassword);
}
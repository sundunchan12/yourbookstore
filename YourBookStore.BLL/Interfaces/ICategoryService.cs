﻿using YourBookStore.Domain.DTOs;
using YourBookStore.Domain.Entities;

namespace YourBookStore.BLL.Interfaces;

public interface ICategoryService
{
    public CategoryDTO GetCategoryByName(string name);

    public IEnumerable<string> GetAllCategoriesNames();

}

﻿using YourBookStore.BLL.Infrastructure.Enums;
using YourBookStore.Domain.DTOs;
using YourBookStore.Utility.GlobalEnums;

namespace YourBookStore.BLL.Interfaces;

public interface IOrderService
{

    public Task<(OrderDTO, CreateOrderStatus)> CreateOrderAsync(OrderDTO order);

    public Task<OrderDTO> GetOrderByIdAsync(Guid id);

    public Task UpdateOrderAsync(OrderDTO order);

    public Task AddItemToBasketAsync(OrderDetailsDTO order, Guid customerId);

    public IEnumerable<OrderDTO> GetAllOrders();

    public Task ChangeQuantityOfOrderDetailsAsync(OrderDTO order);

    public IEnumerable<OrderDTO> GetOrdersByUserName(string userName);

    public Task ChangeOrderStatusAsync(OrderStatus status, Guid orderId);
}
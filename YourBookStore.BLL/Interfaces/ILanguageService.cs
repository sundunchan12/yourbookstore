﻿namespace YourBookStore.BLL.Interfaces;

public interface ILanguageService
{
    public IEnumerable<string> GetAllLanguagesNames();
}
﻿using YourBookStore.Domain.DTOs;

namespace YourBookStore.BLL.Interfaces;

public interface IGenreService
{
    public GenreDTO GetGenreByName(string name);

    public IEnumerable<string> GetAllGenreNames();

    public IEnumerable<string> GetGenreNamesByCategoryName(string name);


}

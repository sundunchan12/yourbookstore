﻿using YourBookStore.Domain.DTOs;

namespace YourBookStore.BLL.Interfaces;

public interface IBookService
{
    public Task CreateBookAsync(BookDTO book);

    public Task DeleteBookAsync(BookDTO book);

    public Task<BookDTO> GetBookByIdAsync(Guid id);
    public Task<BookDTO> GetBookByCodeAsync(string code);

    public IEnumerable<BookDTO> GetBooksByGenre(string genre);
    

    public int GetAllBooksCount();

    public bool IsBookCodeExists(string book);

    public Task<int> GetQuantityByCodeAsync(string code);

    public CategoryDTO GetBookCategoryByCode(string code);

    public Task<decimal> GetMaxBookPriceAsync();

    public Task SoftDeleteBookAsync(BookDTO book);
    public bool IsBookExists(string code);
}
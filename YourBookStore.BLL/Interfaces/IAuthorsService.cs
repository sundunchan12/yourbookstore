﻿namespace YourBookStore.BLL.Interfaces;

public interface IAuthorService
{
    public IEnumerable<string> GetAllAuthorsNames();
}
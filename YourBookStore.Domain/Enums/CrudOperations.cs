﻿namespace YourBookStore.Domain.Enums;

public enum CrudOperations
{
    Create,
    Read,
    Update,
    Delete,
    Other
}
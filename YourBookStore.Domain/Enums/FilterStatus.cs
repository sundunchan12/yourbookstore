﻿namespace YourBookStore.Domain.Enums;

public enum FilterStatus
{
    Bestsellers,
    PriceASC,
    PriceDESC,
    New,
    None
}
﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace YourBookStore.Domain.Entities;

public class OrderDetails 
{
    [Key]
    [Column(Order = 0)]
    public Guid? BookId { get; set; }
    
    
    [Key]
    [Column(Order = 1)]
    public Guid? OrderId { get; set; }
    
    [ForeignKey("OrderId")]
    public virtual Order? Order { get; set; }
    
    [Column(TypeName = "money")]
    public decimal Price { get; set; }
    
    public Int16 Quantity { get; set; }
    
    
}
﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace YourBookStore.Domain.Entities;

public class BookGenres 
{
    public Guid BookId { get; set; }
    

    public virtual Book Book { get; set; }
    public Guid GenreId { get; set; }
    public virtual Genre Genre { get; set; }
    
}
﻿using System.ComponentModel.DataAnnotations;

namespace YourBookStore.Domain.Entities;

public class Language : BaseEntity
{
    [Required]
    [StringLength(20)]
    public string Name { get; set; }
    
    public virtual ICollection<Book> Books { get; set; }
}
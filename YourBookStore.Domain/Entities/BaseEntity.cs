﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace YourBookStore.Domain.Entities;

public class BaseEntity
{
    [Key]
    public Guid Id { get; set; }

}
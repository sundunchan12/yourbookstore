﻿using System.ComponentModel.DataAnnotations;

namespace YourBookStore.Domain.Entities;

public class Author : BaseEntity
{
    [Required]
    [StringLength(30)]
    public string FullName { get; set; }
    
    public virtual ICollection<Book> Books { get; set; }
    
   
}
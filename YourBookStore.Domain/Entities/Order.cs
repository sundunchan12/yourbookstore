﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace YourBookStore.Domain.Entities;

public class Order : BaseEntity
{
   
    public Guid CustomerId { get; set; }
   
    public DateTime? OrderDate { get; set; }
    
    public virtual ICollection<OrderDetails> OrderDetails { get; set; }
    
    [Required]
    [StringLength(30)]
    public string Status { get; set; }
    
    public string ShipAddress { get; set; }
    
    public string Email { get; set; }
    
    public string PostalCode { get; set; }
    
    public string FirstName { get; set; }
    
    public string SecondName { get; set; }
    
    public string Phone { get; set; }

}
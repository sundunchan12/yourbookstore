﻿using System.ComponentModel.DataAnnotations;

namespace YourBookStore.Domain.Entities;

public class User : BaseEntity
{
    [Required]
    [StringLength(30)]
    public string FirstName { get; set; }
   
     
    [Required]
    [StringLength(30)]
    public string LastName { get; set; }
    
    [Required]
    [Display(Name = "UserName")]
    public string Email { get; set; }
    
    [Required]
    [StringLength(50)]
    public string Password { get; set; }
    
    public byte[] StoredSalt { get; set; }
   
    [StringLength(30)]
    public string? Phone { get; set; }
    
    [StringLength(30)]
    public string City { get; set; }
    
    [StringLength(30)]
    public string? PostalCode { get; set; }
    [Required]
    public string Role { get; set; }
    public virtual  ICollection<Order> Orders { get; set; }
}

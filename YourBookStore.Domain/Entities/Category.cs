﻿using System.ComponentModel.DataAnnotations;

namespace YourBookStore.Domain.Entities;

public class Category : BaseEntity
{
    [Required]
    [StringLength(30)]
    public string Name { get; set; }
    
    public virtual ICollection<Genre> Genres { get; set; }
} 
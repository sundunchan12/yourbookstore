﻿using System.ComponentModel.DataAnnotations;

namespace YourBookStore.Domain.Entities;

public class Genre : BaseEntity
{
    [Required]
    [StringLength(30)]
    public string Name { get; set; }

    public Guid CategoryId { get; set; }
    public virtual Category Category { get; set; }
    
    public virtual ICollection<BookGenres> BookGenres { get; set; }
     
}
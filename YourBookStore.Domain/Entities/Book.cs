﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using YourBookStore.Domain.DTOs;

namespace YourBookStore.Domain.Entities;

public class Book : BaseEntity
{
    [Required]
    public string Code { get; set; }
    
    [Required]
    [StringLength(150)]
    public string Title { get; set; }
    
    [Required]
    public string Description { get; set; }
    
    [Required]
    [Column(TypeName = "decimal(18,4)")]
    public decimal Price { get; set; }
    
    public bool IsNew { get; set; }
    
    public bool IsBestseller { get; set; }

    public int NumberOfPages { get; set; }
    
    [ForeignKey("Language")]
    public Guid LanguageId { get; set; }
    public virtual Language Language { get; set; }
    
    [ForeignKey("Author")]
    public Guid AuthorId { get; set; }
    public virtual Author Author { get; set; }

   
    
    public virtual ICollection<OrderDetails> OrderDetails { get; set; }
    public virtual ICollection<BookGenres> BookGenres { get; set; }


}
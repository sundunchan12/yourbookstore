﻿namespace YourBookStore.Domain.DTOs;

public class BookFilterDTO
{
    public string SearchName { get; set; }
    
    public int PageSize { get; set; }
    
    public int PageNumber { get; set; }
    public decimal PriceFrom { get; set; }
    
    public decimal PriceTo { get; set; }
    
    public string FilterStatus { get; set; }
    
    public List<string> Genres { get; set; }
    
    public List<string> Categories { get; set; }
    
    public List<string> Authors { get; set; }
    
    public List<string> Languages { get; set; }
}
﻿using YourBookStore.Domain.Entities;

namespace YourBookStore.Domain.DTOs;

public class BookDTO
{
    public Guid Id { get; set; }
    
    public string Code { get; set; }
    
    public string Title { get; set; }
    
    public string Description { get; set; }
    
    public decimal Price { get; set; }
    
    public bool IsNew { get; set; }
    
    public bool IsBestseller { get; set; }
    
    public int NumberOfPages { get; set; }
    
    public AuthorDTO Author { get; set; }
    
    public BookLanguageDTO Language { get; set; }
    
    public IEnumerable<string> GenreNames { get; set; }


}
﻿namespace YourBookStore.Domain.DTOs;

public class UserDTO
{
    public string Email { get; set; }
    
    public string FirstName { get; set; }
    
    public string LastName { get; set; }
    
    public string PostalCode { get; set; }
    
    public string Phone { get; set; }
    public string Password { get; set; }
    
    public string City { get; set; }
    
    public string Role { get; set; }
}
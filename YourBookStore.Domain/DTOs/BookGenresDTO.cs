﻿namespace YourBookStore.Domain.DTOs;

public class BookGenresDTO
{
    public Guid BookId { get; set; }
    
    public Guid GenreId { get; set; }
}
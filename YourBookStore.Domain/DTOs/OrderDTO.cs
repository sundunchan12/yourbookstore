﻿namespace YourBookStore.Domain.DTOs;

[Serializable]
public class OrderDTO
{
    public Guid Id { get; set; }
    
    public Guid CustomerId { get; set; }
    
    public string Status { get; set; }
    
    public DateTime OrderDate { get; set; }
    
    public List<OrderDetailsDTO> OrderDetails { get; set; }
    
   
    public string ShipAddress { get; set; }
    
    public string Email { get; set; }
    
    public string PostalCode { get; set; }
    
    public string FirstName { get; set; }
    
    public string SecondName { get; set; }
    
    public string Phone { get; set; }
    
    public string CardHolderName { get; set; }
    
    public string CardNumber { get; set; }
    
    public string CVV { get; set; }
    
    public string ExpirationDate { get; set; }

}
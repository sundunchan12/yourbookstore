﻿namespace YourBookStore.Domain.DTOs;

public class HashSalt
{
    public string Hash { get; set; }
    public byte[] Salt { get; set; }
}

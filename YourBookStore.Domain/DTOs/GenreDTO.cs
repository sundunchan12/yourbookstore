﻿using YourBookStore.Domain.Entities;

namespace YourBookStore.Domain.DTOs;

public class GenreDTO
{
    public Guid Id { get; set; }
    
    public string Name { get; set; }
    
    public CategoryDTO Category { get; set; }
    
}
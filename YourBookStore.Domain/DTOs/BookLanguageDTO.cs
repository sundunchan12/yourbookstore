﻿namespace YourBookStore.Domain.DTOs;

public class BookLanguageDTO
{
    public Guid Id { get; set; }
    
    public string Name { get; set; }
}
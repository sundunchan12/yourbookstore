﻿using YourBookStore.Domain.Entities;

namespace YourBookStore.Domain.DTOs;

[Serializable]
public class OrderDetailsDTO
{
    public virtual OrderDTO Order { get; set; }
    
    public Guid OrderId { get; set; }
    
    public virtual BookDTO Book { get; set; }

    public string Code { get; set; }
    public Guid BookId { get; set; }

    public decimal Price { get; set; }

    public Int16 Quantity { get; set; }
}
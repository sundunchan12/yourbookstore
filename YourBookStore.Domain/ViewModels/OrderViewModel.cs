﻿using System.ComponentModel.DataAnnotations;
using YourBookStore.Domain.DTOs;
using YourBookStore.Domain.Entities;

namespace YourBookStore.Domain.ViewModels;

public class OrderViewModel
{
    public Guid? Id { get; set; }

    public Guid? CustomerId { get; set; }
    
    public string? OrderStatus { get; set; }
    
    public DateTime? OrderDate { get; set; }
    
    [Required(ErrorMessage = "Будь-ласка введіть місто та адресу доставки")]
    [StringLength(30)]
    [Display(Name = "Shipping address")]
    public string ShipAddress { get; set; }
    
  
    [Required(ErrorMessage = "Будь-ласка введіть номер карти")]
    [StringLength(16, ErrorMessage = "Номер карти повинен бути довжиною в 16 символів")]
    [Display(Name = "Card number")]
    public string CardNumber { get; set; }

    [Required(ErrorMessage = "Будь-ласка введіть CVV")]
    [StringLength(4)]
    [Display(Name = "CVV")]
    public string CVV { get; set; }
    
    [Required(ErrorMessage = "Будь-ласка введіть термін дії")]
    [StringLength(7)]
    [Display(Name = "Expiration date")]
    [RegularExpression(@"(0[1-9]|10|11|12)/20[0-9]{2}$", 
        ErrorMessage = "Невірний формат")]
    public string ExpirationsDate { get; set; }
    
    [Required(ErrorMessage = "Будь-ласка введіть ім'я карти")]
    [StringLength(30, MinimumLength = 2, ErrorMessage = "Ім'я має бути від 2 до 30 символів")]
    [Display(Name = "Cardholder name")]
    public string CardHolderName { get; set; }
    
    [Required(ErrorMessage = "Будь-ласка введіть ваше Ім'я")]
    [StringLength(30, MinimumLength = 2, ErrorMessage = "Ім'я має бути від 2 до 30 символів")]
    [Display(Name = "First name")]
    public string FirstName { get; set; }
    
    [Required(ErrorMessage = "Будь-ласка введіть ваше прізвище")]
    [StringLength(30, MinimumLength = 2, ErrorMessage = "Прізвище має бути від 2 до 30 символів")]
    public string SecondName { get; set; }
    
    [Required(ErrorMessage = "Будь-ласка введіть Email")]
    [Display(Name = "Email")]
    public string Email { get; set; }
    
    [Required(ErrorMessage = "Будь-ласка введіть поштовий індекс")]
    [StringLength(5)]
    [Display(Name = "Postal code")]
    [RegularExpression(@"\d{5}", 
        ErrorMessage = "Поштовий індекс має складатися з 5 цифр")]
    public string PostalCode { get; set; }
    
    [Required(ErrorMessage = "Будь-ласка введіть номер телефону")]
    [Display(Name = "Phone Number")]
    [RegularExpression(@"^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$", 
        ErrorMessage = "Недопустимий формат")]
    public string Phone { get; set; }

    public List<OrderDetailsViewModel>? OrderDetails { get; set; }
    
    
    
   
    
   
    
    
}
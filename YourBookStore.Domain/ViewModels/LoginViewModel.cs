﻿using System.ComponentModel.DataAnnotations;

namespace YourBookStore.Domain.ViewModels;

public class LoginViewModel
{
    [Required(ErrorMessage = "Будь-ласка введіть Email")]
    [Display(Name = "Email")]
    public string Email { get; set; }
    
    [Required(ErrorMessage = "Будь-ласка введіть пароль")]
    [DataType(DataType.Password)]
    [Display(Name = "Password")]
    public string Password { get; set; }
    
    [Display(Name = "RememberMe")]
    public bool RememberMe { get; set; }
}
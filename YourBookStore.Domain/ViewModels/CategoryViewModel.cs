﻿using System.ComponentModel.DataAnnotations;

namespace YourBookStore.Domain.Entities;

public class CategoryViewModel
{
        public Guid Id { get; set; }
        
        [Required]
        [Display(Name ="Name")]
        public string Name { get; set; }
        
        public ICollection<string> GenreNames { get; set; }
}


﻿using System.ComponentModel.DataAnnotations;
using YourBookStore.Domain.Enums;

namespace YourBookStore.Domain.ViewModels;

public class FilterViewModel
{
    public int PageNumber { get; set; }

    [Display(Name = "PageSize")]
    public int PageSize { get; set; }

    [StringLength(1000, MinimumLength = 4, ErrorMessage = "Search string length must be must be greater than 3")]
    [Display(Name = "SearchName")]
    public string? SearchName { get; set; }

    [Display(Name = "PriceFrom")]
    public decimal PriceFrom { get; set; }

    [Display(Name = "PriceTo")]
    public decimal PriceTo { get; set; }

    [Display(Name = "FilterStatus")]
    public FilterStatus FilterStatus { get; set; }

    [Display(Name = "Genres")]
    public List<string>? Genres { get; set; }

    [Display(Name = "Authors")]
    public List<string>? Authors { get; set; }

    [Display(Name = "Categories")]
    public List<string>? Categories { get; set; }
    
    [Display(Name = "Languages")]
    public List<string>? Languages { get; set; }
}
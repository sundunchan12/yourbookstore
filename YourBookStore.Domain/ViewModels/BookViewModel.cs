﻿using System.ComponentModel.DataAnnotations;

namespace YourBookStore.Domain.ViewModels;

public class BookViewModel
{
    public Guid Id { get; set; }
    
    [Required]
    [Display(Name = "Code")]
    public string Code { get; set; }
    
    [Required]
    [Display(Name = "Title")]
    public string Title { get; set; }
    
    [Required]
    [Display(Name = "Description")]
    public string Description { get; set; }
    
    [Required(ErrorMessage = "Invalid price")]
    [Display(Name = "Price")]
    public decimal? Price { get; set; }
    
    [Required]
    [Display(Name = "Language")]
    public string Language { get; set; }
    
    [Required]
    [Display(Name ="Author")]
    public string Author { get; set; }
    
    public bool IsNew { get; set; }
    
    public bool IsBestSeller { get; set; }
    
    [Required(ErrorMessage = "Invalid number of pages")]
    [Display(Name = "Number of pages")]
    public int NumberOfPages { get; set; }
    
    [Display(Name = "Genres")]
    public IEnumerable<string> GenreNames { get; set; }
    
}
﻿using System.ComponentModel.DataAnnotations;

namespace YourBookStore.Domain.ViewModels;

public class BoxTerminalDataViewModel
{
    [Display(Name = "AccountNumber")]
    public Guid AccountNumber { get; set; }
    
    [Display(Name = "OrderId")]
    public Guid OrderId { get; set; }
    
    [Display(Name = "Sum")]
    public decimal Sum { get; set; }
}
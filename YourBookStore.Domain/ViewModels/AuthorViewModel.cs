﻿using System.ComponentModel.DataAnnotations;

namespace YourBookStore.Domain.ViewModels;

public class AuthorViewModel
{
    public Guid Id { get; set; }
    
    [Required]
    [Display(Name = "Author name")]
    public string Name { get; set; }
}
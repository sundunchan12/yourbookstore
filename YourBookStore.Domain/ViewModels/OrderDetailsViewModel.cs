﻿using System.ComponentModel.DataAnnotations;
using YourBookStore.Domain.DTOs;

namespace YourBookStore.Domain.ViewModels;

public class OrderDetailsViewModel
{
       
    public Guid OrderId { get; set; }
    
    public Guid BookId { get; set; }
    
    public BookDTO Book { get; set; }
    
    [Display(Name = "Product code")]
    public string Code { get; set; }
    
    [Display(Name = "Price")]
    public decimal Price { get; set; }
    
    [Display (Name = "Quantity")]
    public Int16 Quantity { get; set; }
}
﻿using System.ComponentModel.DataAnnotations;

namespace YourBookStore.Domain.ViewModels;

public class RegistrationViewModel
{
    [Required(ErrorMessage = "Будь-ласка введіть Email")]
    [Display(Name = "Email")]
    public string Email { get; set; }
    
    [Required(ErrorMessage = "Будь-ласка введіть пароль")]
    [StringLength(30, ErrorMessage = "Пароль має містити як мінімум {2} символів.", MinimumLength = 6)]
    [DataType(DataType.Password)]
    public string Password { get; set; }
    
    [Required(ErrorMessage = "Будь-ласка підтвердіть пароль")]
    [DataType(DataType.Password)]
    [Display(Name = "ConfirmPassword")]
    [Compare("Password", ErrorMessage = "Паролі не співпадають.")]
    public string ConfirmPassword { get; set; }
    
    [Required(ErrorMessage = "Будь-ласка введіть ваше Ім'я")]
    [StringLength(30, MinimumLength = 2, ErrorMessage = "Ім'я має бути від 2 до 30 символів")]
    [Display(Name = "First name")]
    public string FirstName { get; set; }
    
    [Required(ErrorMessage = "Будь-ласка введіть ваше прізвище")]
    
    [StringLength(30, MinimumLength = 2, ErrorMessage = "Прізвище має бути від 2 до 30 символів")]
    public string LastName { get; set; }
    
    [Required(ErrorMessage = "Будь-ласка введіть номер телефону")]
    [Display(Name = "Phone Number")]
    [RegularExpression(@"^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$", 
        ErrorMessage = "Недопустимий формат")]
    public string Phone { get; set; }
    
    [Required(ErrorMessage = "Будь-ласка введіть поштовий індекс")]
    [StringLength(10)]
    [Display(Name = "Postal code")]
    [RegularExpression(@"\d{5}", 
        ErrorMessage = "Поштовий індекс має складатися з 5 цифр")]
    public string PostalCode { get; set; }

    [Required(ErrorMessage = "Будь-ласка введіть ваше місто")]
    [StringLength(30)]
    [Display(Name = "CIty")]
    public string City { get; set; }
}
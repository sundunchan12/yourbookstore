﻿using System.ComponentModel.DataAnnotations;

namespace YourBookStore.Domain.ViewModels;

public class GenreViewModel
{
    [Display(Name = "Name")]
    public string Name { get; set; }
    
    public Guid CategoryId { get; set; }
    
}